package models;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import br.com.adilson.util.Extenso;
import br.com.adilson.util.PrinterMatrix;
import utility.Conexion;
import utility.Magician;

/**
 * Clase para el manejo de Bancos
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */

public class Solicitud {
	private String codigo;
	private Integer id_cliente;
	private Integer id_beneficiario;
	private Integer id_moneda;
	private Integer id_cuenta;
	private Double monto_envio;
	private Double taza;
	private Double total;
	private String estado;
	private String observacion;
	private Integer id_user;
	private Conexion conn;
	
	public Solicitud(){}
	
	public Solicitud(int cliente, int beneficiario, int cuenta, int moneda, double envio, double taza, double total, String codigo, int id_user){
		this.id_cliente = cliente;
		this.id_beneficiario = beneficiario;
		this.id_moneda = moneda;
		this.id_cuenta = cuenta;
		this.monto_envio = envio;
		this.taza = taza;
		this.total = total;
		this.codigo = codigo;
		this.id_user = id_user;
	}

	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Integer getId_cuenta() {
		return id_cuenta;
	}

	public void setId_cuenta(Integer id_cuenta) {
		this.id_cuenta = id_cuenta;
	}

	public Integer getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(Integer id_cliente) {
		this.id_cliente = id_cliente;
	}

	public Integer getId_beneficiario() {
		return id_beneficiario;
	}

	public void setId_beneficiario(Integer id_beneficiario) {
		this.id_beneficiario = id_beneficiario;
	}

	public Integer getId_moneda() {
		return id_moneda;
	}

	public void setId_moneda(Integer id_moneda) {
		this.id_moneda = id_moneda;
	}

	public Double getMonto_envio() {
		return monto_envio;
	}

	public void setMonto_envio(Double monto_envio) {
		this.monto_envio = monto_envio;
	}

	public Double getTaza() {
		return taza;
	}

	public void setTaza(Double taza) {
		this.taza = taza;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Integer getId_user() {
		return id_user;
	}

	public void setId_user(Integer id_user) {
		this.id_user = id_user;
	}
	

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	/* Desconecta la variable de conexion creada por el objeto en la llamada del metodo
	 */
	public void desconectar() throws SQLException{  
		conn.desConectar();
	}
	
	public Conexion getConn(){
		return conn;
	}
	
	/* Registra un nuevo Banco
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{ 
		String query = "INSERT INTO solicitudes (id_cliente, id_beneficiario, id_moneda, monto, taza, total, estado, id_cuenta, codigo, id_usuario) VALUES ('"+id_cliente+"','"+id_beneficiario+"', '"+id_moneda+"' , '"+monto_envio+"' , '"+taza+"' , '"+total+"','PROCESSANDO', '"+id_cuenta+"' , '"+codigo+"' , '"+id_user+"')";
		boolean status = conn.ejecutar(query);
		if (status == true){
			Icon icono = new ImageIcon(Solicitud.class.getResource("/img/listo.png"));
			JOptionPane.showMessageDialog(null, "Registro realizado com sucesso!", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		return status;
	}
	
	/* Edita el registro del Banco
	 * @param id_banco id del banco
	 * @return retorna si la modificacion se realizo con exito
	 */
	/*public boolean Editar(int id_moneda) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "UPDATE monedas SET taza = '"+taza+"', nombre = '"+nombre+"', pais = '"+pais+"' WHERE id = "+id_moneda;
		boolean status = conn.ejecutar(query);
		if(status == true){
			Icon icono = new ImageIcon(Solicitud.class.getResource("/img/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE,icono);
		}
		conn.desConectar();
		return status;
	}*/
	
	/* Trae un registro segun la primary key 
	 * @param id_banco id del banco
	 */
	public void TraerRegistro(int id_solicitud) throws SQLException{ 
		String query = "SELECT * FROM solicitudes WHERE id = "+id_solicitud;
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			this.id_cliente = rs.getInt("id_cliente");
			this.id_beneficiario = rs.getInt("id_beneficiario");
			this.id_moneda = rs.getInt("id_moneda");
			this.id_cuenta = rs.getInt("id_cuenta");
			this.monto_envio = rs.getDouble("monto");
			this.taza = rs.getDouble("taza");
			this.total = rs.getDouble("total");
			this.codigo = rs.getString("codigo");
			this.estado = rs.getString("estado");
			this.observacion = rs.getString("observacion");
			this.id_user = rs.getInt("id_usuario");
			rs.close();
		}
	}
	
	/* Cambia el estado de un banco registrado
	 * @param id_banco id del banco
	 * @return retorna el estado del banco
	 */
	public String CambiarEstado(int id_solicitud) throws SQLException{ 
		String estado = null;
		String busca_estado = "SELECT estado FROM solicitudes WHERE id = "+id_solicitud;
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("estado");
			busca.close();
		}
		if(estado.equals("PROCESSANDO")){
			String query = "UPDATE solicitudes SET estado = 'REVISAO' WHERE id = "+id_solicitud;
			Icon icono = new ImageIcon(Solicitud.class.getResource("/img/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "Est� seguro de alterar o status da Solicitacao?","Alterar Solicitacao",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Solicitud.class.getResource("/img/listo.png"));
				JOptionPane.showMessageDialog(null, "Solicitacao, alterar para revisao!", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "REVISAO";
	        }else{
	        	estado = "PROCESSANDO";
	        }
		}
		return estado;
	}
	
	public String CambiarEstadoRevision(int id_solicitud, String estado_solicitud, String observacion) throws SQLException{ 
		String estado = null;
		String busca_estado = "SELECT estado FROM solicitudes WHERE id = "+id_solicitud;
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("estado");
			busca.close();
		}
		if(estado.equals("REVISAO")){
			String query = "UPDATE solicitudes SET estado = '"+estado_solicitud+"', observacion = '"+observacion+"' WHERE id = "+id_solicitud;
			Icon icono = new ImageIcon(Solicitud.class.getResource("/img/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "Tem certeza que deseja alterar o status da Solicitaco?","Alterar Status",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Solicitud.class.getResource("/img/listo.png"));
				JOptionPane.showMessageDialog(null, "Solicitacao alterada para "+estado_solicitud, "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = estado_solicitud;
	        }else{
	        	estado = "REVISAO";
	        }
		}
		return estado;
	}
	
	/* Lista todos los bancos registrados
	 * @return lista de bancos registrados
	 */
	public ResultSet ListarSolicitudes() throws SQLException{ 
		String query = "SELECT solicitudes.codigo, clientes.nombre, beneficiarios.nombre, monedas.nombre, monto, solicitudes.taza, total, solicitudes.estado "+
					   "FROM solicitudes "+
					   "JOIN clientes ON clientes.serial = id_cliente "+
					   "JOIN beneficiarios ON beneficiarios.serial = id_beneficiario "+
					   "JOIN monedas ON monedas.id = id_moneda "+
					   "ORDER BY solicitudes.codigo DESC";
		ResultSet rs = conn.Consulta(query);
		return rs;
	}
	
	public ResultSet FiltraSolicitudes(String argumento) throws SQLException{ 
		String query = "SELECT solicitudes.codigo, clientes.nombre, beneficiarios.nombre, monedas.nombre, monto, solicitudes.taza, total, solicitudes.estado "+
					   "FROM solicitudes "+
					   "JOIN clientes ON clientes.serial = id_cliente "+
					   "JOIN beneficiarios ON beneficiarios.serial = id_beneficiario "+
					   "JOIN monedas ON monedas.id = id_moneda "+
					   "WHERE solicitudes.codigo ILIKE '%"+argumento+"%'  "+
					   "ORDER BY solicitudes.codigo DESC";
		ResultSet rs = conn.Consulta(query);
		return rs;
	}
	
	public ResultSet ListarMonedasPais(String pais) throws SQLException{ 
		String query = "SELECT id, nombre FROM monedas WHERE pais = '"+pais+"' ORDER BY id";
		ResultSet rs = conn.Consulta(query);
		return rs;
	}
	
	/* Filtra bancos segun el nombre
	 * @param argumento nombre del banco
	 * @return lista de bancos filtrados
	 */
	public ResultSet FiltraMonedas(String argumento) throws SQLException{ 
		String query = "SELECT * FROM monedas WHERE nombre ILIKE '%"+argumento+"%' OR pais ILIKE '%"+argumento+"%' ORDER BY id";
		ResultSet rs = conn.Consulta(query);
		return rs;
	}
	
	public int RetornaID(String cod) throws SQLException{
		int id = 0;
		String query = "SELECT id FROM solicitudes WHERE codigo = '"+cod+"'";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			id = rs.getInt("id");
			rs.close();
		}
		return id;
	}
	
	public int RetornaSerie() throws SQLException{
		int nro = 0;
		String query = "SELECT MAX(id) as nro FROM solicitudes";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			nro = rs.getInt("nro");
			rs.close();
		}
		return nro;
	}
	
	public void imprimirSolicitud(int id_solicitud) throws SQLException{
		PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        e.setNumber(101.85);


        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(60, 40);
        //Imprimir * de la 2da linea a 25 en la columna 1;
        //printer.printCharAtLin(2, 25, 1, "*");
        //Imprimir * 1ra linea de la columa de 1 a 80
       //printer.printCharAtCol(1, 1, 40, "=");
        //Imprimir Encabezado nombre del La EMpresa
        
        Solicitud Osolicitud = new Solicitud();
        Osolicitud.setConn(conn);
        Osolicitud.TraerRegistro(id_solicitud);
        
       printer.printTextWrap(0, 1, 0, 40, "========================================");
       printer.printTextWrap(1, 1, 0, 40, "ARA CORRETORA DE  CAMBIO E INVESTIMENTOS");
       printer.printTextWrap(2, 1, 14, 40,"DIGITAIS LTDA");
       printer.printTextWrap(3, 1, 8, 40, "CNPJ: 20.419.043/0001-13");
       printer.printTextWrap(4, 1, 12, 40,"(92) 3021-4353");
       printer.printTextWrap(5, 1, 10, 40,"www.aracambio.com.br");
       printer.printTextWrap(6, 1, 0, 40, "========================================");
       printer.printTextWrap(7, 1, 18, 40, "REMETENTE");
       printer.printTextWrap(8, 1, 0, 40, "----------------------------------------");
       printer.printTextWrap(9, 1, 0, 40, "N. DA TRANSACAO:");
       printer.printTextWrap(10, 1, 0, 40, "NOME:");
       printer.printTextWrap(11, 1, 0, 40, "SOBRENOME:");
       printer.printTextWrap(12, 1, 0, 40, "IDENTIDADE:");
       printer.printTextWrap(13, 1, 0, 40, "TELEFONE:");
       printer.printTextWrap(14, 1, 0, 40, "========================================");
       
       Cliente Ocliente = new Cliente();
       Ocliente.setConn(conn);
       Ocliente.TraerRegistro(Osolicitud.getId_cliente()+"");
       
       printer.printTextWrap(9, 1, 18, 40, Osolicitud.getCodigo());
       printer.printTextWrap(10, 1, 18, 40, Ocliente.getNombre());
       printer.printTextWrap(11, 1, 18, 40, Ocliente.getApellido());
       printer.printTextWrap(12, 1, 18, 40, Ocliente.getDocumento());
       printer.printTextWrap(13, 1, 18, 40, Ocliente.getCelular());
       
       printer.printTextWrap(15, 1, 16, 40, "BENEFICIARIO");
       printer.printTextWrap(16, 1, 0, 40, "----------------------------------------");
       printer.printTextWrap(17, 1, 0, 40, "NOME:");
       printer.printTextWrap(18, 1, 0, 40, "SOBRENOME:");
       printer.printTextWrap(19, 1, 0, 40, "IDENTIDADE:");
       printer.printTextWrap(20, 1, 0, 40, "========================================");
       
       Beneficiario Obeneficiario = new Beneficiario();
       Obeneficiario.setConn(conn);
       Obeneficiario.TraerRegistro(Osolicitud.getId_beneficiario()+"");
       
       printer.printTextWrap(17, 1, 18, 40, Obeneficiario.getNombre());
       printer.printTextWrap(18, 1, 18, 40, Obeneficiario.getApellido());
       printer.printTextWrap(19, 1, 18, 40, Obeneficiario.getDocumento());
       
       printer.printTextWrap(21, 1, 14, 40, "DADOS BANCARIOS");
       printer.printTextWrap(22, 1, 0, 40, "----------------------------------------");
       printer.printTextWrap(23, 1, 0, 40, "BANCO:");
       printer.printTextWrap(24, 1, 0, 40, "N. DA CONTA:");
       printer.printTextWrap(25, 1, 0, 40, "TIPO DE CONTA:");
       printer.printTextWrap(26, 1, 0, 40, "========================================");
       
       
       String [] vector = Obeneficiario.TraerRegistroCuenta(Osolicitud.getId_cuenta()+"");
       
       printer.printTextWrap(23, 1, 18, 40, vector[2]);
       printer.printTextWrap(24, 1, 18, 40, vector[3]);
       printer.printTextWrap(25, 1, 18, 40, vector[4]);
       
       printer.printTextWrap(27, 1, 19, 40, "VALORES");
       printer.printTextWrap(28, 1, 0, 40, "----------------------------------------");
       printer.printTextWrap(29, 1, 0, 40, "MOEDA:");
       printer.printTextWrap(30, 1, 0, 40, "VALOR");
       printer.printTextWrap(31, 1, 0, 40, "TAXA:");
       printer.printTextWrap(32, 1, 0, 40, "BOLIVAR:");
       printer.printTextWrap(33, 1, 0, 40, "========================================");
       
       Moneda Omoneda = new Moneda();
       Omoneda.setConn(conn);
       Omoneda.TraerRegistro(Osolicitud.getId_moneda());
       Magician mago = new Magician();
       
       printer.printTextWrap(29, 1, 18, 40, Omoneda.getNombre());
       printer.printTextWrap(30, 1, 18, 40, mago.conversion(Osolicitud.getMonto_envio()));
       printer.printTextWrap(31, 1, 18, 40, mago.conversion(Osolicitud.getTaza()));
       printer.printTextWrap(32, 1, 18, 40, mago.conversion(Osolicitud.getTotal()));
       
       printer.printTextWrap(34, 1, 12, 40,"TERMO DE ACEITE");
       printer.printTextWrap(35, 1, 0, 40, "----------------------------------------");
       printer.printTextWrap(36, 1, 0, 40, "Todas as informacoes fornecidas por mim");
       printer.printTextWrap(37, 1, 0, 40, "e  contidas  em  qualquer  documentacao");
       printer.printTextWrap(38, 1, 0, 40, "emitida  em conexao com a minha  compra");
       printer.printTextWrap(39, 1, 0, 40, "de  tais servicos  sao verdadeiras.  Eu");
       printer.printTextWrap(40, 1, 0, 40, "reconheco ainda que  esta operacao esta");
       printer.printTextWrap(41, 1, 0, 40, "sujeita   aos    termos   e   condicoes");
       printer.printTextWrap(42, 1, 0, 40, "previstos  na  ARA  CAMBIO, e estou  de");
       printer.printTextWrap(43, 1, 0, 40, "acordo com este termos.");
    
       printer.printTextWrap(46, 1, 10, 40, "___________________");
       printer.printTextWrap(47, 1, 15, 40, "ASSINATURA");
       printer.printTextWrap(49, 1, 0, 40,  "========================================");
       
       Usuario Ousuario = new Usuario();
       Ousuario.setConn(conn);
       Ousuario.TraerRegistro(Osolicitud.getId_user());
       Date date = new Date();
       DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
       printer.printTextWrap(50, 1, 0, 40, "HORA E DATA: "+hourdateFormat.format(date));
       printer.printTextWrap(51, 1, 0, 40, "OPERADOR: "+Ousuario.getNombre().toUpperCase());
       printer.printTextWrap(52, 1, 0, 40, "----------------------------------------");
       
       
       
       //printer.printCharAtCol(4, 1, 40, "=");
       //printer.printTextWrap(linI, linE, colI, colE, null);
      
       
     

        
        printer.toFile("impresion.txt");

      FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();


        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }
        
        //inputStream.close();

	}

	public void setConn(Conexion conn) {
		this.conn = conn;
	}
	
}
