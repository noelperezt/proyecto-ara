package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import utility.Conexion;

/**
 * Clase para el manejo de Bancos
 * @author Noel P�rez
 * @email noelperezt@resulthti.com.br
 */

public class Banco {
	private String codigo;
	private String nombre;
	private String pais;
	private Conexion conn;
	
	public Banco(){}
	
	public Banco(String codigo, String nombre, String pais){
		this.codigo = codigo;
		this.nombre = nombre;
		this.pais = pais;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	/* Desconecta la variable de conexion creada por el objeto en la llamada del metodo
	 */
	public void desconectar() throws SQLException{  
		conn.desConectar();
	}
	

	public Conexion getConn() {
		return conn;
	}

	public void setConn(Conexion conn) {
		this.conn = conn;
	}

	/* Registra un nuevo Banco
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{ 
		String query = "INSERT INTO bancos (nombre, pais, status, codigo) VALUES ('"+nombre+"','"+pais+"','A','"+codigo+"')";
		boolean status = conn.ejecutar(query);
		if (status == true){
			Icon icono = new ImageIcon(Banco.class.getResource("/img/listo.png"));
			JOptionPane.showMessageDialog(null, "Registro realizado con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		return status;
	}
	
	/* Edita el registro del Banco
	 * @param id_banco id del banco
	 * @return retorna si la modificacion se realizo con exito
	 */
	public boolean Editar(int id_banco) throws SQLException{ 
		String query = "UPDATE bancos SET codigo = '"+codigo+"', nombre = '"+nombre+"', pais = '"+pais+"' WHERE id = "+id_banco;
		boolean status = conn.ejecutar(query);
		if(status == true){
			Icon icono = new ImageIcon(Banco.class.getResource("/img/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE,icono);
		}
		return status;
	}
	
	/* Trae un registro segun la primary key 
	 * @param id_banco id del banco
	 */
	public void TraerRegistro(int id_banco) throws SQLException{ 
		String query = "SELECT * FROM bancos WHERE id = "+id_banco;
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			this.codigo = rs.getString("codigo");
			this.nombre = rs.getString("nombre");
			this.pais = rs.getString("pais");
			rs.close();
		}
	}
	
	/* Cambia el estado de un banco registrado
	 * @param id_banco id del banco
	 * @return retorna el estado del banco
	 */
	public String CambiarEstado(int id_banco) throws SQLException{ 
		String estado = null;
		String busca_estado = "SELECT status FROM bancos WHERE id = "+id_banco;
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("status");
			busca.close();
		}
		if(estado.equals("I")){
			String query = "UPDATE bancos SET status = 'A' WHERE id = "+id_banco;
			Icon icono = new ImageIcon(Banco.class.getResource("/img/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de activar el Banco?","Activar Banco",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Banco.class.getResource("/img/listo.png"));
				JOptionPane.showMessageDialog(null, "Banco Activado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "A";
	        }else{
	        	estado = "I";
	        }
		}else if(estado.equals("A")){
			String query = "UPDATE bancos SET status = 'I' WHERE id = "+id_banco;
			Icon icono = new ImageIcon(Banco.class.getResource("/img/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de desactivar el Banco?","Desactivar Banco",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Banco.class.getResource("/img/listo.png"));
				JOptionPane.showMessageDialog(null, "Banco Desactivado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "I";
	        }else{
	        	estado = "A";
	        }
		}
		return estado;
	}
	
	/* Lista todos los bancos registrados
	 * @return lista de bancos registrados
	 */
	public ResultSet ListarBancos() throws SQLException{ 
		String query = "SELECT id, codigo, nombre, pais, status FROM bancos ORDER BY nombre ASC";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Lista todos los bancos registrados
	 * @return lista de bancos registrados
	 */
	
	public ResultSet ListarBancosPais(String pais) throws SQLException{ 
		String query = "SELECT id, nombre FROM bancos WHERE pais = '"+pais+"' ORDER BY nombre ASC";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra bancos segun el nombre
	 * @param argumento nombre del banco
	 * @return lista de bancos filtrados
	 */
	public ResultSet FiltraBancos(String argumento) throws SQLException{ 
		String query = "SELECT id, codigo, nombre, pais, status FROM bancos WHERE nombre ILIKE '%"+argumento+"%' OR pais ILIKE '%"+argumento+"%' OR codigo ILIKE '%"+argumento+"%' ORDER BY id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
}
