package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import utility.Conexion;

/**
 * Clase para el manejo de Bancos
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */

public class Moneda {
	private String nombre;
	private String pais;
	private int taza;
	private Conexion conn;
	
	public Moneda(){}
	
	public Moneda(String nombre, String pais, int taza){
		this.nombre = nombre;
		this.pais = pais;
		this.taza = taza;
	}


	public int getTaza() {
		return taza;
	}

	public void setTaza(int taza) {
		this.taza = taza;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	/* Desconecta la variable de conexion creada por el objeto en la llamada del metodo
	 */
	public void desconectar() throws SQLException{  
		conn.desConectar();
	}
	
	public Conexion getConn() {
		return conn;
	}

	public void setConn(Conexion conn) {
		this.conn = conn;
	}

	/* Registra un nuevo Banco
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{ 
		String query = "INSERT INTO monedas (nombre, pais, taza, status) VALUES ('"+nombre+"','"+pais+"', '"+taza+"','A')";
		boolean status = conn.ejecutar(query);
		if (status == true){
			Icon icono = new ImageIcon(Moneda.class.getResource("/img/listo.png"));
			JOptionPane.showMessageDialog(null, "Registro realizado con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		return status;
	}
	
	/* Edita el registro del Banco
	 * @param id_banco id del banco
	 * @return retorna si la modificacion se realizo con exito
	 */
	public boolean Editar(int id_moneda) throws SQLException{ 
		String query = "UPDATE monedas SET taza = '"+taza+"', nombre = '"+nombre+"', pais = '"+pais+"' WHERE id = "+id_moneda;
		boolean status = conn.ejecutar(query);
		if(status == true){
			Icon icono = new ImageIcon(Moneda.class.getResource("/img/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE,icono);
		}
		return status;
	}
	
	/* Trae un registro segun la primary key 
	 * @param id_banco id del banco
	 */
	public void TraerRegistro(int id_moneda) throws SQLException{ 
		String query = "SELECT * FROM monedas WHERE id = "+id_moneda;
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			this.taza = rs.getInt("taza");
			this.nombre = rs.getString("nombre");
			this.pais = rs.getString("pais");
			rs.close();
		}
	}
	
	/* Cambia el estado de un banco registrado
	 * @param id_banco id del banco
	 * @return retorna el estado del banco
	 */
	public String CambiarEstado(int id_moneda) throws SQLException{ 
		String estado = null;
		String busca_estado = "SELECT status FROM monedas WHERE id = "+id_moneda;
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("status");
			busca.close();
		}
		if(estado.equals("I")){
			String query = "UPDATE monedas SET status = 'A' WHERE id = "+id_moneda;
			Icon icono = new ImageIcon(Moneda.class.getResource("/img/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de activar la Moneda?","Activar Moneda",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Moneda.class.getResource("/img/listo.png"));
				JOptionPane.showMessageDialog(null, "Moneda Activada", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "A";
	        }else{
	        	estado = "I";
	        }
		}else if(estado.equals("A")){
			String query = "UPDATE monedas SET status = 'I' WHERE id = "+id_moneda;
			Icon icono = new ImageIcon(Moneda.class.getResource("/img/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de desactivar la Moneda?","Desactivar Moneda",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Moneda.class.getResource("/img/listo.png"));
				JOptionPane.showMessageDialog(null, "Moneda Desactivada", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "I";
	        }else{
	        	estado = "A";
	        }
		}
		return estado;
	}
	
	/* Lista todos los bancos registrados
	 * @return lista de bancos registrados
	 */
	public ResultSet ListarMonedas() throws SQLException{ 
		String query = "SELECT * FROM monedas ORDER BY id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	public ResultSet ListarMonedasPais(String pais) throws SQLException{ 
		String query = "SELECT id, nombre FROM monedas WHERE pais = '"+pais+"' ORDER BY id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra bancos segun el nombre
	 * @param argumento nombre del banco
	 * @return lista de bancos filtrados
	 */
	public ResultSet FiltraMonedas(String argumento) throws SQLException{ 
		String query = "SELECT * FROM monedas WHERE nombre ILIKE '%"+argumento+"%' OR pais ILIKE '%"+argumento+"%' ORDER BY id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
}
