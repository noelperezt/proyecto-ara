package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import utility.Conexion;
import utility.Magician;

/**
 * Clase para el manejo de Clientes
 * @author Noel P�rez
 */

public class Cliente {
	private String codigo;
	private String tipo;
	private String documento;
	private String cpf;
	private String nombre;
	private String apellido;
	private Date fecha_nac;
	private String profesion;
	private String pais;
	private String celular;
	private String n_tabajo;
	private String telefono;
	private String correo;
	private String cep;
	private String lugar;
	private String numero;
	private String barrio;
	private String ciudad;
	private String estado;
	private String observacion;
	private Conexion conn;

	public Cliente(){}
	
	public Cliente(String codigo, String tipo, String documento, String cpf, String nombre,String apellido,Date fecha_nac, String profesion, String pais, String celular, String n_tabajo, String telefono, String correo, String cep, String lugar, String numero, String barrio, String ciudad, String estado, String observacion ){ 
		this.codigo = codigo;
		this.tipo = tipo;
		this.documento = documento;
		this.cpf = cpf;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fecha_nac = fecha_nac;
		this.profesion = profesion;
		this.pais = pais;
		this.celular = celular;
		this.n_tabajo = n_tabajo;
		this.telefono = telefono;
		this.correo = correo;
		this.cep = cep;
		this.lugar = lugar;
		this.numero = numero;
		this.barrio = barrio;
		this.ciudad = ciudad;
		this.estado = estado;
		this.observacion = observacion;
	}
	
	public Conexion getConn() {
		return conn;
	}

	public void setConn(Conexion conn) {
		this.conn = conn;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Date getFecha_nac() {
		return fecha_nac;
	}

	public void setFecha_nac(Date fecha_nac) {
		this.fecha_nac = fecha_nac;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getN_tabajo() {
		return n_tabajo;
	}

	public void setN_tabajo(String n_tabajo) {
		this.n_tabajo = n_tabajo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	/* Desconecta la variable de conexion creada por el objeto en la llmada del metodo
	 */
	public void desconectar() throws SQLException{ 
		conn.desConectar();
	}
	
	/* Registra un nuevo cliente
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{//Registra un nuevo cliente
		boolean registro = false;
   		String fecha;
   		SimpleDateFormat formatter;
   		formatter = new SimpleDateFormat("yyyy-MM-dd");
   		fecha = "null";
   		if(fecha_nac != null){
   			fecha = "'"+formatter.format(fecha_nac)+"'";
   		}
   		String query = "INSERT INTO clientes (codigo, tipo, documento, cpf, nombre, apellido, fecha_nac, profesion, pais, celular, num_trabajo, telefono, email, cep, lugar, numero, barrio, estado, observacion, status, ciudad) "
   				      + "VALUES ('"+codigo+"', '"+tipo+"', '"+documento+"', '"+cpf+"', '"+nombre+"', '"+apellido+"', "+fecha+", '"+profesion+"', '"+pais+"', '"+celular+"', '"+n_tabajo+"','"+telefono+"', '"+correo+"', '"+cep+"', '"+lugar+"', '"+numero+"', '"+barrio+"', '"+estado+"', '"+observacion+"', 'A', '"+ciudad+"')";
   		registro = conn.ejecutar(query);
		if (registro == true){
			Icon icono = new ImageIcon(Cliente.class.getResource("/img/listo.png"));
			JOptionPane.showMessageDialog(null, "Registro de Cliente realizado con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		return registro;
	}
	
	/* Edita el registro del cliente
	 * @param id_cliente id del cliente
	 * @return retorna si la modificacion se realizo con exito
	 */
	public boolean Editar(String codigo) throws SQLException{
		String fecha;
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		fecha = "null";
   		if(fecha_nac != null){
   			fecha = "'"+formatter.format(fecha_nac)+"'";
   		}
		String query = "UPDATE clientes SET  "
				+ "tipo = '"+tipo+"', "
				+ "documento = '"+documento+"', "
				+ "cpf = '"+cpf+"', "
				+ "nombre = '"+nombre+"', "
				+ "apellido = '"+apellido+"', "
				+ "fecha_nac = "+fecha+", "
				+ "profesion = '"+profesion+"', "
				+ "pais = '"+pais+"' ,"
				+ "celular = '"+celular+"', "
				+ "num_trabajo = '"+n_tabajo+"', "
				+ "telefono = '"+telefono+"', "
				+ "email = '"+correo+"', "
				+ "cep = '"+cep+"', "
				+ "lugar = '"+lugar+"',"
				+ "numero = '"+numero+"', "
				+ "barrio = '"+barrio+"', "
				+ "ciudad = '"+ciudad+"' ,"
				+ "estado = '"+estado+"' ,"
				+ "observacion = '"+observacion+"' "
				+ "WHERE codigo = '"+codigo+"'";
		boolean status = conn.ejecutar(query);
		if (status == true){
			Icon icono = new ImageIcon(Cliente.class.getResource("/img/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		return status;
	}
	
	/* Trae un registro segun la primary key
	 * @param id_cliente id del cliente
	 */
	public void TraerRegistro(String codigo) throws SQLException{ 
		Magician mago = new Magician();
		String condicion = "";
		if(mago.isNumeric(codigo)){
			condicion = "OR serial = '"+codigo+"'";
		}else{
			condicion = "OR serial = (SELECT serial FROM clientes WHERE codigo = '"+codigo+"')";
		}
		String query ="SELECT codigo, tipo, documento, cpf, nombre, apellido, fecha_nac, profesion, pais, celular, num_trabajo, telefono, email, cep, lugar, numero, barrio, ciudad, estado, observacion "
				    + "FROM clientes "
				    + "WHERE codigo = '"+codigo+"'"
					+ condicion;
		ResultSet buscar = conn.Consulta(query);
		if(buscar.next()) {
			this.codigo = buscar.getString("codigo");
			this.tipo = buscar.getString("tipo");
			this.documento = buscar.getString("documento");
			this.cpf = buscar.getString("cpf");
			this.nombre = buscar.getString("nombre");
			this.apellido = buscar.getString("apellido");
			this.fecha_nac = buscar.getDate("fecha_nac");
			this.profesion = buscar.getString("profesion");
			this.pais = buscar.getString("pais");
			this.celular = buscar.getString("celular");
			this.n_tabajo = buscar.getString("num_trabajo");
			this.telefono = buscar.getString("telefono");
			this.correo = buscar.getString("email");
			this.cep = buscar.getString("cep");
			this.lugar = buscar.getString("lugar");
			this.numero = buscar.getString("numero");
			this.barrio = buscar.getString("barrio");
			this.ciudad = buscar.getString("ciudad");
			this.estado = buscar.getString("estado");
			this.observacion = buscar.getString("observacion");
			buscar.close();
		}
	}
	
	/* Cambia el estado del cliente
	 * @param id_cliente id del cliente
	 * @return estado del cliente
	 */
	public String CambiarEstado(String codigo) throws SQLException{ 
		String estado = null;
		String busca_estado = "SELECT status FROM clientes WHERE codigo ='"+codigo+"'";
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("status");
			busca.close();
		}
		if(estado.equals("I")){
			String query = "UPDATE clientes SET status = 'A' WHERE codigo ='"+codigo+"'";
			Icon icono = new ImageIcon(Cliente.class.getResource("/img/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de activar al Cliente?","Activar Cliente",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Cliente.class.getResource("/img/listo.png"));
				JOptionPane.showMessageDialog(null, "Cliente activado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "A";
	        }else{
	        	estado = "I";
	        }	
		}else if(estado.equals("A")){
			String query = "UPDATE clientes SET status = 'I' WHERE codigo ='"+codigo+"'";
			Icon icono = new ImageIcon(Cliente.class.getResource("/img/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de desactivar al Cliente?","Desactivar Cliente",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Cliente.class.getResource("/img/listo.png"));
				JOptionPane.showMessageDialog(null, "Cliente desactivado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "I";
			}else{
	        	estado = "A";
	        }	
		}
		return estado;
	}
	
	/* Valida si el RIF se necuentra registrado
	 * @param rif RIF del cliente
	 * @return cantidas de filas de la consulta
	 */
	public boolean ValidarRif(String rif) throws SQLException{
		boolean rows = false;
		String query = "SELECT * FROM ads_clientes WHERE rif = '"+rif+"'";
		ResultSet rs = conn.Consulta(query);
		if(conn.getRowsCount(rs) != 0){
			rows = true;
		}
		return rows;
	}
	
	/* Lista todos los clientes registrados 
	 * @return lista de clientes registrados
	 */
	public ResultSet ListarClientes() throws SQLException{ 
		this.conn = conn;
		String query = "SELECT codigo, tipo, documento, cpf, nombre, apellido, pais, status "
				     + "FROM clientes "
				     + "ORDER BY codigo";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Lista todos los clientes registrados 
	 * @return lista de clientes registrados
	 * @note la consulta de esta metodo es mas corto en el SELECT
	 */
	public ResultSet ListarClientesResumen() throws SQLException{ 
		this.conn = conn;
		String query = "SELECT codigo, tipo, documento,  nombre, apellido "
				     + "FROM clientes "
				     + "ORDER BY codigo";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra los clientes 
	 * @param argumento razon social del cliente
	 * @return lista de clientes filtrados
	 */
	public ResultSet FiltraClientes(String argumento) throws SQLException{ 
		this.conn = conn;
		String query = "SELECT codigo, tipo, documento, cpf, nombre, apellido, pais, status "
			     + "FROM clientes "
				 +" WHERE nombre ILIKE '%"+argumento+"%'"
				 + "OR apellido ILIKE '%"+argumento+"%' "
				 + "OR documento ILIKE '%"+argumento+"%' "
			     + "ORDER BY codigo";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra los clientes 
	 * @param argumento razon social del cliente
	 * @return lista de clientes filtrados
	 * @note la consulta de esta metodo es mas corto en el SELECT
	 */
	public ResultSet FiltraClientesResumen(String argumento) throws SQLException{ 
		this.conn = conn;
		String query = "SELECT codigo, tipo, documento,  nombre, apellido "
			     + "FROM clientes "
				 +" WHERE nombre ILIKE '%"+argumento+"%'"
				 + "OR apellido ILIKE '%"+argumento+"%' "
				 + "OR documento ILIKE '%"+argumento+"%' "
			     + "ORDER BY codigo";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Retorna el id del cliente segun el RIF
	 * @param rif RIF del cliente
	 * @return id del cliente
	 */
	public int RetornaID(String codigo) throws SQLException{
		int id = 0;
		String query = "SELECT serial FROM clientes WHERE codigo = '"+codigo+"'";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			id = rs.getInt("serial");
			rs.close();
		}
		return id;
	}
	
	public int RetornaSerie() throws SQLException{
		int nro = 0;
		String query = "SELECT MAX(serial) as nro FROM clientes";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			nro = rs.getInt("nro");
			rs.close();
		}
		return nro;
	}
	
	public boolean ValidarCliente(String codigo) throws SQLException{
		boolean rows = false;
		String query = "SELECT * FROM clientes WHERE codigo = '"+codigo+"'";
		ResultSet rs = conn.Consulta(query);
		if(conn.getRowsCount(rs) != 0){
			rows = true;
		}
		return rows;
	}
}

