package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import utility.Conexion;
import utility.Magician;
import utility.Tables;

/**
 * Clase para el manejo de Beneficiarios
 * @author Noel P�rez
 */

public class Beneficiario {
	private String codigo;
	private String tipo;
	private String documento;
	private String cpf;
	private String nombre;
	private String apellido;
	private Date fecha_nac;
	private String profesion;
	private String pais;
	private String celular;
	private String telefono;
	private String correo;
	private Conexion conn;
	
	public Conexion getConn() {
		return conn;
	}

	public void setConn(Conexion conn) {
		this.conn = conn;
	}

	public Beneficiario(){}
	
	public Beneficiario(String codigo, String tipo, String documento, String cpf, String nombre,String apellido,Date fecha_nac, String profesion, String pais, String celular,  String telefono, String correo){ 
		this.codigo = codigo;
		this.tipo = tipo;
		this.documento = documento;
		this.cpf = cpf;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fecha_nac = fecha_nac;
		this.profesion = profesion;
		this.pais = pais;
		this.celular = celular;
		this.telefono = telefono;
		this.correo = correo;
	}
	

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Date getFecha_nac() {
		return fecha_nac;
	}

	public void setFecha_nac(Date fecha_nac) {
		this.fecha_nac = fecha_nac;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}


	/* Desconecta la variable de conexion creada por el objeto en la llmada del metodo
	 */
	public void desconectar() throws SQLException{ 
		conn.desConectar();
	}
	
	/* Registra un nuevo Beneficiario
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{//Registra un nuevo cliente
		boolean registro = false;
   		String fecha;
   		SimpleDateFormat formatter;
   		formatter = new SimpleDateFormat("yyyy-MM-dd");
   		fecha = "null";
   		if(fecha_nac != null){
   			fecha = "'"+formatter.format(fecha_nac)+"'";
   		}
   		String query = "INSERT INTO beneficiarios (codigo, tipo, documento, cpf, nombre, apellido, fecha_nac, profesion, pais, celular, telefono, email, status) "
   				     + "VALUES ('"+codigo+"', '"+tipo+"', '"+documento+"', '"+cpf+"', '"+nombre+"', '"+apellido+"', "+fecha+", '"+profesion+"', '"+pais+"', '"+celular+"', '"+telefono+"', '"+correo+"', 'A')";
   		registro = conn.ejecutar(query);
		if (registro == true){
			Icon icono = new ImageIcon(Beneficiario.class.getResource("/img/listo.png"));
			JOptionPane.showMessageDialog(null, "Registro de Beneficiario realizado con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		return registro;
	}
	
	/* Edita el registro del beneficiario
	 * @param codigo id del beneficiario
	 * @return retorna si la modificacion se realizo con exito
	 */
	public boolean Editar(String codigo) throws SQLException{
		String fecha;
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		fecha = "null";
   		if(fecha_nac != null){
   			fecha = "'"+formatter.format(fecha_nac)+"'";
   		}
		String query = "UPDATE beneficiarios SET  "
				+ "tipo = '"+tipo+"', "
				+ "documento = '"+documento+"', "
				+ "cpf = '"+cpf+"', "
				+ "nombre = '"+nombre+"', "
				+ "apellido = '"+apellido+"', "
				+ "fecha_nac = "+fecha+", "
				+ "profesion = '"+profesion+"', "
				+ "pais = '"+pais+"', "
				+ "celular = '"+celular+"', "
				+ "telefono = '"+telefono+"', "
				+ "email = '"+correo+"' "
				+ "WHERE codigo = '"+codigo+"'";
		boolean status = conn.ejecutar(query);
		if (status == true){
			Icon icono = new ImageIcon(Beneficiario.class.getResource("/img/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		return status;
	}
	
	/* Trae un registro segun la primary key
	 * @param codigo id del beneficiario
	 */
	public void TraerRegistro(String codigo) throws SQLException{ 
		Magician mago = new Magician();
		String condicion = "";
		if(mago.isNumeric(codigo)){
			condicion = "OR serial = '"+codigo+"'";
		}else{
			condicion = "OR serial = (SELECT serial FROM beneficiarios WHERE codigo = '"+codigo+"')";
		}
		String query ="SELECT codigo, tipo, documento, cpf, nombre, apellido, fecha_nac, profesion, pais, celular, telefono, email "
				    + "FROM beneficiarios "
				    + "WHERE codigo = '"+codigo+"'"
					+ condicion;
		ResultSet buscar = conn.Consulta(query);
		if(buscar.next()) {
			this.codigo = buscar.getString("codigo");
			this.tipo = buscar.getString("tipo");
			this.documento = buscar.getString("documento");
			this.cpf = buscar.getString("cpf");
			this.nombre = buscar.getString("nombre");
			this.apellido = buscar.getString("apellido");
			this.fecha_nac = buscar.getDate("fecha_nac");
			this.profesion = buscar.getString("profesion");
			this.pais = buscar.getString("pais");
			this.celular = buscar.getString("celular");
			this.telefono = buscar.getString("telefono");
			this.correo = buscar.getString("email");
			buscar.close();
		}
	}
	
	/* Cambia el estado del cliente
	 * @param id_cliente id del cliente
	 * @return estado del cliente
	 */
	public String CambiarEstado(String codigo) throws SQLException{ 
		String estado = null;
		String busca_estado = "SELECT status FROM beneficiarios WHERE codigo ='"+codigo+"'";
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("status");
			busca.close();
		}
		if(estado.equals("I")){
			String query = "UPDATE beneficiarios SET status = 'A' WHERE codigo = '"+codigo+"'";
			Icon icono = new ImageIcon(Beneficiario.class.getResource("/img/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de activar al Beneficiario?","Activar Cliente",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Beneficiario.class.getResource("/img/listo.png"));
				JOptionPane.showMessageDialog(null, "Beneficiario activado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "A";
	        }else{
	        	estado = "I";
	        }	
		}else if(estado.equals("A")){
			String query = "UPDATE beneficiarios SET status = 'I' WHERE codigo= '"+codigo+"'";
			Icon icono = new ImageIcon(Beneficiario.class.getResource("/img/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de desactivar al Beneficiario?","Desactivar Cliente",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Beneficiario.class.getResource("/img/listo.png"));
				JOptionPane.showMessageDialog(null, "Beneficiario desactivado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "I";
			}else{
	        	estado = "A";
	        }	
		}
		return estado;
	}
	

	/* Lista todos los clientes registrados 
	 * @return lista de clientes registrados
	 */
	public ResultSet ListarBeneficiarios() throws SQLException{ 
		String query = "SELECT codigo, tipo, documento, cpf, nombre, apellido, pais, status "
				     + "FROM beneficiarios "
				     + "ORDER BY codigo";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Lista todos los clientes registrados 
	 * @return lista de clientes registrados
	 * @note la consulta de esta metodo es mas corto en el SELECT
	 */
	public ResultSet ListarBeneficiariosResumen() throws SQLException{ 
		String query = "SELECT codigo, tipo, documento,  nombre, apellido "
				     + "FROM beneficiarios "
				     + "ORDER BY codigo";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra los beneficiarios 
	 * @param argumento razon social del cliente
	 * @return lista de clientes filtrados
	 */
	public ResultSet FiltraBeneficiarios(String argumento) throws SQLException{ 
		String query = "SELECT codigo, tipo, documento, cpf, nombre, apellido, pais, status "
			         + "FROM beneficiarios "
			         +" WHERE nombre ILIKE '%"+argumento+"%'"
			         + "OR apellido ILIKE '%"+argumento+"%' "
			         + "OR documento ILIKE '%"+argumento+"%' "
			         + "ORDER BY codigo";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra los beneficiarios 
	 * @param argumento razon social del cliente
	 * @return lista de clientes filtrados
	 * @note la consulta de esta metodo es mas corto en el SELECT
	 */
	public ResultSet FiltraBeneficiariosResumen(String argumento) throws SQLException{ 
		String query = "SELECT codigo, tipo, documento,  nombre, apellido "
			     	 + "FROM beneficiarios "
			     	 +" WHERE nombre ILIKE '%"+argumento+"%'"
			     	 + "OR apellido ILIKE '%"+argumento+"%' "
			     	 + "OR documento ILIKE '%"+argumento+"%' "
			     	 + "ORDER BY codigo";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	public int RetornaID(String codigo) throws SQLException{
		int id = 0;
		String query = "SELECT serial FROM beneficiarios WHERE codigo = '"+codigo+"'";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			id = rs.getInt("serial");
			rs.close();
		}
		return id;
	}
	
	public int RetornaSerie() throws SQLException{
		int nro = 0;
		String query = "SELECT MAX(serial) as nro FROM beneficiarios";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			nro = rs.getInt("nro");
			rs.close();
		}
		return nro;
	}
	
	public boolean ValidarBeneficiario(String codigo) throws SQLException{
		boolean rows = false;
		String query = "SELECT * FROM beneficiarios WHERE codigo = '"+codigo+"'";
		ResultSet rs = conn.Consulta(query);
		if(conn.getRowsCount(rs) != 0){
			rows = true;
		}
		return rows;
	}
	
	public void RegistrarCuentasBancarias(Tables cuentas, int id_beneficiario) throws SQLException{
		for(int i = 0; i < cuentas.getRowCount(); i++){
			String query = "INSERT INTO cuentas_bancarias (id_banco, tipo_cuenta, cuenta, id_beneficiario, status) VALUES ('"+cuentas.getValueAt(i, 0).toString()+"', '"+cuentas.getValueAt(i, 4).toString()+"','"+cuentas.getValueAt(i, 3).toString()+"', '"+id_beneficiario+"','A')";
			conn.ejecutar(query);
		}
	}
	
	public void RegistrarCuentaBancaria(Integer id_banco,String tipo_cuenta, String cuenta, String id_beneficiario) throws SQLException{
		String query = "INSERT INTO cuentas_bancarias (id_banco, tipo_cuenta, cuenta, id_beneficiario, status) VALUES ('"+id_banco+"', '"+tipo_cuenta+"','"+cuenta+"', '"+id_beneficiario+"', 'A')";
		conn.ejecutar(query);
	}
	
	public void EditarCuentaBancaria(String id_cuenta, Integer id_banco,String tipo_cuenta, String cuenta) throws SQLException{
		String query = "UPDATE cuentas_bancarias SET  "
				+ "id_banco = '"+id_banco+"', "
				+ "tipo_cuenta = '"+tipo_cuenta+"', "
				+ "cuenta = '"+cuenta+"' "		
				+ "WHERE id = '"+id_cuenta+"'";
		boolean status = conn.ejecutar(query);
		if (status == true){
			Icon icono = new ImageIcon(Beneficiario.class.getResource("/img/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		conn.ejecutar(query);
	}
	
	public void CambiarEstadoCuenta(String codigo) throws SQLException{ 
		String estado = null;
		String busca_estado = "SELECT status FROM cuentas_bancarias WHERE id ='"+codigo+"'";
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("status");
			busca.close();
		}
		if(estado.equals("I")){
			String query = "UPDATE cuentas_bancarias SET status = 'A' WHERE id = "+codigo;
			Icon icono = new ImageIcon(Beneficiario.class.getResource("/img/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de Activar la Cuenta Bancaria?","Acivar Cuenta Bancaria",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Beneficiario.class.getResource("/img/listo.png"));
				JOptionPane.showMessageDialog(null, "Cuenta Bancaria Activada", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "A";
	        }else{
	        	estado = "I";
	        }	
		}else if(estado.equals("A")){
			String query = "UPDATE cuentas_bancarias SET status = 'I' WHERE id = "+codigo;
			Icon icono = new ImageIcon(Beneficiario.class.getResource("/img/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de Borrar la Cuenta Bancaria?","Borrar Cuenta Bancaria",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Beneficiario.class.getResource("/img/listo.png"));
				JOptionPane.showMessageDialog(null, "Cuenta Bancaria Eliminada", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "I";
	        }else{
	        	estado = "A";
	        }	
		}
	}
	
	public ResultSet ListarCuentas(String id_beneficiario) throws SQLException{ 
		String query = "SELECT cuentas_bancarias.id, bancos.pais, bancos.nombre, cuentas_bancarias.cuenta, cuentas_bancarias.tipo_cuenta "+
				       "FROM cuentas_bancarias "+
                       "JOIN bancos ON bancos.id = id_banco "+
                       "WHERE id_beneficiario = "+id_beneficiario+" "+
                       "AND cuentas_bancarias.status = 'A'"+
                       "ORDER BY cuentas_bancarias.id ";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	public ResultSet FiltraCuentas(String argumento, String id_beneficiario) throws SQLException{ 
		String query = "SELECT cuentas_bancarias.id, bancos.pais, bancos.nombre, cuentas_bancarias.cuenta, cuentas_bancarias.tipo_cuenta "+
			           "FROM cuentas_bancarias "+
                       "JOIN bancos ON bancos.id = id_banco "+
                       "WHERE id_beneficiario = "+id_beneficiario+" "+
                       "AND (bancos.pais ILIKE '%"+argumento+"%' "+
                       "OR bancos.nombre ILIKE '%"+argumento+"%' "+
                       "OR cuentas_bancarias.cuenta ILIKE '%"+argumento+"%' "+
                       "OR cuentas_bancarias.tipo_cuenta ILIKE '%"+argumento+"%') "+
                       "AND cuentas_bancarias.status = 'A'"+
                       "ORDER BY cuentas_bancarias.id ";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	public String[] TraerRegistroCuenta(String id_cuenta) throws SQLException{
		String[] vector = new String[6];
		String query = "SELECT cuentas_bancarias.id, bancos.pais, bancos.nombre, cuentas_bancarias.cuenta, cuentas_bancarias.tipo_cuenta, cuentas_bancarias.id_banco "+
				       "FROM cuentas_bancarias "+
                       "JOIN bancos ON bancos.id = id_banco "+
                       "WHERE cuentas_bancarias.id = "+id_cuenta+" ";
		ResultSet buscar = conn.Consulta(query);
		if(buscar.next()) {
			vector[0] = buscar.getString("id");
			vector[1] = buscar.getString("pais");
			vector[2] = buscar.getString("nombre");
			vector[3] = buscar.getString("cuenta");
			vector[4] = buscar.getString("tipo_cuenta");
			vector[5] = buscar.getString("id_banco");
			buscar.close();
		}
		return vector;
	}
}
