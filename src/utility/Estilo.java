package utility;
import java.awt.Color;

import javax.swing.plaf.*;
import javax.swing.plaf.metal.*;

public class Estilo  extends DefaultMetalTheme {
   public String getName() { return "Horus"; }
   
   private final ColorUIResource primary1 = new ColorUIResource(76, 76, 76); //Color de Borde de linea de Menus y de la fuente de carga
   private final ColorUIResource primary2 = new ColorUIResource(201, 201, 201); //Color del fondo de los item del menu
   private final ColorUIResource primary3 = new ColorUIResource(133, 133, 133); //Color elemento seleccionado de la tabla

   private final ColorUIResource secondary1 = new ColorUIResource( 76,  76,  76); //Color de borde de elementos
   private final ColorUIResource secondary2 = new ColorUIResource(76, 76, 76); //Color de borde de las pantallas y boton presionado
   private final ColorUIResource secondary3 = new ColorUIResource(251, 251, 251); //Color de fondo del sistema

   private final ColorUIResource black = new ColorUIResource(0, 0, 0);
   private final ColorUIResource white = new ColorUIResource(255, 255, 255);
   
   private Color principal = new Color(52,165,197); //Color comun de los botones
   private Color secundario = new Color(26,26,26); //Color del hover de los botones
   private Color terciario = new Color(108, 175, 72); //Color de los botones de las tablas
   
   //No invertir los colores blanco y negro ya que se modificaran los colores de las cajas de texto
   
   protected ColorUIResource getPrimary1() { return primary1; }
   protected ColorUIResource getPrimary2() { return primary2; }
   protected ColorUIResource getPrimary3() { return primary3; }

   protected ColorUIResource getSecondary1() { return secondary1; }
   protected ColorUIResource getSecondary2() { return secondary2; }
   protected ColorUIResource getSecondary3() { return secondary3; }
   
   protected ColorUIResource getBlack() { return black; }
   protected ColorUIResource getWhite() { return white; }
   
   public Color getPrincipal(){ //Retorna el color para los botones
	   return principal;
   }
   
   public Color getSecundario(){//Retorna le color para el hover de los botones
	   return secundario;
   }
   
   public Color getTerciario(){
	   return terciario;
   }
}


	

