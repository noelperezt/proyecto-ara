package utility;

import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;

import views.Escritorio;

@SuppressWarnings("serial")
public class DesktopBackground extends JDesktopPane {
	
	 private static java.awt.Image IMG=new ImageIcon(Escritorio.class.getResource("/img/background.jpg")).getImage();

    public void paintChildren(java.awt.Graphics g){
        g.drawImage(IMG, 0, 0, getWidth(), getHeight(), this);
        super.paintChildren(g);
    }   
}
