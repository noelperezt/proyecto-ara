package utility;

import java.io.IOException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;


public class Calculadora {
	String osName = System.getProperty("os.name").toLowerCase();
	Runtime obj = Runtime.getRuntime();
	
	public Calculadora(){}
	
	public void Ejecutar(){
		if(osName.indexOf("win") >= 0) {
			try {
				obj.exec("C:\\WINDOWS\\system32\\CALC.EXE");
			} catch (IOException e) {
				 Icon icono = new ImageIcon(Calculadora.class.getResource("/img/error.png"));
		    	  JOptionPane.showMessageDialog(null, "No disponible para su Sistema Operativo","Error" , JOptionPane.ERROR_MESSAGE, icono);
				e.printStackTrace();
			}
		}
		 
		else if(osName.indexOf("mac") >= 0) {
			// codigo para Mac
			Icon icono = new ImageIcon(Calculadora.class.getResource("/img/error.png"));
			JOptionPane.showMessageDialog(null, "No disponible para su Sistema Operativo","Error" , JOptionPane.ERROR_MESSAGE, icono);
		}
		 
		else if(osName.indexOf("nux") >= 0) {
			try {
				obj.exec("/usr/bin/gnome-calculator");
			} catch (IOException e) {
				Icon icono = new ImageIcon(Calculadora.class.getResource("/img/error.png"));
				JOptionPane.showMessageDialog(null, "No disponible para su Sistema Operativo","Error" , JOptionPane.ERROR_MESSAGE, icono);
				e.printStackTrace();
			}
		}
	}
}
