package utility;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import models.Banco;
import models.Beneficiario;
import models.Cliente;
import models.Moneda;
import models.Solicitud;
import models.Usuario;
import views.AdministrarBeneficiarios;
import views.AdministrarClientes;
import views.RegistroBeneficiarios;
import views.RegistroClientes;

@SuppressWarnings("serial")
public class Tables extends JTable {

	public DefaultTableModel modelo;
	public int fila;
	public int columna;
	public String ruta = new File ("").getAbsolutePath ();
	private String[] nombre_columnas;
	private int[] columnas_editables;
	private int[] ancho_columnas;
	Estilo tema = new Estilo();
	private Conexion conn;
	
	public Conexion getConn() {
		return conn;
	}

	public void setConn(Conexion conn) {
		this.conn = conn;
	}

	public Tables(){
	}

	public Tables(String[] nombre_columnas, int[] editable, int[] anchos , @SuppressWarnings("rawtypes") Class[] datos){
		
		setNombre_columnas(nombre_columnas);
		setColumnas_editables(editable);
		setAnchos_Columnas(anchos);
		
		this.setModel(new DefaultTableModel(null,nombre_columnas) {
			@SuppressWarnings("rawtypes")
			Class[] tipos = datos;

			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public Class getColumnClass(int columnIndex) {
				return tipos[columnIndex];
			}

			@Override
			public boolean isCellEditable (int fila, int iCols) {
				try{ 
					for(int z = 0; z < columnas_editables.length; z++){ 
						if(iCols==columnas_editables[z]){ 
							return true; 
						}	
					}	
				}catch(java.lang.NullPointerException e){ 
					return false; 
				} 
				return false;
			}
		});
		backgroundHeader();
		setResizeColumn();
	}
	

	private void backgroundHeader(){
		getTableHeader().setForeground(tema.getPrincipal());
		getTableHeader().setFont(new Font("Arial", Font.BOLD, 11)); 
	}
	
	public void setAnchos_Columnas(int[] anchos){
		ancho_columnas = anchos;
	}
	
	public void setResizeColumn(){
		for(int i = 0; i < ancho_columnas.length; i++) {
			getColumnModel().getColumn(i).setPreferredWidth(ancho_columnas[i]);
		}	
	}
	
	public String[] getNombre_columnas(){
		return nombre_columnas;
	}
	
	public void setNombre_columnas(String[] columnas){
		nombre_columnas = columnas;
	}
	
	public int[] getColumnas_editables(){
		return columnas_editables;
	}
	
	public void setColumnas_editables(int[] editables){
		columnas_editables = editables;
	}
	
	public void addRow(){
		Object[] col={};
		modelo = (DefaultTableModel) getModel();
		modelo.addRow(col);
	}
	
	public void addRow(Object[] fila){
		modelo = (DefaultTableModel) getModel();
		modelo.addRow(fila);
	}
	
	public void deleteRow(int RowSelected){
		modelo = (DefaultTableModel) getModel();
		modelo.removeRow(RowSelected);
	}

	public void setClearGrid(){
		int x;
		DefaultTableModel newModelTable = new DefaultTableModel();
		newModelTable = (DefaultTableModel) getModel();
		for (x =  newModelTable.getRowCount()-1; x>=0;x--){
			newModelTable.removeRow(x);
		}
	} 

	public Object getValueAt(int Row, int Col) {
		modelo = (DefaultTableModel) getModel();
		return  modelo.getValueAt(Row, Col);
	} 

	public void setValueAt(Object var, int Row, int Col){
		modelo = (DefaultTableModel) getModel();
		modelo.setValueAt(var, Row, Col);
		modelo.fireTableDataChanged();
	
	}
	
	@SuppressWarnings({ "unchecked" })
	public void PrimeraColumnaBoolean(ResultSet rs, boolean estado){
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		@SuppressWarnings("rawtypes")
		ArrayList datosTemp = new ArrayList<>();

		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;
		int adicional = 1;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[adicional+numeroColumnas];
			tiposColumnas = new Class[adicional+numeroColumnas];
			
			for (int i = 0; i < adicional; i++){
				columnas[i] = "";
				tiposColumnas[i] = java.lang.Boolean.class;
			}
			for (int i = 0; i < numeroColumnas; i++){
				columnas[adicional+i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[adicional+i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for(int i = 0; i < adicional; i++){
					fila[i] = estado;
				}
				for (int i = 0; i < numeroColumnas; i++) 
					fila[adicional+i] = rsUsuarios.getObject(i + 1);
				
				datosTemp.add(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
		@SuppressWarnings("rawtypes")
		final Class[] tiposColumnasFinal = tiposColumnas;
		Object[][] datos = new Object[datosTemp.size()][columnas.length];
		datosTemp.toArray(datos);

		this.setModel(new DefaultTableModel(datos,nombre_columnas) {
			@SuppressWarnings("rawtypes")
			Class[] tipos = tiposColumnasFinal;

			@SuppressWarnings("rawtypes")
			@Override
			public Class getColumnClass(int columnIndex) {
				return tipos[columnIndex];
			}

			@Override
			public boolean isCellEditable (int fila, int iCols) {
				try{ 
					for(int z = 0; z < columnas_editables.length; z++){ 
						if(iCols==columnas_editables[z]){ 
							return true; 
						}	
					}	
				}catch(java.lang.NullPointerException e){ 
					return false; 
				} 
				return false;
			}
		});
		setResizeColumn();
	}
	
	public void CambioEstadoCliente(String cliente,int fila, int columna) throws SQLException{
		Cliente Ocliente = new Cliente();
		Ocliente.setConn(conn);
		setValueAt(Ocliente.CambiarEstado(cliente), fila, columna);
	}
	
	public void CambioEstadoBeneficiario(String beneficiario,int fila, int columna) throws SQLException{
		Beneficiario Obeneficiario = new Beneficiario();
		Obeneficiario.setConn(conn);
		setValueAt(Obeneficiario.CambiarEstado(beneficiario), fila, columna);
	}
	
	public void CambioEstadoBanco(int banco,int fila, int columna) throws SQLException{
		Banco Obanco = new Banco();
		Obanco.setConn(conn);
		setValueAt(Obanco.CambiarEstado(banco), fila, columna);
	}
	
	public void CambioEstadoMoneda(int moneda,int fila, int columna) throws SQLException{
		Moneda Omoneda = new Moneda();
		Omoneda.setConn(conn);
		setValueAt(Omoneda.CambiarEstado(moneda), fila, columna);
	}
	
	public void CambioEstadoSolicitud(int solicitud,int fila, int columna) throws SQLException{
		Solicitud Osolicitud = new Solicitud();
		Osolicitud.setConn(conn);
		setValueAt(Osolicitud.CambiarEstado(solicitud), fila, columna);
	}
	
	@SuppressWarnings({ "unchecked" })
	public void UltimaColumnaBoolean(ResultSet rs, boolean estado){
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		@SuppressWarnings("rawtypes")
		ArrayList datosTemp = new ArrayList<>();

		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;
		int adicional = 1;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[numeroColumnas+adicional];
			tiposColumnas = new Class[numeroColumnas+adicional];
			
			for (int i = 0; i < numeroColumnas; i++){
				columnas[i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}
			for (int i = 0; i < adicional; i++){
				columnas[numeroColumnas+i] = "";
				tiposColumnas[numeroColumnas+i] = java.lang.Boolean.class;
			}
			

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for (int i = 0; i < numeroColumnas; i++) 
					fila[i] = rsUsuarios.getObject(i + 1);
				
				for(int i = 0; i < adicional; i++){
					fila[numeroColumnas+i] = estado;
				}
				datosTemp.add(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
		@SuppressWarnings("rawtypes")
		final Class[] tiposColumnasFinal = tiposColumnas;
		Object[][] datos = new Object[datosTemp.size()][columnas.length];
		datosTemp.toArray(datos);

		this.setModel(new DefaultTableModel(datos,nombre_columnas) {
			@SuppressWarnings("rawtypes")
			Class[] tipos = tiposColumnasFinal;

			@SuppressWarnings("rawtypes")
			@Override
			public Class getColumnClass(int columnIndex) {
				return tipos[columnIndex];
			}

			@Override
			public boolean isCellEditable (int fila, int iCols) {
				try{ 
					for(int z = 0; z < columnas_editables.length; z++){ 
						if(iCols==columnas_editables[z]){ 
							return true; 
						}	
					}	
				}catch(java.lang.NullPointerException e){ 
					return false; 
				} 
				return false;
			}
		});
		setResizeColumn();
	}
	
	@SuppressWarnings("unchecked")
	public void Listar(ResultSet rs){
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		@SuppressWarnings("rawtypes")
		ArrayList datosTemp = new ArrayList<>();

		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[numeroColumnas];
			tiposColumnas = new Class[numeroColumnas];
			
			for (int i = 0; i < numeroColumnas; i++){
				columnas[i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for (int i = 0; i < numeroColumnas; i++) 
					fila[i] = rsUsuarios.getObject(i + 1);
				datosTemp.add(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
		@SuppressWarnings("rawtypes")
		final Class[] tiposColumnasFinal = tiposColumnas;
		Object[][] datos = new Object[datosTemp.size()][columnas.length];
		datosTemp.toArray(datos);

		this.setModel(new DefaultTableModel(datos,nombre_columnas) {
			@SuppressWarnings("rawtypes")
			Class[] tipos = tiposColumnasFinal;

			@SuppressWarnings("rawtypes")
			@Override
			public Class getColumnClass(int columnIndex) {
				return tipos[columnIndex];
			}

			@Override
			public boolean isCellEditable (int fila, int iCols) {
				try{ 
					for(int z = 0; z < columnas_editables.length; z++){ 
						if(iCols==columnas_editables[z]){ 
							return true; 
						}	
					}	
				}catch(java.lang.NullPointerException e){ 
					return false; 
				} 
				return false;
			}
		});
		setResizeColumn();
	}
	
	@SuppressWarnings("unchecked")
	public void Listar(ResultSet rs, @SuppressWarnings("rawtypes") Class[] tiposColumnas){
		String[] columnas = nombre_columnas;

		@SuppressWarnings("rawtypes")
		ArrayList datosTemp = new ArrayList<>();

		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[numeroColumnas];

		} catch (SQLException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for (int i = 0; i < numeroColumnas; i++) 
					fila[i] = rsUsuarios.getObject(i + 1);
				datosTemp.add(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
		@SuppressWarnings("rawtypes")
		final Class[] tiposColumnasFinal = tiposColumnas;
		Object[][] datos = new Object[datosTemp.size()][columnas.length];
		datosTemp.toArray(datos);

		this.setModel(new DefaultTableModel(datos,nombre_columnas) {
			@SuppressWarnings("rawtypes")
			Class[] tipos = tiposColumnasFinal;

			@SuppressWarnings("rawtypes")
			@Override
			public Class getColumnClass(int columnIndex) {
				return tipos[columnIndex];
			}

			@Override
			public boolean isCellEditable (int fila, int iCols) {
				try{ 
					for(int z = 0; z < columnas_editables.length; z++){ 
						if(iCols==columnas_editables[z]){ 
							return true; 
						}	
					}	
				}catch(java.lang.NullPointerException e){ 
					return false; 
				} 
				return false;
			}
		});
		setResizeColumn();
	}
	
	
	public void CambioEstadoUsuario(int usuario,int fila, int columna) throws SQLException{
		Usuario Ousuario = new Usuario();
		Ousuario.setConn(conn);
		setValueAt(Ousuario.CambiarEstado(usuario), fila, columna);
	}
	
	
	public void RefreshTableButtomDouble(ResultSet rs){
		String[] botones = new String[] {
				"Editar",
				"Inhabilitar",
		};
			
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;
		int numeroBotones = botones.length;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[numeroColumnas + numeroBotones];
			tiposColumnas = new Class[numeroColumnas + numeroBotones];
			
			for (int i = 0; i < numeroColumnas; i++){
				columnas[i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

			for (int i = 0; i < botones.length; i++){
				columnas[numeroColumnas + i] = botones[i];
				tiposColumnas[numeroColumnas + i] = JButton.class;
			}

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for (int i = 0; i < numeroColumnas; i++) 
					fila[i] = rsUsuarios.getObject(i + 1);
				for (int j = 0; j < botones.length; j++){
					JButton boton = new JButton("");
					Estilo tema = new Estilo();
					boton.setBackground(tema.getTerciario());
					boton.setBorderPainted(false);
					if (botones[j].equals("Editar")){
						boton.setIcon(new ImageIcon(Tables.class.getResource("/img/ico_editar.png")));
						boton.setToolTipText("Editar");
					}else{
						boton.setIcon(new ImageIcon(Tables.class.getResource("/img/ico_etiqueta_azul.png")));
						boton.setToolTipText("Cambiar Estado");
					}
					fila[numeroColumnas + j] = boton;	
				}
				modelo.addRow(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
	}
	
	 
	public void Refresh(ResultSet rs){		
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[numeroColumnas ];
			tiposColumnas = new Class[numeroColumnas ];
			
			for (int i = 0; i < numeroColumnas; i++){
				columnas[i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

		} catch (SQLException | ClassNotFoundException e)
		{e.printStackTrace();}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for (int i = 0; i < numeroColumnas; i++) 
					fila[i] = rsUsuarios.getObject(i + 1);
				modelo.addRow(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{e.printStackTrace();}
	}

	public void RefreshFirstColumCheck(ResultSet rs, boolean estado){
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;
		int adicional = 1;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[adicional+numeroColumnas];
			tiposColumnas = new Class[adicional+numeroColumnas];
			
			for (int i = 0; i < adicional; i++){
				columnas[i] = "";
				tiposColumnas[i] = java.lang.Boolean.class;
			}
			for (int i = 0; i < numeroColumnas; i++){
				columnas[adicional+i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[adicional+i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for(int i = 0; i < adicional; i++){
					fila[i] = estado;
				}
				for (int i = 0; i < numeroColumnas; i++) 
					fila[adicional+i] = rsUsuarios.getObject(i + 1);
				
				modelo.addRow(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
	}
	
	@SuppressWarnings({ "unchecked" })
	public void TableButtomDouble(ResultSet rs) throws SQLException{
		String[] botones = new String[] {
				"Editar",
				"Inhabilitar",
		};
			
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		@SuppressWarnings("rawtypes")
		ArrayList datosTemp = new ArrayList<>();

		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;
		int numeroBotones = botones.length;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[numeroColumnas + numeroBotones];
			tiposColumnas = new Class[numeroColumnas + numeroBotones];
			
			for (int i = 0; i < numeroColumnas; i++){
				columnas[i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

			for (int i = 0; i < botones.length; i++){
				columnas[numeroColumnas + i] = botones[i];
				tiposColumnas[numeroColumnas + i] = JButton.class;
			}

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for (int i = 0; i < numeroColumnas; i++) 
					fila[i] = rsUsuarios.getObject(i + 1);
				for (int j = 0; j < botones.length; j++){
					JButton boton = new JButton("");
					Estilo tema = new Estilo();
					boton.setBorderPainted(false);
					boton.setBackground(tema.getTerciario());
					if (botones[j].equals("Editar")){
						boton.setIcon(new ImageIcon(Tables.class.getResource("/img/ico_editar.png")));
						boton.setToolTipText("Editar");
					}else{
						boton.setIcon(new ImageIcon(Tables.class.getResource("/img/ico_etiqueta_azul.png")));
						boton.setToolTipText("Cambiar Estado");
					}
					fila[numeroColumnas + j] = boton;	
				}
				datosTemp.add(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
		@SuppressWarnings("rawtypes")
		final Class[] tiposColumnasFinal = tiposColumnas;
		Object[][] datos = new Object[datosTemp.size()][columnas.length];
		datosTemp.toArray(datos);

		this.setModel(new DefaultTableModel(datos,nombre_columnas) {
			@SuppressWarnings("rawtypes")
			Class[] tipos = tiposColumnasFinal;

			@SuppressWarnings("rawtypes")
			@Override
			public Class getColumnClass(int columnIndex) {
				return tipos[columnIndex];
			}

			@Override
			public boolean isCellEditable (int fila, int iCols) {
				try{ 
					for(int z = 0; z < columnas_editables.length; z++){ 
						if(iCols==columnas_editables[z]){ 
							return true; 
						}	
					}	
				}catch(java.lang.NullPointerException e){ 
					return false; 
				} 
				return false;
			}
		});

		this.setDefaultRenderer(JButton.class, new TableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(JTable jtable, Object objeto, boolean estaSeleccionado, boolean tieneElFoco, int fila, int columna) {
				return (Component) objeto;
			}
		});
		
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				fila = rowAtPoint(e.getPoint()); 
				columna = columnAtPoint(e.getPoint());
					if (getModel().getColumnClass(columna).equals(JButton.class)) {
						String beneficiario = getValueAt(fila, 0).toString();
						if (columna == 8){
							try {
								AdministrarBeneficiarios ventana = new AdministrarBeneficiarios(conn);
								RegistroBeneficiarios modal = new RegistroBeneficiarios(beneficiario,conn);
								modal.setLocationRelativeTo(ventana);
								Beneficiario Obeneficiario = new Beneficiario();
								modal.setVisible(true);
								setClearGrid();
								RefreshTableButtomDouble(Obeneficiario.ListarBeneficiarios());
								
							} catch (SQLException e1) {
								e1.printStackTrace();
							} catch (PropertyVetoException e1) {
								e1.printStackTrace();
							}
						}else if (columna == 9){
							try {
								CambioEstadoBeneficiario(beneficiario, fila, 7);
							} catch (SQLException e1) {
								e1.printStackTrace();
							}
						}
					}
				}
			});
		
		setResizeColumn();
	}
	
	@SuppressWarnings({ "unchecked" })
	public void TableButtomDoubleT(ResultSet rs) throws SQLException{
		String[] botones = new String[] {
				"Editar",
				"Inhabilitar",
		};
			
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		@SuppressWarnings("rawtypes")
		ArrayList datosTemp = new ArrayList<>();

		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;
		int numeroBotones = botones.length;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[numeroColumnas + numeroBotones];
			tiposColumnas = new Class[numeroColumnas + numeroBotones];
			
			for (int i = 0; i < numeroColumnas; i++){
				columnas[i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

			for (int i = 0; i < botones.length; i++){
				columnas[numeroColumnas + i] = botones[i];
				tiposColumnas[numeroColumnas + i] = JButton.class;
			}

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for (int i = 0; i < numeroColumnas; i++) 
					fila[i] = rsUsuarios.getObject(i + 1);
				for (int j = 0; j < botones.length; j++){
					JButton boton = new JButton("");
					Estilo tema = new Estilo();
					boton.setBorderPainted(false);
					boton.setBackground(tema.getTerciario());
					if (botones[j].equals("Editar")){
						boton.setIcon(new ImageIcon(Tables.class.getResource("/img/ico_editar.png")));
						boton.setToolTipText("Editar");
					}else{
						boton.setIcon(new ImageIcon(Tables.class.getResource("/img/ico_etiqueta_azul.png")));
						boton.setToolTipText("Cambiar Estado");
					}
					fila[numeroColumnas + j] = boton;	
				}
				datosTemp.add(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
		@SuppressWarnings("rawtypes")
		final Class[] tiposColumnasFinal = tiposColumnas;
		Object[][] datos = new Object[datosTemp.size()][columnas.length];
		datosTemp.toArray(datos);

		this.setModel(new DefaultTableModel(datos,nombre_columnas) {
			@SuppressWarnings("rawtypes")
			Class[] tipos = tiposColumnasFinal;

			@SuppressWarnings("rawtypes")
			@Override
			public Class getColumnClass(int columnIndex) {
				return tipos[columnIndex];
			}

			@Override
			public boolean isCellEditable (int fila, int iCols) {
				try{ 
					for(int z = 0; z < columnas_editables.length; z++){ 
						if(iCols==columnas_editables[z]){ 
							return true; 
						}	
					}	
				}catch(java.lang.NullPointerException e){ 
					return false; 
				} 
				return false;
			}
		});

		this.setDefaultRenderer(JButton.class, new TableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(JTable jtable, Object objeto, boolean estaSeleccionado, boolean tieneElFoco, int fila, int columna) {
				return (Component) objeto;
			}
		});
		
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				fila = rowAtPoint(e.getPoint()); 
				columna = columnAtPoint(e.getPoint());
					if (getModel().getColumnClass(columna).equals(JButton.class)) {
						String cliente = getValueAt(fila, 0).toString();
						if (columna == 8){
							try {
								AdministrarClientes ventana = new AdministrarClientes(conn);
								RegistroClientes modal = new RegistroClientes(cliente,conn);
								modal.setLocationRelativeTo(ventana);
								Cliente Ocliente = new Cliente();
								modal.setVisible(true);
								setClearGrid();
								RefreshTableButtomDouble(Ocliente.ListarClientes());
								
							} catch (SQLException e1) {
								e1.printStackTrace();
							} catch (PropertyVetoException e1) {
								e1.printStackTrace();
							}
						}else if (columna == 9){
							try {
								CambioEstadoCliente(cliente, fila, 7);
							} catch (SQLException e1) {
								e1.printStackTrace();
							}
						}
					}
				}
			});
		setResizeColumn();
	}



}
