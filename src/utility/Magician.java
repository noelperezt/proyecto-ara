package utility;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JTextField;

/**
 * Mago realiza todas la validaciones de elementos usado para la entrada de datos
 * @author Noel P�rez
 *
 */

public class Magician {
	
	private boolean answer;
	
	public Magician(){}
	
	public boolean getAnswer(){
		return this.answer;
	}
	
	public void setAnswer(boolean answer){
		this.answer = answer;
	}
	
	public boolean isRif(JTextField element){
		Pattern pat = Pattern.compile("[VEGJ]{1}[0-9]{7,9}"); 
	    Matcher mat = pat.matcher(element.getText()); 
	    if(mat.find()){
	    	answer = true;
	    }else{
	    	answer = false;
	    }
	    return answer;
	}
	
	public boolean isEmail(JTextField element){
		Pattern pat = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"); 
		Matcher mat = pat.matcher(element.getText());
		if(!element.getText().equals("")){
			if(mat.find()){
		    	answer = true;
		    }else{
		    	answer = false;
		    }
		}else{
			answer = true;
		}	
	    return answer;
	}
	
	public boolean isNumeric(String element){
		try{
			Integer.parseInt(element);
			answer = true;
		}catch (NumberFormatException nfe){
			answer = false;
		}
		return answer;
	}
	
	public String OnlyNumbers(String cadena){
		String caracteres = "";
		Pattern pat = Pattern.compile("[0-9]");
		for(int i = 0; i< cadena.length(); i++){
			Matcher mat = pat.matcher(cadena.charAt(i)+"");
			if(mat.find()){
				caracteres = caracteres + cadena.charAt(i);
			}
		}
		return caracteres;
	}
	
	public String RetornaCodigo(int numero) throws SQLException{ 
		String n_pre = "";
		String nro = "";
		String cod ="";
		nro = ""+(numero+1);
		
		for(int i = 0; i< 6-nro.length(); i++){
			n_pre = n_pre + "0";
		}
		cod = "ARA."+n_pre + nro;
		return cod;
	}
	
	 public String conversion(double valor){
	    Locale.setDefault(Locale.US);
	    DecimalFormat num = new DecimalFormat("###.00");
	    return num.format(valor);
	 }
}
