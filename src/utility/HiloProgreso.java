package utility;

import javax.swing.JProgressBar;

public class HiloProgreso extends Thread{
    JProgressBar progreso;

    public HiloProgreso(JProgressBar progreso1){        
        super();
        this.progreso=progreso1;
    }
    public void run(){
        int l=0;
        for(l=1;l<=100;l++) {
            progreso.setValue(l);
            pausa(45); //100 por el tiempo de la musica
            progreso.setString(l+"%");
        }
    }
    public void pausa(int mlSeg){
        try{
            Thread.sleep(mlSeg);
        }catch(Exception e){}
    }
}
