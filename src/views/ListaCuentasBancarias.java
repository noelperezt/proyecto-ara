package views;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import models.Beneficiario;
import utility.Conexion;
import utility.Estilo;
import utility.Tables;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyVetoException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ListaCuentasBancarias extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private Tables listacuentas;
	private String codigo;
	
	/**
	 * Create the dialog.
	 * @throws SQLException 
	 */
	
	public String getCodigo(){
		return codigo;
	}
	
	public void setCodigo(String codigo){
		this.codigo = codigo;
	}
	
	public ListaCuentasBancarias(String id_beneficiario, Conexion conn) throws SQLException {
		setModal(true);
		setTitle("Constas Bancarias Cadastradas");
		setResizable(false);
		setBounds(100, 100, 572, 408);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				setCodigo("");
				setVisible(false);
			}
		});
		
		JLabel label = new JLabel("BUSCAR:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(24, 25, 56, 14);
		contentPanel.add(label);
		
		Beneficiario Obeneficiario = new Beneficiario();
		Obeneficiario.setConn(conn);
		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listacuentas.requestFocus();
				listacuentas.setRowSelectionInterval(0, 0);
			}
		});
		textField.setBounds(77, 22, 299, 20);
		textField.setToolTipText("");
		textField.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
				if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE ) {
			         evt.consume();
			    }
			}
			@Override
			public void keyReleased(KeyEvent e) {
				listacuentas.setClearGrid();
				try {
					listacuentas.Refresh(Obeneficiario.FiltraCuentas(textField.getText(), id_beneficiario));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		contentPanel.add(textField);
		textField.setColumns(10);
		
		String campos[] = {"ID","PAIS","BANCO","N� DE CUENTA","TIPO"};
		int ancho[] = {80,120,120,130,120};
		int editable[] = null;
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(24, 50, 514, 260);
		contentPanel.add(scrollPane);
		listacuentas = new Tables(campos, editable, ancho, null);
		listacuentas.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyPressed(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		        	if(listacuentas.getSelectedRow() != -1){
						setCodigo(listacuentas.getValueAt(listacuentas.getSelectedRow(), 0).toString());
					}else{
						setCodigo("");
					}
					setVisible(false);
		        }	
			}
		});
		listacuentas.Listar(Obeneficiario.ListarCuentas(id_beneficiario));
		scrollPane.setViewportView(listacuentas);
		
		Estilo tema = new Estilo();
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listacuentas.getSelectedRow() != -1){
					setCodigo(listacuentas.getValueAt(listacuentas.getSelectedRow(), 0).toString());
				}else{
					setCodigo("");
				}
				
				setVisible(false);
			}
		});
		btnAgregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnAgregar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnAgregar.setBackground(tema.getPrincipal());
			}
		});
		btnAgregar.setIcon(new ImageIcon(ListaCuentasBancarias.class.getResource("/img/add.png")));
		btnAgregar.setForeground(Color.WHITE);
		btnAgregar.setBorderPainted(false);
		btnAgregar.setBackground(tema.getPrincipal());
		btnAgregar.setBounds(123, 324, 120, 36);
		contentPanel.add(btnAgregar);
		
		JButton button_1 = new JButton("Volver");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCodigo("");
				setVisible(false);
			}
		});
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getPrincipal());
			}
		});
		button_1.setIcon(new ImageIcon(ListaCuentasBancarias.class.getResource("/img/back.png")));
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(tema.getPrincipal());
		button_1.setBounds(330, 324, 120, 36);
		contentPanel.add(button_1);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AgregarCuenta modal;
				try {
					boolean accion;
					if(Obeneficiario.getConn().getRowsCount(Obeneficiario.ListarCuentas(id_beneficiario)) > 0){
						accion = true;
					}else{
						accion = false;
					}
					modal = new AgregarCuenta(id_beneficiario,"0",conn);
					AdministrarBeneficiarios ventana = new AdministrarBeneficiarios(conn);
					modal.setLocationRelativeTo(ventana);
					modal.setVisible(true);
					listacuentas.setClearGrid();
					if(accion){
						listacuentas.Refresh(Obeneficiario.ListarCuentas(id_beneficiario));
					}else{
						listacuentas.Listar(Obeneficiario.ListarCuentas(id_beneficiario));
					}
				} catch (SQLException | PropertyVetoException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnCadastrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnCadastrar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnCadastrar.setBackground(tema.getPrincipal());
			}
		});
		btnCadastrar.setIcon(new ImageIcon(ListaCuentasBancarias.class.getResource("/img/ico_agregar.png")));
		btnCadastrar.setForeground(Color.WHITE);
		btnCadastrar.setBorderPainted(false);
		btnCadastrar.setBackground(tema.getPrincipal());
		btnCadastrar.setBounds(405, 21, 133, 21);
		contentPanel.add(btnCadastrar);
		
	}
}
