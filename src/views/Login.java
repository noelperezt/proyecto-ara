package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextField;
import models.Usuario;
import utility.Conexion;
import utility.Estilo;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.awt.Toolkit;
import java.sql.SQLException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Component;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Login {

	public JFrame frmIniciarSesin;
	private JTextField usuario;
	private JPasswordField clave;
	
	public Login() {
		initialize();
	}
	
	@SuppressWarnings({ "deprecation" })
	public void Ejecutar() throws SQLException{
		Icon icono = new ImageIcon(Login.class.getResource("/img/precaucion.png"));
		if(usuario.getText().equals("")){
			JOptionPane.showMessageDialog(null, "Deve informar o nome de usuario","Advertencia" , JOptionPane.WARNING_MESSAGE, icono);
		}else if(clave.getText().equals("")){
			JOptionPane.showMessageDialog(null, "Deve infromar a senha de acesso","Advertencia" , JOptionPane.WARNING_MESSAGE, icono);
		}else{
			Conexion conn = new Conexion();
			Usuario Ouser = new Usuario(usuario.getText());
			Ouser.setConn(conn);
			if(Ouser.IngresoSistema(clave.getText())){
				Ouser.TraerRegistro(Ouser.RetornaIdUsuario(usuario.getText()));
				Escritorio ventana = new Escritorio(Ouser, conn);
				ventana.escritorio.setVisible(true);
				frmIniciarSesin.dispose();
			}		
		}
	}
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmIniciarSesin = new JFrame();
		frmIniciarSesin.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Icon icono = new ImageIcon(Login.class.getResource("/img/interrogacion.png"));
				if(JOptionPane.showConfirmDialog(null, "Tem certeza que deseja sair?","Cerrar Sistema",1,0,icono)==0){
		            System.exit(0);
		        }
			}
		});
		frmIniciarSesin.setTitle("ARA CAMBIO & TURISMO");
		frmIniciarSesin.setIconImage(Toolkit.getDefaultToolkit().getImage(Login.class.getResource("/img/favicon.png")));
		frmIniciarSesin.setBounds(100, 100, 605, 344);
		frmIniciarSesin.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frmIniciarSesin.getContentPane().setLayout(null);
		frmIniciarSesin.setResizable(false);
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmIniciarSesin.getSize();  
        frmIniciarSesin.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 

		Estilo tema = new Estilo();
        
		clave = new JPasswordField();
		clave.setBorder(null);
		clave.setBounds(29, 170, 243, 21);
		clave.addActionListener(new ActionListener() {
		     @SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
		          clave.nextFocus();
		     }
		});
		frmIniciarSesin.getContentPane().add(clave);
		
		usuario = new JTextField();
		usuario.setBorder(null);
		usuario.addActionListener(new ActionListener() {
		     @SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
		          usuario.nextFocus();
		     }
		});
		usuario.setBounds(29, 109, 243, 21);
		frmIniciarSesin.getContentPane().add(usuario);
		usuario.setColumns(10);
		
		JLabel nombre_usuario = new JLabel("USUARIO:");
		nombre_usuario.setForeground(Color.WHITE);
		nombre_usuario.setAlignmentX(Component.RIGHT_ALIGNMENT);
		nombre_usuario.setFont(new Font("Arial", Font.BOLD, 11));
		nombre_usuario.setBounds(29, 85, 56, 21);
		frmIniciarSesin.getContentPane().add(nombre_usuario);
		
		JLabel clave_usuario = new JLabel("SENHA:");
		clave_usuario.setForeground(Color.WHITE);
		clave_usuario.setFont(new Font("Arial", Font.BOLD, 11));
		clave_usuario.setBounds(29, 151, 78, 21);
		frmIniciarSesin.getContentPane().add(clave_usuario);
		
		JButton ingresar = new JButton("Entrar");
		ingresar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				ingresar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				ingresar.setBackground(tema.getPrincipal());
			}
		});
		ingresar.setForeground(Color.WHITE);

		ingresar.setBackground(tema.getPrincipal());
		ingresar.setBorderPainted(false);
		ingresar.addActionListener(new ActionListener() {
			@SuppressWarnings({ })
			public void actionPerformed(ActionEvent e) {
				try {
					Ejecutar();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		ingresar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		        	try {
						Ejecutar();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }	
			}
		});
		
		JLabel lblIniciarSesin = new JLabel("SEJA BEM-VINDO");
		lblIniciarSesin.setForeground(Color.WHITE);
		lblIniciarSesin.setFont(new Font("Arial", Font.BOLD, 20));
		lblIniciarSesin.setAlignmentX(1.0f);
		lblIniciarSesin.setBounds(191, 39, 200, 21);
		frmIniciarSesin.getContentPane().add(lblIniciarSesin);
		ingresar.setBounds(29, 219, 118, 36);
		ingresar.setIcon(new ImageIcon(Login.class.getResource("/img/key.png")));
		frmIniciarSesin.getContentPane().add(ingresar);
		
		JButton salir = new JButton("Sair");
		salir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				salir.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				salir.setBackground(tema.getPrincipal());
			}
		});
		salir.setForeground(Color.WHITE);
		salir.setBorderPainted(false);
		salir.setBackground(tema.getPrincipal());
		salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Icon icono = new ImageIcon((Login.class.getResource("/img/interrogacion.png")));
				if(JOptionPane.showConfirmDialog(null, "Deseja Realmente Sair?","Aviso",1,0,icono)==0){
		            System.exit(0);
		        }
			}
		});
		salir.setBounds(157, 219, 118, 36);
		salir.setIcon(new ImageIcon(Login.class.getResource("/img/close.png")));
		frmIniciarSesin.getContentPane().add(salir);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Login.class.getResource("/img/Login_ara.png")));
		lblNewLabel.setBounds(0, 0, 600, 318);
		frmIniciarSesin.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Login.class.getResource("/img/login.png")));
		lblNewLabel_1.setBounds(0, 0, 600, 318);
		frmIniciarSesin.getContentPane().add(lblNewLabel_1);
		
	}
}
