package views;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import models.Banco;
import models.Beneficiario;
import models.Cliente;
import models.Moneda;
import models.Solicitud;
import utility.Conexion;
import utility.Estilo;
import utility.Magician;
import utility.Objeto;
import javax.swing.JLabel;
import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;

public class DetalleSolicitud extends JDialog {


	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField doc;
	private JTextField cpf;
	private JTextField nombre;
	private JTextField apellido;
	private JTextField celular;
	private JTextField email;
	private JTextField cuenta;
	@SuppressWarnings("rawtypes")
	private JComboBox pais_banco;
	@SuppressWarnings("rawtypes")
	private JComboBox tipo_cuenta;
	@SuppressWarnings("rawtypes")
	private JComboBox moneda;
	private JTextField nombre1;
	private JTextField apellido1;
	private JTextField doc1;
	private JTextField celular1;
	private JTextField cpf1;
	private JTextField email1;
	private JTextField envio;
	private JTextField taza;
	private JTextField total;
	private JTextField nombre_banco;
	public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
		String cadena= (jTextfieldS.getText()).toUpperCase();
		jTextfieldS.setText(cadena);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DetalleSolicitud(int solicitud, Conexion conn) throws SQLException {
		setModal(true);
		
		Solicitud Osolicitud = new Solicitud();
		Osolicitud.setConn(conn);
		Osolicitud.TraerRegistro(solicitud);
		Estilo tema = new Estilo();
		
		setTitle("Detalhes da Solitica\u00E7\u00E3o");
		setResizable(false);
		setBounds(100, 100, 956, 605);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		Magician mago = new Magician();
		
		Cliente Ocliente = new Cliente();
		Ocliente.setConn(conn);
		Ocliente.TraerRegistro(Osolicitud.getId_cliente()+"");
		
		JLabel lblDocumentoDeIdentidad = new JLabel("DOC.DE IDENTIDAD:");
		lblDocumentoDeIdentidad.setFont(new Font("Arial", Font.BOLD, 11));
		lblDocumentoDeIdentidad.setBounds(570, 67, 154, 21);
		contentPanel.add(lblDocumentoDeIdentidad);
		
		doc = new JTextField();
		doc.setEditable(false);
		doc.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		doc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cpf.requestFocus();
			}
		});
		doc.setText((String) null);
		doc.setColumns(10);
		doc.setText(Ocliente.getDocumento());
		doc.setBounds(570, 91, 154, 21);
		contentPanel.add(doc);
		
		JLabel lblCpfcnpj = new JLabel("CPF/CNPJ (Passaporte):");
		lblCpfcnpj.setFont(new Font("Arial", Font.BOLD, 11));
		lblCpfcnpj.setBounds(734, 67, 154, 21);
		contentPanel.add(lblCpfcnpj);
		
		cpf = new JTextField();
		cpf.setEditable(false);
		cpf.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		cpf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nombre.requestFocus();
			}
		});
		cpf.setText((String) null);
		cpf.setColumns(10);
		cpf.setText(Ocliente.getCpf());
		cpf.setBounds(734, 91, 154, 21);
		contentPanel.add(cpf);
		
		JLabel lblNome = new JLabel("NOME:");
		lblNome.setFont(new Font("Arial", Font.BOLD, 11));
		lblNome.setBounds(45, 67, 52, 21);
		contentPanel.add(lblNome);
		
		nombre = new JTextField();
		nombre.setEditable(false);
		nombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				apellido.requestFocus();
			}
		});
		nombre.setText((String) null);
		nombre.setColumns(10);
		nombre.setText(Ocliente.getNombre());
		nombre.setBounds(45, 91, 263, 21);
		contentPanel.add(nombre);
		
		JLabel lblNomeDoMeio = new JLabel("SOBRENOME:");
		lblNomeDoMeio.setFont(new Font("Arial", Font.BOLD, 11));
		lblNomeDoMeio.setBounds(318, 67, 154, 21);
		contentPanel.add(lblNomeDoMeio);
		
		apellido = new JTextField();
		apellido.setEditable(false);
		apellido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
		
			}
		});
		apellido.setText((String) null);
		apellido.setColumns(10);
		apellido.setText(Ocliente.getApellido());
		apellido.setBounds(318, 91, 242, 21);
		contentPanel.add(apellido);
		
		JLabel lblNCelular = new JLabel("CELULAR:");
		lblNCelular.setFont(new Font("Arial", Font.BOLD, 11));
		lblNCelular.setBounds(45, 123, 154, 21);
		contentPanel.add(lblNCelular);
		
		celular = new JTextField();
		celular.setEditable(false);
		celular.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || celular.getText().length() == 20 ){
		            evt.consume();
		        }
			}
		});
		celular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		celular.setText((String) null);
		celular.setColumns(10);
		celular.setText(Ocliente.getCelular());
		celular.setBounds(45, 147, 154, 21);
		contentPanel.add(celular);
		
		JLabel lblCorreoElectronico = new JLabel("EMAIL:");
		lblCorreoElectronico.setFont(new Font("Arial", Font.BOLD, 11));
		lblCorreoElectronico.setBounds(211, 123, 284, 21);
		contentPanel.add(lblCorreoElectronico);
		
		email = new JTextField();
		email.setEditable(false);
		email.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pais_banco.requestFocus();
			}
		});
		email.setText((String) null);
		email.setColumns(10);
		email.setText(Ocliente.getCorreo());
		email.setBounds(209, 147, 679, 21);
		contentPanel.add(email);
		
		JLabel lblPais = new JLabel("PAIS:");
		lblPais.setFont(new Font("Arial", Font.BOLD, 11));
		lblPais.setBounds(47, 366, 114, 21);
		contentPanel.add(lblPais);
		
		Beneficiario Obeneficiario = new Beneficiario();
		String[] vector = Obeneficiario.TraerRegistroCuenta(Osolicitud.getId_cuenta()+"");
		Obeneficiario.TraerRegistro(Osolicitud.getId_beneficiario()+"");
		
		pais_banco = new JComboBox();
		pais_banco.setModel(new DefaultComboBoxModel(new String[] {"BOLIVIA", "BRASIL", "CHILE", "COLOMBIA", "VENEZUELA"}));
		pais_banco.setEnabled(false);
		pais_banco.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				Banco Obanco = new Banco();
				Obanco.setConn(conn);
				ResultSet buscar;
				try {
					buscar = Obanco.ListarBancosPais(pais_banco.getSelectedItem().toString());
					DefaultComboBoxModel value = new DefaultComboBoxModel();
					while (buscar.next()){
						value.addElement(new Objeto(buscar.getInt("id"), buscar.getString("nombre")));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}		
			}
		});
		pais_banco.setFont(new Font("Arial", Font.PLAIN, 11));
		pais_banco.setBounds(47, 390, 210, 20);
		pais_banco.setSelectedItem(vector[1]);
		contentPanel.add(pais_banco);
		DefaultComboBoxModel value = new DefaultComboBoxModel();
		
		JLabel lblNombreDelBanco = new JLabel("BANCO:");
		lblNombreDelBanco.setFont(new Font("Arial", Font.BOLD, 11));
		lblNombreDelBanco.setBounds(263, 366, 154, 21);
		contentPanel.add(lblNombreDelBanco);
		
		JLabel lblTipoDeCuenta = new JLabel("TIPO DE CONTA:");
		lblTipoDeCuenta.setFont(new Font("Arial", Font.BOLD, 11));
		lblTipoDeCuenta.setBounds(770, 366, 114, 21);
		contentPanel.add(lblTipoDeCuenta);
		
		tipo_cuenta = new JComboBox();
		tipo_cuenta.setEnabled(false);
		tipo_cuenta.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				tipo_cuenta.nextFocus();
			}
		});
		tipo_cuenta.setModel(new DefaultComboBoxModel(new String[] {"POUPANCA", "CORRENTE"}));
		tipo_cuenta.setFont(new Font("Arial", Font.PLAIN, 11));
		tipo_cuenta.setSelectedItem(vector[4]);
		tipo_cuenta.setBounds(770, 390, 120, 20);
		contentPanel.add(tipo_cuenta);
		
		JLabel lblCuenta = new JLabel("CONTA:");
		lblCuenta.setFont(new Font("Arial", Font.BOLD, 11));
		lblCuenta.setBounds(526, 366, 234, 21);
		contentPanel.add(lblCuenta);
		
		cuenta = new JTextField();
		cuenta.setEditable(false);
		cuenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tipo_cuenta.requestFocus();
			}
		});
		cuenta.setText(vector[3]);
		cuenta.setColumns(10);
		cuenta.setBounds(526, 390, 234, 21);
		contentPanel.add(cuenta);
		
		JButton volver = new JButton("SAIR");
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				volver.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				volver.setBackground(tema.getPrincipal());
			}
		});
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		volver.setIcon(new ImageIcon(DetalleSolicitud.class.getResource("/img/back.png")));
		volver.setForeground(Color.WHITE);
		volver.setBorderPainted(false);
		volver.setBackground(tema.getPrincipal());
		volver.setBounds(684, 512, 132, 36);
		contentPanel.add(volver);
		
		JLabel label_2 = new JLabel("NOME:");
		label_2.setFont(new Font("Arial", Font.BOLD, 11));
		label_2.setBounds(45, 218, 52, 21);
		contentPanel.add(label_2);
		
		nombre1 = new JTextField();
		nombre1.setEditable(false);
		nombre1.setText(Obeneficiario.getNombre());
		nombre1.setColumns(10);
		nombre1.setBounds(45, 242, 263, 21);
		contentPanel.add(nombre1);
		
		JLabel lblSobrenome = new JLabel("SOBRENOME:");
		lblSobrenome.setFont(new Font("Arial", Font.BOLD, 11));
		lblSobrenome.setBounds(318, 218, 154, 21);
		contentPanel.add(lblSobrenome);
		
		apellido1 = new JTextField();
		apellido1.setEditable(false);
		apellido1.setText(Obeneficiario.getApellido());
		apellido1.setColumns(10);
		apellido1.setBounds(318, 242, 242, 21);
		contentPanel.add(apellido1);
		
		JLabel lblIdentidade = new JLabel("IDENTIDADE:");
		lblIdentidade.setFont(new Font("Arial", Font.BOLD, 11));
		lblIdentidade.setBounds(570, 218, 154, 21);
		contentPanel.add(lblIdentidade);
		
		doc1 = new JTextField();
		doc1.setEditable(false);
		doc1.setText(Obeneficiario.getDocumento());
		doc1.setColumns(10);
		doc1.setBounds(570, 242, 154, 21);
		contentPanel.add(doc1);
		
		JLabel label_5 = new JLabel("N. CELULAR:");
		label_5.setFont(new Font("Arial", Font.BOLD, 11));
		label_5.setBounds(45, 274, 154, 21);
		contentPanel.add(label_5);
		
		celular1 = new JTextField();
		celular1.setEditable(false);
		celular1.setText(Obeneficiario.getCelular());
		celular1.setColumns(10);
		celular1.setBounds(45, 298, 154, 21);
		contentPanel.add(celular1);
		
		JLabel label_8 = new JLabel("CPF/CNPJ (Passaporte):");
		label_8.setFont(new Font("Arial", Font.BOLD, 11));
		label_8.setBounds(734, 218, 154, 21);
		contentPanel.add(label_8);
		
		cpf1 = new JTextField();
		cpf1.setEditable(false);
		cpf1.setText(Obeneficiario.getCpf());
		cpf1.setColumns(10);
		cpf1.setBounds(734, 242, 154, 21);
		contentPanel.add(cpf1);
		
		JLabel lblEmail = new JLabel("EMAIL:");
		lblEmail.setFont(new Font("Arial", Font.BOLD, 11));
		lblEmail.setBounds(213, 274, 284, 21);
		contentPanel.add(lblEmail);
		
		email1 = new JTextField();
		email1.setEditable(false);
		email1.setText(Obeneficiario.getCorreo());
		email1.setColumns(10);
		email1.setBounds(211, 298, 677, 21);
		contentPanel.add(email1);
		
		JLabel lblVlAEnviar = new JLabel("R$ ENVIAR:");
		lblVlAEnviar.setFont(new Font("Arial", Font.BOLD, 11));
		lblVlAEnviar.setBounds(263, 422, 132, 21);
		contentPanel.add(lblVlAEnviar);
		
		envio = new JTextField();
		envio.setHorizontalAlignment(SwingConstants.CENTER);
		envio.setEnabled(false);
		envio.setText(mago.conversion(Osolicitud.getMonto_envio()));
		envio.setColumns(10);
		envio.setBounds(263, 445, 253, 21);
		envio.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				double enviar = Double.parseDouble(envio.getText());
				double tz = Double.parseDouble(taza.getText());
				double neto = enviar * tz;
				
				total.setText(mago.conversion(neto));
			}
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter != '.')) ){
		            evt.consume();
		        }
			}
		});
		contentPanel.add(envio);
		
		JLabel lbltaza = new JLabel("VALOR DA TAXA:");
		lbltaza.setFont(new Font("Arial", Font.BOLD, 11));
		lbltaza.setBounds(526, 422, 154, 21);
		contentPanel.add(lbltaza);
		
		JLabel lblTotal = new JLabel("TOTAL:");
		lblTotal.setFont(new Font("Arial", Font.BOLD, 11));
		lblTotal.setBounds(736, 421, 154, 21);
		contentPanel.add(lblTotal);
		
		total = new JTextField();
		total.setHorizontalAlignment(SwingConstants.CENTER);
		total.setEditable(false);
		total.setText(mago.conversion(Osolicitud.getTotal()));
		total.setColumns(10);
		total.setBounds(736, 445, 154, 21);
		contentPanel.add(total);
		
		nombre_banco = new JTextField();
		nombre_banco.setEditable(false);
		nombre_banco.setText(vector[2]);
		nombre_banco.setColumns(10);
		nombre_banco.setBounds(267, 390, 249, 21);
		contentPanel.add(nombre_banco);
		
		JLabel lblMoneda = new JLabel("MOEDA:");
		lblMoneda.setFont(new Font("Arial", Font.BOLD, 11));
		lblMoneda.setBounds(47, 421, 114, 21);
		contentPanel.add(lblMoneda);
		
		Moneda Omoneda = new Moneda();
		Omoneda.setConn(conn);
		
		moneda = new JComboBox();
		moneda.setEnabled(false);
		moneda.setFont(new Font("Arial", Font.PLAIN, 11));
		moneda.setBounds(47, 445, 210, 20);
		ResultSet buscar;
		try {
			buscar = Omoneda.ListarMonedas();
			moneda.removeAllItems();
			moneda.setModel(value);
			while (buscar.next()){
				value.addElement(new Objeto(buscar.getInt("id"), buscar.getString("nombre")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		moneda.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				Moneda Omoneda = new Moneda();
				Omoneda.setConn(conn);
				Objeto dp = (Objeto) moneda.getSelectedItem();
				try {
					Omoneda.TraerRegistro(dp.getId());
					taza.setText(Omoneda.getTaza()+"");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		Omoneda.TraerRegistro(Osolicitud.getId_moneda());
		moneda.setSelectedItem(Omoneda.getNombre());
		contentPanel.add(moneda);
		
		Objeto dp = (Objeto) moneda.getSelectedItem();
		Omoneda.TraerRegistro(dp.getId());
		
		taza = new JTextField();
		taza.setHorizontalAlignment(SwingConstants.CENTER);
		taza.setEditable(false);
		taza.setText(Osolicitud.getTaza()+"");
		taza.setColumns(10);
		taza.setBounds(526, 445, 200, 21);
		taza.setText(Omoneda.getTaza()+"");
		contentPanel.add(taza);
		
		
		JButton accion = new JButton("STATUS");
		accion.setIcon(new ImageIcon(DetalleSolicitud.class.getResource("/img/bolt.png")));
		accion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!Osolicitud.getEstado().equals("REVISAO")){
					VerObeservacion modal;
					DetalleSolicitud ventana;
						try {
							modal = new VerObeservacion(solicitud,conn);
							ventana = new DetalleSolicitud(solicitud,conn);
							modal.setLocationRelativeTo(ventana);
							modal.setVisible(true);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}		
				}else{
					CambiarEstado modal;
					DetalleSolicitud ventana;
					try {
						modal = new CambiarEstado();
						ventana = new DetalleSolicitud(solicitud,conn);
						modal.setLocationRelativeTo(ventana);
						modal.setVisible(true);
						if(!modal.getEstado().equals("REVISION")){
							accion.setEnabled(false);
							Osolicitud.CambiarEstadoRevision(solicitud, modal.getEstado(), modal.getObservacion());
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}		
				}
					
			}
		});
		accion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				accion.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				accion.setBackground(tema.getPrincipal());
			}
		});
		
		if(!Osolicitud.getEstado().equals("REVISAO")){
			accion.setText("Observacion");
		}
		accion.setForeground(Color.WHITE);
		accion.setBorderPainted(false);
		accion.setBackground(tema.getPrincipal());
		accion.setBounds(140, 512, 132, 36);
		contentPanel.add(accion);
		
		JLabel lblSolicitaoNNull = new JLabel("Solicita\u00E7\u00E3o N\u00BA: "+Osolicitud.getCodigo());
		lblSolicitaoNNull.setHorizontalAlignment(SwingConstants.CENTER);
		lblSolicitaoNNull.setFont(new Font("Arial", Font.BOLD, 15));
		lblSolicitaoNNull.setBounds(385, 11, 194, 21);
		contentPanel.add(lblSolicitaoNNull);
		
		JButton btnImprimir = new JButton("Imprimir");
		btnImprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					VistaImpresion modal = new VistaImpresion();
					DetalleSolicitud ventana = new DetalleSolicitud(solicitud,conn);
					modal.setLocationRelativeTo(ventana);
					modal.setVisible(true);
					if(modal.getProgreso() == 100){
						Osolicitud.imprimirSolicitud(Osolicitud.RetornaID(Osolicitud.getCodigo()));
					}	
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnImprimir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnImprimir.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnImprimir.setBackground(tema.getPrincipal());
			}
		});
		btnImprimir.setIcon(new ImageIcon(DetalleSolicitud.class.getResource("/img/printer.png")));
		btnImprimir.setForeground(Color.WHITE);
		btnImprimir.setBorderPainted(false);
		btnImprimir.setBackground(new Color(52, 165, 197));
		btnImprimir.setBounds(412, 512, 132, 36);
		contentPanel.add(btnImprimir);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Dados do Remetente", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 47, 921, 136);
		contentPanel.add(panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Dados do Benefici\u00E1rio", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(10, 198, 921, 136);
		contentPanel.add(panel_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Dados da Solicita\u00E7\u00E3o", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBounds(11, 348, 921, 136);
		contentPanel.add(panel_2);
		
	}
}
