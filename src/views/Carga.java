package views;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.event.ChangeListener;
import com.sun.awt.AWTUtilities;
import utility.Estilo;
import utility.HiloProgreso;
import javax.swing.event.ChangeEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class Carga {
	public int i = 50;
	public float j = 1;
	public JFrame frame;

	public Carga() throws InterruptedException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws InterruptedException 
	 */
	private void initialize() throws InterruptedException {
		
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(Carga.class.getResource("/img/favicon.png")));
		frame.setBounds(100, 100, 591, 378);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setUndecorated(true);
        
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frame.getSize();  
        frame.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
        frame.getContentPane().setLayout(null);
        
        JProgressBar progressBar = new JProgressBar();
        progressBar.setStringPainted(true);
        Estilo tema = new Estilo();
        progressBar.setForeground(tema.getPrincipal());
        progressBar.setBorderPainted(false);
        HiloProgreso hilo = new HiloProgreso(progressBar);
        hilo.start();
        progressBar.addChangeListener(new ChangeListener() {
        	public void stateChanged(ChangeEvent arg0) {
        		if(progressBar.getValue()==i){
        	        if(j!=1.1){
        	            AWTUtilities.setWindowOpacity(frame, j);
        	            i++;
        	            j = (float) (j - 0.02);
        	        }
        	    }

                if(progressBar.getValue()==100){
                	Login abrir = new Login();
                	abrir.frmIniciarSesin.setVisible(true);
                	frame.dispose();
                }
        	}
        });
        progressBar.setBounds(236, 72, -5, 20);
        frame.getContentPane().add(progressBar);   
        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setBounds(18, 26, 555, 320); 
        lblNewLabel.setIcon(new ImageIcon(Carga.class.getResource("/img/inicio.png")));
        
        
        frame.getContentPane().add(lblNewLabel);
	}
}
