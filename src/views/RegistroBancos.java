package views;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import models.Banco;
import utility.Conexion;
import utility.Estilo;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.sql.SQLException;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class RegistroBancos extends JDialog {


	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField codigo;
	private JTextField nombre;
	@SuppressWarnings("rawtypes")
	private JComboBox pais;

	public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
		String cadena= (jTextfieldS.getText()).toUpperCase();
		jTextfieldS.setText(cadena);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public RegistroBancos(Integer banco, Conexion conn) throws SQLException {
		setModal(true);
		String titulo;
		if(banco == 0){
			titulo = "Registro de Bancos";
		}else{
			titulo = "Modificar Banco";
		}
		
		Banco Obanco = new Banco();
		Obanco.setConn(conn);
		Obanco.TraerRegistro(banco);
		Estilo tema = new Estilo();
		
		setTitle(titulo);
		setResizable(false);
		setBounds(100, 100, 726, 194);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblCodigo = new JLabel("CODIGO:");
		lblCodigo.setFont(new Font("Arial", Font.BOLD, 11));
		lblCodigo.setBounds(22, 24, 52, 21);
		contentPanel.add(lblCodigo);
		
		codigo = new JTextField();
		codigo.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		codigo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nombre.requestFocus();
			}
		});
		codigo.setText((String) null);
		codigo.setColumns(10);
		codigo.setBounds(22, 48, 154, 21);
		codigo.setText(Obanco.getCodigo());
		contentPanel.add(codigo);
		
		JLabel lblNombre = new JLabel("BANCO:");
		lblNombre.setFont(new Font("Arial", Font.BOLD, 11));
		lblNombre.setBounds(194, 24, 323, 21);
		contentPanel.add(lblNombre);
		
		nombre = new JTextField();
		nombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				convertiraMayusculasEnJtextfield(nombre);
			}
		});
		nombre.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				nombre.nextFocus();
			}
		});
		nombre.setText((String) null);
		nombre.setColumns(10);
		nombre.setText(Obanco.getNombre());
		nombre.setBounds(194, 48, 323, 21);
		contentPanel.add(nombre);
		
		JButton volver = new JButton("Volver");
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				volver.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				volver.setBackground(tema.getPrincipal());
			}
		});
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		
		JLabel lblPais = new JLabel("PAIS:");
		lblPais.setFont(new Font("Arial", Font.BOLD, 11));
		lblPais.setBounds(536, 24, 44, 21);
		contentPanel.add(lblPais);
		
		pais = new JComboBox();
		pais.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				pais.nextFocus();
			}
		});
		pais.setFont(new Font("Arial", Font.PLAIN, 11));
		pais.setModel(new DefaultComboBoxModel(new String[] {"BOLIVIA", "BRASIL", "COLOMBIA", "CHILE", "VENEZUELA"}));
		pais.setBounds(536, 48, 154, 20);
		pais.setSelectedItem(Obanco.getPais());
		contentPanel.add(pais);
		volver.setIcon(new ImageIcon(RegistroBancos.class.getResource("/img/back.png")));
		volver.setForeground(Color.WHITE);
		volver.setBorderPainted(false);
		volver.setBackground(tema.getPrincipal());
		volver.setBounds(436, 111, 120, 36);
		contentPanel.add(volver);
		
		
		JButton accion = new JButton("");
		accion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				accion.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				accion.setBackground(tema.getPrincipal());
			}
		});
		accion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(codigo.getText().equals("") || nombre.getText().equals("") || pais.getSelectedItem().toString().equals("")){
					Icon icono = new ImageIcon(RegistroBancos.class.getResource("/img/error.png"));
					JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
				}else{
					boolean status = true;
					Banco Obanco = new Banco(codigo.getText(), nombre.getText(), pais.getSelectedItem().toString());
			    	Obanco.setConn(conn);
					if (banco == 0){
			    		try {
			    			status = Obanco.Registrar();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			    	}else{
			    		try {
							Obanco.Editar(banco);
			    		} catch (SQLException e) {
							// TODO Auto-generated catch block
			    			e.printStackTrace();
			    		}
				   	}
					if (status == true){
						codigo.setText("");
	   					nombre.setText("");
	   					pais.setSelectedItem("");
						dispose();
					} 	   
				}
			}
		});
		accion.setForeground(Color.WHITE);
		accion.setBorderPainted(false);
		accion.setBackground(tema.getPrincipal());
		accion.setBounds(190, 111, 120, 36);
		contentPanel.add(accion);
		
		if (banco == 0){
			accion.setText("Registrar");
			accion.setIcon(new ImageIcon(RegistroBancos.class.getResource("/img/add.png")));
		}else{
			accion.setText("Guardar");
			accion.setIcon(new ImageIcon(RegistroBancos.class.getResource("/img/save.png")));
		}
		
		JLabel label = new JLabel("(*)");
		label.setForeground(Color.RED);
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(69, 24, 12, 21);
		contentPanel.add(label);
		
		JLabel label_7 = new JLabel("(*)");
		label_7.setForeground(Color.RED);
		label_7.setFont(new Font("Arial", Font.BOLD, 11));
		label_7.setBounds(568, 24, 12, 21);
		contentPanel.add(label_7);
		
		JLabel label_11 = new JLabel("(*)");
		label_11.setForeground(Color.RED);
		label_11.setFont(new Font("Arial", Font.BOLD, 11));
		label_11.setBounds(309, 24, 12, 21);
		contentPanel.add(label_11);
		
		JLabel lblLosCamposMarcados = new JLabel("LOS CAMPOS MARCADOS CON       SON OBLIGATORIOS");
		lblLosCamposMarcados.setForeground(Color.GRAY);
		lblLosCamposMarcados.setFont(new Font("Arial", Font.ITALIC, 11));
		lblLosCamposMarcados.setBounds(392, 79, 298, 21);
		contentPanel.add(lblLosCamposMarcados);
		
		JLabel label_18 = new JLabel("(*)");
		label_18.setForeground(Color.RED);
		label_18.setFont(new Font("Arial", Font.BOLD, 11));
		label_18.setBounds(563, 79, 12, 21);
		contentPanel.add(label_18);
		
		if (banco == 0){
			titulo = "REGISTRO DE BANCOS";
		}else{
			titulo = "MODIFICAR BANCO";
		}
	}
}
