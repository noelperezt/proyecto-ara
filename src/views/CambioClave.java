package views;


import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import javax.swing.JPasswordField;
import models.Usuario;
import utility.Conexion;
import utility.Estilo;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

@SuppressWarnings("serial")
public class CambioClave extends JInternalFrame {
	private JPasswordField actual;
	private JPasswordField nueva;
	private JPasswordField confirmacion;

	public CambioClave(Usuario logueado, Conexion conn) {
		setFrameIcon(new ImageIcon(CambioClave.class.getResource("/img/pass.png")));
		setTitle("Cambio de Contrase\u00F1a");
		setClosable(true);
		setIconifiable(true);
		setBounds(100, 100, 516, 202);
		getContentPane().setLayout(null);
		
		Estilo tema = new Estilo();
		JLabel label = new JLabel("CONTRASE\u00D1A  ACTUAL:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(125, 21, 127, 21);
		getContentPane().add(label);
		
		actual = new JPasswordField();
		actual.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				actual.nextFocus();
			}
		});
		actual.setFont(new Font("Tahoma", Font.PLAIN, 12));
		actual.setBounds(262, 21, 223, 21); 
		getContentPane().add(actual);
		
		JLabel label_1 = new JLabel("CONTRASE\u00D1A NUEVA:");
		label_1.setFont(new Font("Arial", Font.BOLD, 11));
		label_1.setBounds(135, 53, 115, 21);
		getContentPane().add(label_1);
		
		nueva = new JPasswordField();
		nueva.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				nueva.nextFocus();
			}
		});
		nueva.setFont(new Font("Tahoma", Font.PLAIN, 12));
		nueva.setBounds(262, 53, 223, 21);
		getContentPane().add(nueva);
		
		JLabel label_2 = new JLabel("CONFIRME CONTRASE\u00D1A:");
		label_2.setFont(new Font("Arial", Font.BOLD, 11));
		label_2.setBounds(118, 86, 134, 21);
		getContentPane().add(label_2);
		
		confirmacion = new JPasswordField();
		confirmacion.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				confirmacion.nextFocus();
			}
		});
		confirmacion.setFont(new Font("Tahoma", Font.PLAIN, 12));
		confirmacion.setBounds(262, 85, 223, 21);
		getContentPane().add(confirmacion);
		
		JButton button = new JButton("Volver");
		button.setIcon(new ImageIcon(CambioClave.class.getResource("/img/back.png")));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				button.setBackground(tema.getPrincipal());
			}
		});
		button.setForeground(Color.WHITE);
		button.setBorderPainted(false);
		button.setBackground(tema.getPrincipal());
		button.setBounds(313, 122, 127, 36);
		getContentPane().add(button);
		
		JButton button_1 = new JButton("Guardar");
		button_1.setIcon(new ImageIcon(CambioClave.class.getResource("/img/save.png")));
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button_1.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				button_1.setBackground(tema.getPrincipal());
			}
		});
		button_1.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				if(!(nueva.getText().equals("") || actual.getText().equals("") || confirmacion.getText().equals(""))){
					Usuario Ouser = new Usuario();
					Ouser.setConn(conn);
					try {
						if(Ouser.CambiarClave(logueado.getUsuario(), actual.getText(), nueva.getText(), confirmacion.getText())){
							nueva.setText("");
							actual.setText("");
							confirmacion.setText("");
							dispose();
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(CambioClave.class.getResource("/img/error.png"));
					JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
				}
			}
		});
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(tema.getPrincipal());
		button_1.setBounds(146, 122, 127, 36);
		getContentPane().add(button_1);
		
		JLabel label_3 = new JLabel("");
		label_3.setIcon(new ImageIcon(CambioClave.class.getResource("/img/key2.png")));
		label_3.setBounds(21, 21, 92, 134);
		getContentPane().add(label_3);

	}

}
