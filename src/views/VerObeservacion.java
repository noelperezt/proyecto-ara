package views;

import java.awt.BorderLayout;
import java.sql.SQLException;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import models.Solicitud;
import utility.Conexion;
import utility.Estilo;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

@SuppressWarnings("serial")
public class VerObeservacion extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextArea obs; 
	/**
	 * Create the dialog.
	 * @throws SQLException 
	 */

	@SuppressWarnings({ })
	public VerObeservacion(int id_solicitud, Conexion conn) throws SQLException {
		setModal(true);
		setTitle("Observacao");
		setResizable(false);
		setBounds(100, 100, 405, 221);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				setVisible(false);
			}
		});
		
		
		Estilo tema = new Estilo();
		
		JButton button_1 = new JButton("Volver");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getPrincipal());
			}
		});
		button_1.setIcon(new ImageIcon(VerObeservacion.class.getResource("/img/back.png")));
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(tema.getPrincipal());
		button_1.setBounds(139, 133, 120, 36);
		contentPanel.add(button_1);
		
		JLabel lblObservacion = new JLabel("OBSERVACAO");
		lblObservacion.setFont(new Font("Arial", Font.BOLD, 11));
		lblObservacion.setBounds(35, 21, 89, 21);
		contentPanel.add(lblObservacion);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(35, 44, 322, 65);
		contentPanel.add(scrollPane);
		
		Solicitud Osolicitud = new Solicitud();
		Osolicitud.setConn(conn);
		Osolicitud.TraerRegistro(id_solicitud);
		
		obs  = new JTextArea();
		obs.setEditable(false);
		obs.setRows(3);
		obs.setColumns(22);
		obs.setText(Osolicitud.getObservacion());
		scrollPane.setViewportView(obs);
		
	}
}
