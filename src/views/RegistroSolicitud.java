package views;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import models.Banco;
import models.Beneficiario;
import models.Cliente;
import models.Moneda;
import models.Solicitud;
import utility.Conexion;
import utility.Estilo;
import utility.Magician;
import utility.Objeto;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import javax.swing.SwingConstants;

public class RegistroSolicitud extends JDialog {


	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField codigo;
	private JTextField doc;
	private JTextField cpf;
	private JTextField nombre;
	private JTextField apellido;
	private JTextField celular;
	private JTextField email;
	private JTextField cuenta;
	@SuppressWarnings("rawtypes")
	private JComboBox pais_banco;
	@SuppressWarnings("rawtypes")
	private JComboBox tipo_cuenta;
	@SuppressWarnings("rawtypes")
	private JComboBox moneda;
	private JTextField codigo1;
	private JTextField nombre1;
	private JTextField apellido1;
	private JTextField doc1;
	private JTextField celular1;
	private JTextField cpf1;
	private JTextField email1;
	private JTextField envio;
	private JTextField taza;
	private JTextField total;
	private JTextField nombre_banco;
	private Integer id_cuenta;

	public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
		String cadena= (jTextfieldS.getText()).toUpperCase();
		jTextfieldS.setText(cadena);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public RegistroSolicitud(String solicitud, int id_user, Conexion conn) throws SQLException {
		
		setModal(true);
		String titulo;
		if(solicitud.equals("0")){
			titulo = "NOVO  |   Solicitação de Envio";
		}else{
			titulo = "ALTERAR  |   Solicitação de Envio";
		}
		
		Beneficiario Obeneficiario = new Beneficiario();
		Obeneficiario.setConn(conn);
		Obeneficiario.TraerRegistro(solicitud);
		Estilo tema = new Estilo();
		
		setTitle(titulo);
		setResizable(false);
		setBounds(100, 100, 940, 601);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		Magician mago = new Magician();
		
		codigo = new JTextField();
		codigo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Cliente Ocliente = new Cliente();
				Ocliente.setConn(conn);
				try {
					if(Ocliente.ValidarCliente(codigo.getText())){
						Ocliente.TraerRegistro(codigo.getText());
						nombre.setText(Ocliente.getNombre());
						apellido.setText(Ocliente.getApellido());
						doc.setText(Ocliente.getDocumento());
						cpf.setText(Ocliente.getCpf());
						celular.setText(Ocliente.getCelular());
						email.setText(Ocliente.getCorreo());
					}else{
						Icon icono = new ImageIcon(RegistroSolicitud.class.getResource("/img/error.png"));
				    	JOptionPane.showMessageDialog(null, "Codigo informado nao existe!","Error" , JOptionPane.ERROR_MESSAGE,icono);
				    	nombre.setText("");
						apellido.setText("");
						doc.setText("");
						cpf.setText("");
						celular.setText("");
						email.setText("");
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				nombre.requestFocus();
			}
		});
		codigo.setText((String) null);
		codigo.setColumns(10);
		codigo.setBounds(22, 85, 120, 21);
		contentPanel.add(codigo);
		
		JLabel lblDocumentoDeIdentidad = new JLabel("IDENTIDADE:");
		lblDocumentoDeIdentidad.setFont(new Font("Arial", Font.BOLD, 11));
		lblDocumentoDeIdentidad.setBounds(712, 49, 154, 21);
		contentPanel.add(lblDocumentoDeIdentidad);
		
		doc = new JTextField();
		doc.setEditable(false);
		doc.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		doc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cpf.requestFocus();
			}
		});
		doc.setText((String) null);
		doc.setColumns(10);
		doc.setText(Obeneficiario.getDocumento());
		doc.setBounds(712, 71, 154, 21);
		contentPanel.add(doc);
		
		JLabel lblCpfcnpj = new JLabel("CPF/CNPJ (Passaporte):");
		lblCpfcnpj.setFont(new Font("Arial", Font.BOLD, 11));
		lblCpfcnpj.setBounds(206, 103, 154, 21);
		contentPanel.add(lblCpfcnpj);
		
		cpf = new JTextField();
		cpf.setEditable(false);
		cpf.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		cpf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nombre.requestFocus();
			}
		});
		cpf.setText((String) null);
		cpf.setColumns(10);
		cpf.setText(Obeneficiario.getCpf());
		cpf.setBounds(206, 127, 154, 21);
		contentPanel.add(cpf);
		
		JLabel lblNome = new JLabel("NOME:");
		lblNome.setFont(new Font("Arial", Font.BOLD, 11));
		lblNome.setBounds(206, 47, 52, 21);
		contentPanel.add(lblNome);
		
		nombre = new JTextField();
		nombre.setEditable(false);
		nombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				apellido.requestFocus();
			}
		});
		nombre.setText((String) null);
		nombre.setColumns(10);
		nombre.setText(Obeneficiario.getNombre());
		nombre.setBounds(206, 71, 239, 21);
		contentPanel.add(nombre);
		
		JLabel lblNomeDoMeio = new JLabel("SOBRENOME:");
		lblNomeDoMeio.setFont(new Font("Arial", Font.BOLD, 11));
		lblNomeDoMeio.setBounds(463, 49, 154, 21);
		contentPanel.add(lblNomeDoMeio);
		
		apellido = new JTextField();
		apellido.setEditable(false);
		apellido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		apellido.setText((String) null);
		apellido.setColumns(10);
		apellido.setText(Obeneficiario.getApellido());
		apellido.setBounds(463, 71, 239, 21);
		contentPanel.add(apellido);
		
		JLabel lblNCelular = new JLabel("CELULAR:");
		lblNCelular.setFont(new Font("Arial", Font.BOLD, 11));
		lblNCelular.setBounds(370, 103, 154, 21);
		contentPanel.add(lblNCelular);
		
		celular = new JTextField();
		celular.setEditable(false);
		celular.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || celular.getText().length() == 20 ){
		            evt.consume();
		        }
			}
		});
		celular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		celular.setText((String) null);
		celular.setColumns(10);
		celular.setText(Obeneficiario.getCelular());
		celular.setBounds(370, 127, 154, 21);
		contentPanel.add(celular);
		
		JLabel lblCorreoElectronico = new JLabel("EMAIL:");
		lblCorreoElectronico.setFont(new Font("Arial", Font.BOLD, 11));
		lblCorreoElectronico.setBounds(534, 103, 284, 21);
		contentPanel.add(lblCorreoElectronico);
		
		email = new JTextField();
		email.setEditable(false);
		email.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pais_banco.requestFocus();
			}
		});
		email.setText((String) null);
		email.setColumns(10);
		email.setText(Obeneficiario.getCorreo());
		email.setBounds(534, 127, 332, 21);
		contentPanel.add(email);
		
		JLabel lblPais = new JLabel("PAIS:");
		lblPais.setFont(new Font("Arial", Font.BOLD, 11));
		lblPais.setBounds(206, 362, 114, 21);
		contentPanel.add(lblPais);
		
		pais_banco = new JComboBox();
		pais_banco.setModel(new DefaultComboBoxModel(new String[] {"BOLIVIA", "BRASIL", "CHILE", "COLOMBIA", "VENEZUELA"}));
		pais_banco.setEnabled(false);
		pais_banco.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				Banco Obanco = new Banco();
				Obanco.setConn(conn);
				ResultSet buscar;
				try {
					buscar = Obanco.ListarBancosPais(pais_banco.getSelectedItem().toString());
					DefaultComboBoxModel value = new DefaultComboBoxModel();
					while (buscar.next()){
						value.addElement(new Objeto(buscar.getInt("id"), buscar.getString("nombre")));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}		
			}
		});
		pais_banco.setFont(new Font("Arial", Font.PLAIN, 11));
		pais_banco.setBounds(206, 386, 154, 20);
		contentPanel.add(pais_banco);
		DefaultComboBoxModel value = new DefaultComboBoxModel();
		
		JLabel lblNombreDelBanco = new JLabel("BANCO:");
		lblNombreDelBanco.setFont(new Font("Arial", Font.BOLD, 11));
		lblNombreDelBanco.setBounds(370, 362, 154, 21);
		contentPanel.add(lblNombreDelBanco);
		
		JLabel lblTipoDeCuenta = new JLabel("TIPO DE CONTA:");
		lblTipoDeCuenta.setFont(new Font("Arial", Font.BOLD, 11));
		lblTipoDeCuenta.setBounds(746, 362, 114, 21);
		contentPanel.add(lblTipoDeCuenta);
		
		tipo_cuenta = new JComboBox();
		tipo_cuenta.setEnabled(false);
		tipo_cuenta.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				tipo_cuenta.nextFocus();
			}
		});
		tipo_cuenta.setModel(new DefaultComboBoxModel(new String[] {"POUPANCA", "CORRENTE"}));
		tipo_cuenta.setFont(new Font("Arial", Font.PLAIN, 11));
		tipo_cuenta.setBounds(746, 386, 120, 20);
		contentPanel.add(tipo_cuenta);
		
		JLabel lblCuenta = new JLabel("CONTA:");
		lblCuenta.setFont(new Font("Arial", Font.BOLD, 11));
		lblCuenta.setBounds(558, 362, 141, 21);
		contentPanel.add(lblCuenta);
		
		cuenta = new JTextField();
		cuenta.setEditable(false);
		cuenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tipo_cuenta.requestFocus();
			}
		});
		cuenta.setText((String) null);
		cuenta.setColumns(10);
		cuenta.setBounds(558, 386, 178, 21);
		contentPanel.add(cuenta);
		
		JButton volver = new JButton("Sair");
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				volver.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				volver.setBackground(tema.getPrincipal());
			}
		});
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		volver.setIcon(new ImageIcon(RegistroSolicitud.class.getResource("/img/back.png")));
		volver.setForeground(Color.WHITE);
		volver.setBorderPainted(false);
		volver.setBackground(tema.getPrincipal());
		volver.setBounds(599, 517, 120, 36);
		contentPanel.add(volver);
		
		codigo1 = new JTextField();
		codigo1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Beneficiario Obeneficiario = new Beneficiario();
				Obeneficiario.setConn(conn);
				try {
					if(Obeneficiario.ValidarBeneficiario(codigo1.getText())){
						Obeneficiario.TraerRegistro(codigo1.getText());
						nombre1.setText(Obeneficiario.getNombre());
						apellido1.setText(Obeneficiario.getApellido());
						doc1.setText(Obeneficiario.getDocumento());
						cpf1.setText(Obeneficiario.getCpf());
						celular1.setText(Obeneficiario.getCelular());
						email1.setText(Obeneficiario.getCorreo());
						
					}else{
						Icon icono = new ImageIcon(RegistroSolicitud.class.getResource("/img/error.png"));
				    	JOptionPane.showMessageDialog(null, "Codigo nao existe!","Error" , JOptionPane.ERROR_MESSAGE,icono);
				    	nombre1.setText("");
						apellido1.setText("");
						doc1.setText("");
						cpf1.setText("");
						celular1.setText("");
						email1.setText("");
						cuenta.setText("");
						tipo_cuenta.setSelectedItem("");
						pais_banco.setSelectedItem("");
						nombre_banco.setText("");
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				nombre.requestFocus();
			}
		});
		codigo1.setText((String) null);
		codigo1.setColumns(10);
		codigo1.setBounds(26, 247, 120, 21);
		contentPanel.add(codigo1);
		
		JLabel label_2 = new JLabel("NOME:");
		label_2.setFont(new Font("Arial", Font.BOLD, 11));
		label_2.setBounds(210, 193, 52, 21);
		contentPanel.add(label_2);
		
		nombre1 = new JTextField();
		nombre1.setEditable(false);
		nombre1.setText((String) null);
		nombre1.setColumns(10);
		nombre1.setBounds(210, 217, 239, 21);
		contentPanel.add(nombre1);
		
		JLabel lblSobrenome = new JLabel("SOBRENOME:");
		lblSobrenome.setFont(new Font("Arial", Font.BOLD, 11));
		lblSobrenome.setBounds(467, 193, 154, 21);
		contentPanel.add(lblSobrenome);
		
		apellido1 = new JTextField();
		apellido1.setEditable(false);
		apellido1.setText((String) null);
		apellido1.setColumns(10);
		apellido1.setBounds(467, 217, 239, 21);
		contentPanel.add(apellido1);
		
		JLabel lblIdentidade = new JLabel("IDENTIDADE:");
		lblIdentidade.setFont(new Font("Arial", Font.BOLD, 11));
		lblIdentidade.setBounds(716, 193, 154, 21);
		contentPanel.add(lblIdentidade);
		
		doc1 = new JTextField();
		doc1.setEditable(false);
		doc1.setText((String) null);
		doc1.setColumns(10);
		doc1.setBounds(716, 217, 154, 21);
		contentPanel.add(doc1);
		
		JLabel lblCelular = new JLabel("CELULAR:");
		lblCelular.setFont(new Font("Arial", Font.BOLD, 11));
		lblCelular.setBounds(374, 249, 154, 21);
		contentPanel.add(lblCelular);
		
		celular1 = new JTextField();
		celular1.setEditable(false);
		celular1.setText((String) null);
		celular1.setColumns(10);
		celular1.setBounds(374, 273, 154, 21);
		contentPanel.add(celular1);
		
		JLabel label_8 = new JLabel("CPF/CNPJ (Passaporte):");
		label_8.setFont(new Font("Arial", Font.BOLD, 11));
		label_8.setBounds(210, 249, 154, 21);
		contentPanel.add(label_8);
		
		cpf1 = new JTextField();
		cpf1.setEditable(false);
		cpf1.setText((String) null);
		cpf1.setColumns(10);
		cpf1.setBounds(210, 273, 154, 21);
		contentPanel.add(cpf1);
		
		JLabel lblEmail = new JLabel("EMAIL:");
		lblEmail.setFont(new Font("Arial", Font.BOLD, 11));
		lblEmail.setBounds(540, 249, 284, 21);
		contentPanel.add(lblEmail);
		
		email1 = new JTextField();
		email1.setEditable(false);
		email1.setText((String) null);
		email1.setColumns(10);
		email1.setBounds(538, 273, 332, 21);
		contentPanel.add(email1);
		
		JLabel lblVlAEnviar = new JLabel("R$ REAL");
		lblVlAEnviar.setFont(new Font("Arial", Font.BOLD, 11));
		lblVlAEnviar.setBounds(448, 418, 132, 21);
		contentPanel.add(lblVlAEnviar);
		
		envio = new JTextField();
		envio.setHorizontalAlignment(SwingConstants.CENTER);
		envio.setText("0");
		envio.setColumns(10);
		envio.setBounds(448, 442, 141, 21);
		envio.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				double enviar = Double.parseDouble(envio.getText());
				double tz = Double.parseDouble(taza.getText());
				double neto = enviar * tz;
				
				total.setText(mago.conversion(neto));
			}
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter != '.')) ){
		            evt.consume();
		        }
			}
		});
		contentPanel.add(envio);
		
		JLabel lbltaza = new JLabel("TAXA:");
		lbltaza.setFont(new Font("Arial", Font.BOLD, 11));
		lbltaza.setBounds(599, 418, 70, 21);
		contentPanel.add(lbltaza);
		
		JLabel lblTotal = new JLabel("T. CONVERTIDO:");
		lblTotal.setFont(new Font("Arial", Font.BOLD, 11));
		lblTotal.setBounds(720, 418, 146, 21);
		contentPanel.add(lblTotal);
		
		total = new JTextField();
		total.setHorizontalAlignment(SwingConstants.CENTER);
		total.setEditable(false);
		total.setText((String) null);
		total.setColumns(10);
		total.setBounds(720, 442, 146, 21);
		contentPanel.add(total);
		
		nombre_banco = new JTextField();
		nombre_banco.setEditable(false);
		nombre_banco.setText((String) null);
		nombre_banco.setColumns(10);
		nombre_banco.setBounds(370, 386, 178, 21);
		contentPanel.add(nombre_banco);
		
		JLabel lblMoneda = new JLabel("MOEDA:");
		lblMoneda.setFont(new Font("Arial", Font.BOLD, 11));
		lblMoneda.setBounds(206, 418, 114, 21);
		contentPanel.add(lblMoneda);
		
		Moneda Omoneda = new Moneda();
		Omoneda.setConn(conn);
		
		
		moneda = new JComboBox();
		moneda.setFont(new Font("Arial", Font.PLAIN, 11));
		moneda.setBounds(206, 442, 232, 20);
		ResultSet buscar;
		try {
			buscar = Omoneda.ListarMonedas();
			moneda.removeAllItems();
			moneda.setModel(value);
			while (buscar.next()){
				value.addElement(new Objeto(buscar.getInt("id"), buscar.getString("nombre")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		moneda.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				Moneda Omoneda = new Moneda();
				Omoneda.setConn(conn);
				Objeto dp = (Objeto) moneda.getSelectedItem();
				try {
					Omoneda.TraerRegistro(dp.getId());
					taza.setText(Omoneda.getTaza()+"");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		contentPanel.add(moneda);
		
		Objeto dp = (Objeto) moneda.getSelectedItem();
		Omoneda.TraerRegistro(dp.getId());
		
		taza = new JTextField();
		taza.setHorizontalAlignment(SwingConstants.CENTER);
		taza.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter != '.')) ){
		            evt.consume();
		        }
			}
			@Override
			public void keyReleased(KeyEvent e) {
				double enviar = Double.parseDouble(envio.getText());
				double tz = Double.parseDouble(taza.getText());
				double neto = enviar * tz;
				
				total.setText(mago.conversion(neto));
			}
		});
		if(id_user == 1){
			taza.setEditable(true);
		}else{
			taza.setEditable(false);
		}
		taza.setText((String) null);
		taza.setColumns(10);
		taza.setBounds(599, 442, 105, 21);
		taza.setText(Omoneda.getTaza()+"");
		contentPanel.add(taza);
		
		JButton button = new JButton("");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegistroSolicitud modal;
				ListaClientes lista = null;
				try {
					modal = new RegistroSolicitud(solicitud, id_user,conn);
					lista = new ListaClientes(codigo.getText(),conn);
					lista.setLocationRelativeTo(modal);
					lista.setVisible(true);
					Cliente Ocliente = new Cliente();
					Ocliente.setConn(conn);
					if(!(lista.getCodigo().equals(""))){
						codigo.setText(lista.getCodigo());
						Ocliente.TraerRegistro(codigo.getText());
						nombre.setText(Ocliente.getNombre());
						apellido.setText(Ocliente.getApellido());
						doc.setText(Ocliente.getDocumento());
						cpf.setText(Ocliente.getCpf());
						celular.setText(Ocliente.getCelular());
						email.setText(Ocliente.getCorreo());
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		button.setIcon(new ImageIcon(RegistroSolicitud.class.getResource("/img/search.png")));
		button.setForeground(Color.WHITE);
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				button.setBackground(tema.getPrincipal());
			}
		});
		button.setBorderPainted(false);
		button.setBackground(tema.getPrincipal());
		button.setBounds(147, 85, 29, 21);
		button.requestFocus();
		contentPanel.add(button);
		
		JButton button_1 = new JButton("");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegistroSolicitud modal;
				ListaBeneficiarios lista = null;
				try {
					modal = new RegistroSolicitud(solicitud,id_user,conn);
					lista = new ListaBeneficiarios(conn);
					lista.setLocationRelativeTo(modal);
					lista.setVisible(true);
					if(!(lista.getCodigo().equals(""))){
						codigo1.setText(lista.getCodigo());
						Obeneficiario.TraerRegistro(codigo1.getText());
						nombre1.setText(Obeneficiario.getNombre());
						apellido1.setText(Obeneficiario.getApellido());
						doc1.setText(Obeneficiario.getDocumento());
						cpf1.setText(Obeneficiario.getCpf());
						celular1.setText(Obeneficiario.getCelular());
						email1.setText(Obeneficiario.getCorreo());
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		button_1.setIcon(new ImageIcon(RegistroSolicitud.class.getResource("/img/search.png")));
		button_1.setForeground(Color.WHITE);
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button_1.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				button_1.setBackground(tema.getPrincipal());
			}
		});
		button_1.setBorderPainted(false);
		button_1.setBackground(tema.getPrincipal());
		button_1.setBounds(151, 246, 29, 21);
		contentPanel.add(button_1);
		
		
		JButton accion = new JButton("");
		accion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(codigo1.getText().equals("")|| codigo.getText().equals("") || taza.getText().equals("") || envio.getText().equals("") || moneda.getSelectedItem().toString().equals("")){
					Icon icono = new ImageIcon(RegistroSolicitud.class.getResource("/img/error.png"));
					JOptionPane.showMessageDialog(null, "Necessario preenchimento","Error" , JOptionPane.ERROR_MESSAGE, icono);
				}else{
					boolean status = true;
					Objeto dp = (Objeto) moneda.getSelectedItem();
					Cliente Ocliente = new Cliente();
					Ocliente.setConn(conn);
					Beneficiario Obeneficiario = new Beneficiario();
					double enviar = Double.parseDouble(envio.getText());
					double tz = Double.parseDouble(taza.getText());
					double neto = Double.parseDouble(total.getText());
					Solicitud Osolicitud = null;
					try {
						Solicitud obj = new Solicitud();
						obj.setConn(conn);
						String cod = mago.RetornaCodigo(obj.RetornaSerie());
						Osolicitud = new Solicitud(Ocliente.RetornaID(codigo.getText()), Obeneficiario.RetornaID(codigo1.getText()),id_cuenta, dp.getId(), enviar, tz, neto,cod, id_user);
						Osolicitud.setConn(conn);
						Osolicitud.Registrar();
						Icon icono = new ImageIcon(RegistroSolicitud.class.getResource("/img/interrogacion.png"));
						if(JOptionPane.showConfirmDialog(null, "Deseja Imprimir o Comprovante?","Imprimir",1,0,icono)==0){
							try {
								VistaImpresion modal = new VistaImpresion();
								DetalleSolicitud ventana = new DetalleSolicitud(Osolicitud.RetornaSerie(),conn);
								modal.setLocationRelativeTo(ventana);
								modal.setVisible(true);
								if(modal.getProgreso() == 100){
									Osolicitud.imprimirSolicitud(Osolicitud.RetornaID(Osolicitud.getCodigo()));
								}	
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							if(JOptionPane.showConfirmDialog(null, "Deseja imprimir a 2 via?","Imprimir",1,0,icono)==0){
								try {
									VistaImpresion modal = new VistaImpresion();
									DetalleSolicitud ventana = new DetalleSolicitud(Osolicitud.RetornaSerie(),conn);
									modal.setLocationRelativeTo(ventana);
									modal.setVisible(true);
									if(modal.getProgreso() == 100){
										Osolicitud.imprimirSolicitud(Osolicitud.RetornaID(Osolicitud.getCodigo()));
									}	
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
					        } 
				        }	
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
					if (status == true){
						pais_banco.setSelectedItem("");
						nombre_banco.setText("");
						cuenta.setText("");
						tipo_cuenta.setSelectedItem("");
						moneda.setSelectedItem("");
						envio.setText("0");
						total.setText("0");
						codigo.setText("");
						nombre.setText("");
						apellido.setText("");
						doc.setText("");
						cpf.setText("");
						celular.setText("");
						email.setText("");
						codigo1.setText("");
						nombre1.setText("");
						apellido1.setText("");
						doc1.setText("");
						cpf1.setText("");
						celular1.setText("");
						email1.setText("");
					} 	   
				}
			}
		});
		accion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				accion.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				accion.setBackground(tema.getPrincipal());
			}
		});
		
		accion.setForeground(Color.WHITE);
		accion.setBorderPainted(false);
		accion.setBackground(tema.getPrincipal());
		accion.setBounds(294, 517, 120, 36);
		contentPanel.add(accion);
		
		JButton btnCuentasBancarias = new JButton("SELECIONAR");
		btnCuentasBancarias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RegistroSolicitud modal;
				ListaCuentasBancarias lista = null;
				try {
					modal = new RegistroSolicitud(solicitud, id_user,conn);
					lista = new ListaCuentasBancarias(Obeneficiario.RetornaID(codigo1.getText())+"",conn);
					lista.setLocationRelativeTo(modal);
					lista.setVisible(true);
					if(!(lista.getCodigo().equals(""))){
						String[] vector = Obeneficiario.TraerRegistroCuenta(lista.getCodigo());
						id_cuenta = Integer.parseInt(vector[0]);
						pais_banco.setSelectedItem(vector[1]);
						nombre_banco.setText(vector[2]);
						cuenta.setText(vector[3]);
						tipo_cuenta.setSelectedItem(vector[4]);
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnCuentasBancarias.setIcon(new ImageIcon(RegistroSolicitud.class.getResource("/img/search.png")));
		btnCuentasBancarias.setForeground(Color.WHITE);
		btnCuentasBancarias.setBorderPainted(false);
		btnCuentasBancarias.setBackground(new Color(52, 165, 197));
		btnCuentasBancarias.setBounds(22, 408, 154, 25);
		contentPanel.add(btnCuentasBancarias);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Dados do Cliente", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(186, 21, 731, 149);
		contentPanel.add(panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Dados do Benefici\u00E1rio", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(190, 181, 727, 149);
		contentPanel.add(panel_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Dados da Transa\u00E7\u00E3o", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBounds(186, 341, 731, 149);
		contentPanel.add(panel_2);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "C\u00F3digo do Cliente", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(9, 66, 180, 58);
		contentPanel.add(panel_3);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "C\u00F3digo do Benefici\u00E1rio", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_4.setBounds(13, 226, 180, 58);
		contentPanel.add(panel_4);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Contas Banc\u00E1rias", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_5.setBounds(9, 386, 180, 58);
		contentPanel.add(panel_5);
		
		if (solicitud.equals("0")){
			accion.setText("Registrar");
			accion.setIcon(new ImageIcon(RegistroSolicitud.class.getResource("/img/add.png")));
		}else{
			accion.setText("Guardar");
			accion.setIcon(new ImageIcon(RegistroSolicitud.class.getResource("/img/save.png")));
		}
		
		if (solicitud.equals("0")){
			titulo = "NOVO  |   Solicitação de Envio";
		}else{
			titulo = "ALTERAR  |   Solicitação de Envio";
		}
	}
}
