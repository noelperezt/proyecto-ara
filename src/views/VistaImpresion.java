package views;

import java.awt.FlowLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import utility.Estilo;
import utility.HiloProgreso;

@SuppressWarnings("serial")
public class VistaImpresion extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private int progreso;
	
	public int getProgreso() {
		return progreso;
	}

	public void setProgreso(int progreso) {
		this.progreso = progreso;
	}

	public VistaImpresion() {
		setTitle("AGUARDE  -  IMPRIMINDO...");
		setResizable(false);
		setModal(true);
		contentPanel.setLayout(new FlowLayout());
		setBounds(100, 100, 675, 208);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		Estilo tema = new Estilo();
        progressBar.setForeground(tema.getPrincipal());
        progressBar.setBorderPainted(false);
        HiloProgreso hilo = new HiloProgreso(progressBar);
        hilo.start();
        progressBar.addChangeListener(new ChangeListener() {
        	public void stateChanged(ChangeEvent arg0) {
                if(progressBar.getValue()==100){
                	setProgreso(progressBar.getValue());
                	dispose();
                }
        	}
        });
        getContentPane().setLayout(null);
        getContentPane().setLayout(null);
        progressBar.setBounds(10, 144, 430, 24);
        getContentPane().add(progressBar);
        
        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon(VistaImpresion.class.getResource("/img/print.png")));
        lblNewLabel.setBounds(155, 10, 139, 123);
        getContentPane().add(lblNewLabel);
		
		setBounds(100, 100, 450, 208);
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
	}

}
