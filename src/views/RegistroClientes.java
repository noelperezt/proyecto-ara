package views;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import models.Cliente;
import utility.Conexion;
import utility.Estilo;
import utility.Magician;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.sql.SQLException;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import com.toedter.calendar.JDateChooser;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class RegistroClientes extends JDialog {


	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField codigo;
	private JTextField doc;
	private JTextField cpf;
	private JTextField nombre;
	private JTextField apellido;
	private JTextField profesion;
	private JTextField celular;
	private JTextField residencia;
	private JTextField trabajo;
	private JTextField email;
	private JTextField cep;
	private JTextField lugar;
	private JTextField num;
	private JTextField barrio;
	private JTextField ciudad;
	private JTextField estado;
	@SuppressWarnings("rawtypes")
	private JComboBox pais;
	@SuppressWarnings("rawtypes")
	private JComboBox tipo;
	private JTextArea obs;
	private JDateChooser nac;

	public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
		String cadena= (jTextfieldS.getText()).toUpperCase();
		jTextfieldS.setText(cadena);
	}
	
	public void convertiraMayusculasEnJtextarea(javax.swing.JTextArea jTextarea){
		String cadena= (jTextarea.getText()).toUpperCase();
		jTextarea.setText(cadena);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public RegistroClientes(String cliente, Conexion conn) throws SQLException {
		setModal(true);
		String titulo;
		if(cliente.equals("0")){
			titulo = "Registro de Clientes";
		}else{
			titulo = "Modificar Cliente";
		}
		
		Cliente Ocliente = new Cliente();
		Ocliente.setConn(conn);
		Ocliente.TraerRegistro(cliente);
		Estilo tema = new Estilo();
		
		setTitle(titulo);
		setResizable(false);
		setBounds(100, 100, 726, 503);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblRazobSocial = new JLabel("CODIGO:");
		lblRazobSocial.setFont(new Font("Arial", Font.BOLD, 11));
		lblRazobSocial.setBounds(22, 24, 52, 21);
		contentPanel.add(lblRazobSocial);
		
		Magician mago = new Magician();
		
		codigo = new JTextField();
		codigo.setEditable(false);
		codigo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nombre.requestFocus();
			}
		});
		codigo.setText((String) null);
		codigo.setColumns(10);
		codigo.setBounds(22, 48, 154, 21);
		if(cliente.equals("0")){
			//codigo.setText(mago.RetornaCodigo(Ocliente.RetornaSerie()));
		}else{
			codigo.setText(Ocliente.getCodigo());
		}	
		contentPanel.add(codigo);
	
		
		JLabel lblTipoDeCliente = new JLabel("TIPO DE CLIENTE:");
		lblTipoDeCliente.setFont(new Font("Arial", Font.BOLD, 11));
		lblTipoDeCliente.setBounds(194, 24, 154, 21);
		contentPanel.add(lblTipoDeCliente);
		
		tipo = new JComboBox();
		tipo.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				tipo.nextFocus();
			}
		});
		tipo.setFont(new Font("Arial", Font.PLAIN, 11));
		tipo.setModel(new DefaultComboBoxModel(new String[] {"BRASILEIRO", "ESTRANGEIRO"}));
		tipo.setBounds(196, 48, 154, 20);
		tipo.setSelectedItem(Ocliente.getTipo());
		contentPanel.add(tipo);
		
		JLabel lblDocumentoDeIdentidad = new JLabel("IDENTIDADE:");
		lblDocumentoDeIdentidad.setFont(new Font("Arial", Font.BOLD, 11));
		lblDocumentoDeIdentidad.setBounds(22, 80, 154, 21);
		contentPanel.add(lblDocumentoDeIdentidad);
		
		doc = new JTextField();
		doc.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		doc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cpf.requestFocus();
			}
		});
		doc.setText((String) null);
		doc.setColumns(10);
		doc.setText(Ocliente.getDocumento());
		doc.setBounds(22, 104, 154, 21);
		contentPanel.add(doc);
		
		JLabel lblCpfcnpj = new JLabel("CPF/CNPJ (Passaporte):");
		lblCpfcnpj.setFont(new Font("Arial", Font.BOLD, 11));
		lblCpfcnpj.setBounds(194, 80, 154, 21);
		contentPanel.add(lblCpfcnpj);
		
		cpf = new JTextField();
		cpf.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		cpf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nombre.requestFocus();
			}
		});
		cpf.setText((String) null);
		cpf.setColumns(10);
		cpf.setText(Ocliente.getCpf());
		cpf.setBounds(194, 104, 154, 21);
		contentPanel.add(cpf);
		
		JLabel lblNome = new JLabel("NOME:");
		lblNome.setFont(new Font("Arial", Font.BOLD, 11));
		lblNome.setBounds(367, 80, 52, 21);
		contentPanel.add(lblNome);
		
		nombre = new JTextField();
		nombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				convertiraMayusculasEnJtextfield(nombre);
			}
		});
		nombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				apellido.requestFocus();
			}
		});
		nombre.setText((String) null);
		nombre.setColumns(10);
		nombre.setText(Ocliente.getNombre());
		nombre.setBounds(367, 104, 154, 21);
		contentPanel.add(nombre);
		
		JLabel lblNomeDoMeio = new JLabel("SOBRENOME:");
		lblNomeDoMeio.setFont(new Font("Arial", Font.BOLD, 11));
		lblNomeDoMeio.setBounds(536, 80, 154, 21);
		contentPanel.add(lblNomeDoMeio);
		
		apellido = new JTextField();
		apellido.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				convertiraMayusculasEnJtextfield(apellido);
			}
		});
		apellido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nac.requestFocus();
			}
		});
		apellido.setText((String) null);
		apellido.setColumns(10);
		apellido.setText(Ocliente.getApellido());
		apellido.setBounds(536, 104, 154, 21);
		contentPanel.add(apellido);
		
		JLabel lblFechaNascimento = new JLabel("NASCIMENTO:");
		lblFechaNascimento.setFont(new Font("Arial", Font.BOLD, 11));
		lblFechaNascimento.setBounds(22, 136, 154, 21);
		contentPanel.add(lblFechaNascimento);
		
		nac = new JDateChooser();
		nac.setDate(Ocliente.getFecha_nac());
		nac.getCalendarButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				profesion.requestFocus();
			}
		});
		nac.setBounds(22, 160, 154, 20);
		contentPanel.add(nac);
		
		JLabel lblProff = new JLabel("PROFISSAO:");
		lblProff.setFont(new Font("Arial", Font.BOLD, 11));
		lblProff.setBounds(194, 136, 154, 21);
		contentPanel.add(lblProff);
		
		profesion = new JTextField();
		profesion.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				convertiraMayusculasEnJtextfield(profesion);
			}
		});
		profesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pais.requestFocus();
			}
		});
		profesion.setText((String) null);
		profesion.setColumns(10);
		profesion.setText(Ocliente.getProfesion());
		profesion.setBounds(194, 160, 154, 21);
		contentPanel.add(profesion);
		
		JLabel lblPaisDeNacimiento = new JLabel("PAIS DE NASCIMENTO:");
		lblPaisDeNacimiento.setFont(new Font("Arial", Font.BOLD, 11));
		lblPaisDeNacimiento.setBounds(367, 136, 154, 21);
		contentPanel.add(lblPaisDeNacimiento);
		
		pais = new JComboBox();
		pais.setEditable(true);
		pais.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				pais.nextFocus();
			}
		});
		pais.setFont(new Font("Arial", Font.PLAIN, 11));
		pais.setModel(new DefaultComboBoxModel(new String[] {"AFGANIST\u00C1N ", "AKROTIRI ", "ALBANIA ", "ALEMANIA ", "ANDORRA ", "ANGOLA ", "ANGUILA ", "ANT\u00C1RTIDA ", "ANTIGUA Y BARBUDA ", "ANTILLAS NEERLANDESAS ", "ARABIA SAUD\u00CD ", "ARCTIC OCEAN ", "ARGELIA ", "ARGENTINA ", "ARMENIA ", "ARUBA ", "ASHMORE ANDCARTIER ISLANDS ", "ATLANTIC OCEAN ", "AUSTRALIA ", "AUSTRIA ", "AZERBAIY\u00C1N ", "BAHAMAS ", "BAHR\u00C1IN ", "BANGLADESH ", "BARBADOS ", "B\u00C9LGICA ", "BELICE ", "BEN\u00CDN ", "BERMUDAS ", "BIELORRUSIA ", "BIRMANIA MYANMAR ", "BOLIVIA ", "BOSNIA Y HERCEGOVINA ", "BOTSUANA ", "BRASIL ", "BRUN\u00C9I ", "BULGARIA ", "BURKINA FASO ", "BURUNDI ", "BUT\u00C1N ", "CABO VERDE ", "CAMBOYA ", "CAMER\u00DAN ", "CANAD\u00C1 ", "CHAD ", "CHILE ", "CHINA ", "CHIPRE ", "CLIPPERTON ISLAND ", "COLOMBIA ", "COMORAS ", "CONGO ", "CORAL SEA ISLANDS ", "COREA DEL NORTE ", "COREA DEL SUR ", "COSTA DE MARFIL ", "COSTA RICA ", "CROACIA ", "CUBA ", "DHEKELIA ", "DINAMARCA ", "DOMINICA ", "ECUADOR ", "EGIPTO ", "EL SALVADOR ", "EL VATICANO ", "EMIRATOS \u00C1RABES UNIDOS ", "ERITREA ", "ESLOVAQUIA ", "ESLOVENIA ", "ESPA\u00D1A ", "ESTADOS UNIDOS ", "ESTONIA ", "ETIOP\u00CDA ", "FILIPINAS ", "FINLANDIA ", "FIYI ", "FRANCIA ", "GAB\u00D3N ", "GAMBIA ", "GAZA STRIP ", "GEORGIA ", "GHANA ", "GIBRALTAR ", "GRANADA ", "GRECIA ", "GROENLANDIA ", "GUAM ", "GUATEMALA ", "GUERNSEY ", "GUINEA ", "GUINEA ECUATORIAL ", "GUINEA-BISSAU ", "GUYANA ", "HAIT\u00CD ", "HONDURAS ", "HONG KONG ", "HUNGR\u00CDA ", "INDIA ", "INDIAN OCEAN ", "INDONESIA ", "IR\u00C1N ", "IRAQ ", "IRLANDA ", "ISLA BOUVET ", "ISLA CHRISTMAS ", "ISLA NORFOLK ", "ISLANDIA ", "ISLAS CAIM\u00C1N ", "ISLAS COCOS ", "ISLAS COOK ", "ISLAS FEROE ", "ISLAS GEORGIA DEL SUR Y SANDWICH DEL SUR ", "ISLAS HEARD Y MCDONALD ", "ISLAS MALVINAS ", "ISLAS MARIANAS DEL NORTE ", "ISLASMARSHALL ", "ISLAS PITCAIRN ", "ISLAS SALOM\u00D3N ", "ISLAS TURCAS Y CAICOS ", "ISLAS V\u00CDRGENES AMERICANAS ", "ISLAS V\u00CDRGENES BRIT\u00C1NICAS ", "ISRAEL ", "ITALIA ", "JAMAICA ", "JAN MAYEN ", "JAP\u00D3N ", "JERSEY ", "JORDANIA ", "KAZAJIST\u00C1N ", "KENIA ", "KIRGUIZIST\u00C1N ", "KIRIBATI ", "KUWAIT ", "LAOS ", "LESOTO ", "LETONIA ", "L\u00CDBANO ", "LIBERIA ", "LIBIA ", "LIECHTENSTEIN ", "LITUANIA ", "LUXEMBURGO ", "MACAO ", "MACEDONIA ", "MADAGASCAR ", "MALASIA ", "MALAUI ", "MALDIVAS ", "MAL\u00CD ", "MALTA ", "MAN, ISLE OF ", "MARRUECOS ", "MAURICIO ", "MAURITANIA ", "MAYOTTE ", "M\u00C9XICO ", "MICRONESIA ", "MOLDAVIA ", "M\u00D3NACO ", "MONGOLIA ", "MONTSERRAT ", "MOZAMBIQUE ", "NAMIBIA ", "NAURU ", "NAVASSA ISLAND ", "NEPAL ", "NICARAGUA ", "N\u00CDGER ", "NIGERIA ", "NIUE ", "NORUEGA ", "NUEVA CALEDONIA ", "NUEVA ZELANDA ", "OM\u00C1N ", "PACIFIC OCEAN ", "PA\u00CDSES BAJOS ", "PAKIST\u00C1N ", "PALAOS ", "PANAM\u00C1 ", "PAP\u00DAA-NUEVA GUINEA ", "PARACEL ISLANDS ", "PARAGUAY ", "PER\u00DA ", "POLINESIA FRANCESA ", "POLONIA ", "PORTUGAL ", "PUERTO RICO ", "QATAR ", "REINO UNIDO ", "REP\u00DABLICA CENTROAFRICANA ", "REP\u00DABLICA CHECA ", "REP\u00DABLICA DEMOCR\u00C1TICA DEL CONGO ", "REP\u00DABLICA DOMINICANA ", "RUANDA ", "RUMANIA ", "RUSIA ", "S\u00C1HARA OCCIDENTAL ", "SAMOA ", "SAMOA AMERICANA ", "SAN CRIST\u00D3BAL Y NIEVES ", "SAN MARINO ", "SAN PEDRO Y MIQUEL\u00D3N ", "SAN VICENTE Y LAS GRANADINAS ", "SANTA HELENA ", "SANTA LUC\u00CDA ", "SANTO TOM\u00C9 Y PR\u00CDNCIPE ", "SENEGAL ", "SEYCHELLES ", "SIERRA LEONA ", "SINGAPUR ", "SIRIA ", "SOMALIA ", "SOUTHERN OCEAN ", "SPRATLY ISLANDS ", "SRI LANKA ", "SUAZILANDIA ", "SUD\u00C1FRICA ", "SUD\u00C1N ", "SUECIA ", "SUIZA ", "SURINAM ", "SVALBARD Y JAN MAYEN ", "TAILANDIA ", "TAIW\u00C1N ", "TANZANIA ", "TAYIKIST\u00C1N ", "TERRITORIOBRIT\u00C1NICODEL OC\u00C9ANO INDICO ", "TERRITORIOS AUSTRALES FRANCESES ", "TIMOR ORIENTAL ", "TOGO ", "TOKELAU ", "TONGA ", "TRINIDAD Y TOBAGO ", "T\u00DANEZ ", "TURKMENIST\u00C1N ", "TURQU\u00CDA ", "TUVALU ", "UCRANIA ", "UGANDA ", "UNI\u00D3N EUROPEA ", "URUGUAY ", "UZBEKIST\u00C1N ", "VANUATU ", "VENEZUELA ", "VIETNAM ", "WAKE ISLAND ", "WALLIS Y FUTUNA ", "WEST BANK ", "WORLD ", "YEMEN ", "YIBUTI ", "ZAMBIA ", "ZIMBABUE"}));
		pais.setBounds(367, 160, 154, 20);
		pais.setSelectedItem(Ocliente.getPais());
		AutoCompleteDecorator.decorate(pais);
		contentPanel.add(pais);
		
		JLabel lblNCelular = new JLabel("N. CELULAR:");
		lblNCelular.setFont(new Font("Arial", Font.BOLD, 11));
		lblNCelular.setBounds(536, 136, 154, 21);
		contentPanel.add(lblNCelular);
		
		celular = new JTextField();
		celular.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || celular.getText().length() == 20 ){
		            evt.consume();
		        }
			}
		});
		celular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				residencia.requestFocus();
			}
		});
		celular.setText((String) null);
		celular.setColumns(10);
		celular.setText(Ocliente.getCelular());
		celular.setBounds(536, 160, 154, 21);
		contentPanel.add(celular);
		
		JLabel lblNTelefonoResidencial = new JLabel("TELEFONE:");
		lblNTelefonoResidencial.setFont(new Font("Arial", Font.BOLD, 11));
		lblNTelefonoResidencial.setBounds(22, 192, 99, 21);
		contentPanel.add(lblNTelefonoResidencial);
		
		residencia = new JTextField();
		residencia.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || residencia.getText().length() == 20 ){
		            evt.consume();
		        }
			}
		});
		residencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				trabajo.requestFocus();
			}
		});
		residencia.setText((String) null);
		residencia.setColumns(10);
		residencia.setText(Ocliente.getTelefono());
		residencia.setBounds(22, 216, 154, 21);
		contentPanel.add(residencia);
		
		JLabel lblNTrabajo = new JLabel("TELEFONE 2:");
		lblNTrabajo.setFont(new Font("Arial", Font.BOLD, 11));
		lblNTrabajo.setBounds(194, 192, 154, 21);
		contentPanel.add(lblNTrabajo);
		
		trabajo = new JTextField();
		trabajo.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || trabajo.getText().length() == 20 ){
		            evt.consume();
		        }
			}
		});
		trabajo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				email.requestFocus();
			}
		});
		trabajo.setText((String) null);
		trabajo.setColumns(10);
		trabajo.setText(Ocliente.getN_tabajo());
		trabajo.setBounds(194, 216, 154, 21);
		contentPanel.add(trabajo);
		
		JLabel lblCorreoElectronico = new JLabel("EMAIL:");
		lblCorreoElectronico.setFont(new Font("Arial", Font.BOLD, 11));
		lblCorreoElectronico.setBounds(367, 192, 323, 21);
		contentPanel.add(lblCorreoElectronico);
		
		email = new JTextField();
		email.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cep.requestFocus();
			}
		});
		email.setText((String) null);
		email.setColumns(10);
		email.setText(Ocliente.getCorreo());
		email.setBounds(367, 216, 323, 21);
		contentPanel.add(email);
		
		JLabel lblCep = new JLabel("CEP:");
		lblCep.setFont(new Font("Arial", Font.BOLD, 11));
		lblCep.setBounds(22, 264, 52, 21);
		contentPanel.add(lblCep);
		
		cep = new JTextField();
		cep.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lugar.requestFocus();
			}
		});
		cep.setText((String) null);
		cep.setColumns(10);
		cep.setText(Ocliente.getCep());
		cep.setBounds(22, 288, 125, 21);
		contentPanel.add(cep);
		
		JLabel lblLogradouro = new JLabel("LOGRADOURO:");
		lblLogradouro.setFont(new Font("Arial", Font.BOLD, 11));
		lblLogradouro.setBounds(162, 264, 227, 21);
		contentPanel.add(lblLogradouro);
		
		lugar = new JTextField();
		lugar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				convertiraMayusculasEnJtextfield(lugar);
			}
		});
		lugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				num.requestFocus();
			}
		});
		lugar.setText((String) null);
		lugar.setColumns(10);
		lugar.setText(Ocliente.getLugar());
		lugar.setBounds(162, 288, 227, 21);
		contentPanel.add(lugar);
		
		JLabel lblNumero = new JLabel("NUMERO:");
		lblNumero.setFont(new Font("Arial", Font.BOLD, 11));
		lblNumero.setBounds(406, 264, 52, 21);
		contentPanel.add(lblNumero);
		
		num = new JTextField();
		num.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		num.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				barrio.requestFocus();
			}
		});
		num.setText((String) null);
		num.setColumns(10);
		num.setText(Ocliente.getNumero());
		num.setBounds(406, 288, 99, 21);
		contentPanel.add(num);
		
		JLabel lblBairrio = new JLabel("BAIRRIO:");
		lblBairrio.setFont(new Font("Arial", Font.BOLD, 11));
		lblBairrio.setBounds(525, 264, 165, 21);
		contentPanel.add(lblBairrio);
		
		barrio = new JTextField();
		barrio.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				convertiraMayusculasEnJtextfield(barrio);
			}
		});
		barrio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ciudad.requestFocus();
			}
		});
		barrio.setText((String) null);
		barrio.setColumns(10);
		barrio.setText(Ocliente.getBarrio());
		barrio.setBounds(525, 288, 165, 21);
		contentPanel.add(barrio);
		
		JLabel lblCidade = new JLabel("CIDADE:");
		lblCidade.setFont(new Font("Arial", Font.BOLD, 11));
		lblCidade.setBounds(22, 320, 52, 21);
		contentPanel.add(lblCidade);
		
		ciudad = new JTextField();
		ciudad.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				convertiraMayusculasEnJtextfield(ciudad);
			}
		});
		ciudad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				estado.requestFocus();
			}
		});
		ciudad.setText((String) null);
		ciudad.setColumns(10);
		ciudad.setText(Ocliente.getCiudad());
		ciudad.setBounds(22, 344, 178, 21);
		contentPanel.add(ciudad);
		
		JLabel lblEstado = new JLabel("ESTADO:");
		lblEstado.setFont(new Font("Arial", Font.BOLD, 11));
		lblEstado.setBounds(218, 320, 52, 21);
		contentPanel.add(lblEstado);
		
		estado = new JTextField();
		estado.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				convertiraMayusculasEnJtextfield(estado);
			}
		});
		estado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				obs.requestFocus();
			}
		});
		estado.setText((String) null);
		estado.setColumns(10);
		estado.setText(Ocliente.getEstado());
		estado.setBounds(217, 344, 172, 21);
		contentPanel.add(estado);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(406, 344, 284, 55);
		contentPanel.add(scrollPane);
		
		obs = new JTextArea();
		obs.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				convertiraMayusculasEnJtextarea(obs);
			}
		});
		obs.setText(Ocliente.getObservacion());
		scrollPane.setViewportView(obs);
		
		JLabel lblObs = new JLabel("OBS:");
		lblObs.setFont(new Font("Arial", Font.BOLD, 11));
		lblObs.setBounds(406, 323, 52, 21);
		contentPanel.add(lblObs);
		
		JButton volver = new JButton("Volver");
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				volver.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				volver.setBackground(tema.getPrincipal());
			}
		});
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		volver.setIcon(new ImageIcon(RegistroClientes.class.getResource("/img/back.png")));
		volver.setForeground(Color.WHITE);
		volver.setBorderPainted(false);
		volver.setBackground(tema.getPrincipal());
		volver.setBounds(424, 420, 120, 36);
		contentPanel.add(volver);
		
		
		JButton accion = new JButton("");
		accion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				accion.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				accion.setBackground(tema.getPrincipal());
			}
		});
		accion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(tipo.getSelectedItem().toString().equals("") || nombre.getText().equals("") || apellido.getText().equals("") || doc.getText().equals("") || cpf.getText().equals("") || tipo.getSelectedItem().toString().equals("") || celular.getText().equals("") ){
					Icon icono = new ImageIcon(RegistroClientes.class.getResource("/img/error.png"));
					JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
				}else{
					if(mago.isEmail(email)){
						boolean status = true;
						String pais_nac = "";
						if(pais.getSelectedIndex() != -1){
							pais_nac = pais.getSelectedItem().toString();
						}
						String cod = null;
						try {
							cod = mago.RetornaCodigo(Ocliente.RetornaSerie());
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
				    	Cliente Ocliente = new Cliente(cod, tipo.getSelectedItem().toString(), doc.getText(), cpf.getText(), nombre.getText(), apellido.getText(), nac.getDate(), profesion.getText(), pais_nac, celular.getText(), trabajo.getText(), residencia.getText(), email.getText(), cep.getText(), lugar.getText(), num.getText(), barrio.getText(), ciudad.getText(), estado.getText(), obs.getText());
				    	Ocliente.setConn(conn);
				    	if (cliente.equals("0")){
				    		try {
				    			status = Ocliente.Registrar();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				    	}else{
				    		try {
								Ocliente.Editar(cliente);
				    		} catch (SQLException e) {
								// TODO Auto-generated catch block
				    			e.printStackTrace();
				    		}
					   	}
						if (status == true){
							codigo.setText("");
							tipo.setSelectedItem("");
		   					nombre.setText("");
		   					apellido.setText("");
		   					doc.setText("");
		   					cpf.setText("");
		   					tipo.setSelectedItem("");
		   					nac.setDate(null);
		   					profesion.setText("");
		   					pais.setSelectedItem("");
		   					celular.setText("");
		   					trabajo.setText("");
		   					residencia.setText("");
		   					email.setText("");
		   					cep.setText("");
		   					lugar.setText("");
		   					num.setText("");
		   					barrio.setText("");
		   					ciudad.setText("");
		   					estado.setText("");
		   					obs.setText("");
							dispose();
						} 	   
					}else{
						Icon icono = new ImageIcon(RegistroClientes.class.getResource("/img/error.png"));
				    	JOptionPane.showMessageDialog(null, "El formato de correo es inv�lido, verifique que lo ingres� de forma correcta. Ejemplo: Prueba@Proveedor.com","Error" , JOptionPane.ERROR_MESSAGE,icono); 
					}
				}
			}
		});
		accion.setForeground(Color.WHITE);
		accion.setBorderPainted(false);
		accion.setBackground(tema.getPrincipal());
		accion.setBounds(178, 420, 120, 36);
		contentPanel.add(accion);
		
		if (cliente.equals("0")){
			accion.setText("Registrar");
			accion.setIcon(new ImageIcon(RegistroClientes.class.getResource("/img/add.png")));
		}else{
			accion.setText("Guardar");
			accion.setIcon(new ImageIcon(RegistroClientes.class.getResource("/img/save.png")));
		}
		
		JLabel label = new JLabel("(*)");
		label.setForeground(Color.RED);
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(288, 24, 12, 21);
		contentPanel.add(label);
		
		JLabel label_1 = new JLabel("(*)");
		label_1.setForeground(Color.RED);
		label_1.setFont(new Font("Arial", Font.BOLD, 11));
		label_1.setBounds(124, 80, 12, 21);
		contentPanel.add(label_1);
		
		JLabel label_2 = new JLabel("(*)");
		label_2.setForeground(Color.RED);
		label_2.setFont(new Font("Arial", Font.BOLD, 11));
		label_2.setBounds(325, 79, 12, 21);
		contentPanel.add(label_2);
		
		JLabel label_3 = new JLabel("(*)");
		label_3.setForeground(Color.RED);
		label_3.setFont(new Font("Arial", Font.BOLD, 11));
		label_3.setBounds(407, 80, 12, 21);
		contentPanel.add(label_3);
		
		JLabel label_4 = new JLabel("(*)");
		label_4.setForeground(Color.RED);
		label_4.setFont(new Font("Arial", Font.BOLD, 11));
		label_4.setBounds(621, 79, 12, 21);
		contentPanel.add(label_4);
		
		JLabel label_8 = new JLabel("(*)");
		label_8.setForeground(Color.RED);
		label_8.setFont(new Font("Arial", Font.BOLD, 11));
		label_8.setBounds(607, 136, 12, 21);
		contentPanel.add(label_8);
		
		JLabel lblLosCamposMarcados = new JLabel("LOS CAMPOS MARCADOS CON       SON OBLIGATORIOS");
		lblLosCamposMarcados.setForeground(Color.GRAY);
		lblLosCamposMarcados.setFont(new Font("Arial", Font.ITALIC, 11));
		lblLosCamposMarcados.setBounds(367, 48, 323, 21);
		contentPanel.add(lblLosCamposMarcados);
		
		JLabel label_18 = new JLabel("(*)");
		label_18.setForeground(Color.RED);
		label_18.setFont(new Font("Arial", Font.BOLD, 11));
		label_18.setBounds(538, 48, 12, 21);
		contentPanel.add(label_18);
		
		if (cliente.equals("0")){
			titulo = "REGISTRO DE CLIENTES";
		}else{
			titulo = "MODIFICAR CLENTES";
		}
	}
}
