package views;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.sql.SQLException;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import models.Cliente;
import utility.Conexion;
import utility.Estilo;
import utility.Tables;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.DebugGraphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class AdministrarClientes extends JInternalFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textField;
	public Tables listaclientes;

	public AdministrarClientes(Conexion conn) throws SQLException, PropertyVetoException {
		setFrameIcon(new ImageIcon(AdministrarClientes.class.getResource("/img/ico_grupo.png")));
		setIconifiable(true);
		setClosable(true);
		setTitle("Administrar Clientes");
		setBounds(100, 100, 920, 502);
		getContentPane().setLayout(null);
		Cliente Ocliente = new Cliente();
		Ocliente.setConn(conn);
		
		JLabel label = new JLabel("BUSCAR:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(31, 25, 56, 21);
		getContentPane().add(label);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				listaclientes.setClearGrid();
				try {
					listaclientes.RefreshTableButtomDouble(Ocliente.FiltraClientes(textField.getText()));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
				if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE ) {
			         evt.consume();
			    }		
			}
		});
		textField.setToolTipText("Informe o nome ou sobrenome para localizar.");
		textField.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textField.setColumns(10);
		textField.setBounds(89, 25, 346, 21);
		getContentPane().add(textField);
		
		String campos[] = {"ID","TIPO","DOCUMENTO","CPF","NOME", "SOBRENOME","PAIS","ESTADO","",""};
		int ancho[] = {50,50,50,50,50,50,50,50,1,1};
		int editable[] = null;
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(31, 76, 840, 328);
		listaclientes = new Tables(campos, editable, ancho, null);
		listaclientes.setConn(conn);
		listaclientes.TableButtomDoubleT(Ocliente.ListarClientes());
		
		getContentPane().add(scrollPane);
		scrollPane.setViewportView(listaclientes);
		
		Estilo tema = new Estilo();
		
		JButton agregar = new JButton("NOVO");
		agregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegistroClientes modal;
				try {
					boolean accion;
					if(Ocliente.getConn().getRowsCount(Ocliente.ListarClientes()) > 0){
						accion = true;
					}else{
						accion = false;
					}
					modal = new RegistroClientes("0",conn);
					AdministrarClientes ventana = new AdministrarClientes(conn);
					modal.setLocationRelativeTo(ventana);
					modal.setVisible(true);
					listaclientes.setClearGrid();
					if(accion){
						listaclientes.RefreshTableButtomDouble(Ocliente.ListarClientes());
					}else{
						listaclientes.TableButtomDoubleT(Ocliente.ListarClientes());
					}
				} catch (SQLException | PropertyVetoException e) {
					e.printStackTrace();
				}
			}
		});
		agregar.setIcon(new ImageIcon(AdministrarClientes.class.getResource("/img/ico_agregar.png")));
		agregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				agregar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				agregar.setBackground(tema.getPrincipal());
			}
		});
		agregar.setForeground(Color.WHITE);
		agregar.setBorderPainted(false);
		agregar.setBackground(tema.getPrincipal());
		agregar.setBounds(742, 44, 129, 21);
		getContentPane().add(agregar);
		
		JButton actualizar = new JButton("Atualizar");
		actualizar.setIcon(new ImageIcon(AdministrarClientes.class.getResource("/img/refresh.png")));
		actualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					listaclientes.setClearGrid();
					listaclientes.RefreshTableButtomDouble(Ocliente.ListarClientes());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		actualizar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				actualizar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				actualizar.setBackground(tema.getPrincipal());
			}
		});
		actualizar.setRolloverEnabled(false);
		actualizar.setRequestFocusEnabled(false);
		actualizar.setForeground(Color.WHITE);
		actualizar.setDoubleBuffered(true);
		actualizar.setDefaultCapable(false);
		actualizar.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		actualizar.setBorder(null);
		actualizar.setBackground(tema.getPrincipal());
		actualizar.setAutoscrolls(true);
		actualizar.setBounds(205, 415, 164, 44);
		getContentPane().add(actualizar);
		
		JButton volver = new JButton("Sair");
		volver.setIcon(new ImageIcon(AdministrarClientes.class.getResource("/img/back.png")));
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				volver.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				volver.setBackground(tema.getPrincipal());
			}
		});
		volver.setForeground(Color.WHITE);
		volver.setBorderPainted(false);
		volver.setBackground(tema.getPrincipal());
		volver.setBounds(531, 415, 164, 44);
		getContentPane().add(volver);

	}
}
