package views;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import models.Moneda;
import utility.Conexion;
import utility.Estilo;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.sql.SQLException;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class RegistroMonedas extends JDialog {


	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField taza;
	private JTextField nombre;
	@SuppressWarnings("rawtypes")
	private JComboBox pais;

	public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
		String cadena= (jTextfieldS.getText()).toUpperCase();
		jTextfieldS.setText(cadena);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public RegistroMonedas(Integer moneda, Conexion conn) throws SQLException {
		setModal(true);
		String titulo;
		if(moneda == 0){
			titulo = "Registro de Monedas";
		}else{
			titulo = "Modificar Moneda";
		}
		
		Moneda Omoneda = new Moneda();
		Omoneda.setConn(conn);
		Omoneda.TraerRegistro(moneda);
		Estilo tema = new Estilo();
		
		setTitle(titulo);
		setResizable(false);
		setBounds(100, 100, 726, 194);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblCodigo = new JLabel("TAXA:");
		lblCodigo.setFont(new Font("Arial", Font.BOLD, 11));
		lblCodigo.setBounds(537, 24, 52, 21);
		contentPanel.add(lblCodigo);
		
		taza = new JTextField();
		taza.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		taza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nombre.requestFocus();
			}
		});
		taza.setText((String) null);
		taza.setColumns(10);
		taza.setBounds(536, 48, 154, 21);
		taza.setText(Omoneda.getTaza()+"");
		contentPanel.add(taza);
		
		JLabel lblNombre = new JLabel("NOMBRE DE LA MONEDA:");
		lblNombre.setFont(new Font("Arial", Font.BOLD, 11));
		lblNombre.setBounds(194, 24, 147, 21);
		contentPanel.add(lblNombre);
		
		nombre = new JTextField();
		nombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				convertiraMayusculasEnJtextfield(nombre);
			}
		});
		nombre.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				nombre.nextFocus();
			}
		});
		nombre.setText((String) null);
		nombre.setColumns(10);
		nombre.setText(Omoneda.getNombre());
		nombre.setBounds(194, 48, 323, 21);
		contentPanel.add(nombre);
		
		JButton volver = new JButton("Volver");
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				volver.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				volver.setBackground(tema.getPrincipal());
			}
		});
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		
		JLabel lblPais = new JLabel("PAIS:");
		lblPais.setFont(new Font("Arial", Font.BOLD, 11));
		lblPais.setBounds(20, 24, 44, 21);
		contentPanel.add(lblPais);
		
		pais = new JComboBox();
		pais.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				pais.nextFocus();
			}
		});
		pais.setFont(new Font("Arial", Font.PLAIN, 11));
		pais.setModel(new DefaultComboBoxModel(new String[] {"BOLIVIA", "BRASIL", "COLOMBIA", "CHILE", "VENEZUELA"}));
		pais.setBounds(20, 48, 154, 20);
		pais.setSelectedItem(Omoneda.getPais());
		contentPanel.add(pais);
		volver.setIcon(new ImageIcon(RegistroMonedas.class.getResource("/img/back.png")));
		volver.setForeground(Color.WHITE);
		volver.setBorderPainted(false);
		volver.setBackground(tema.getPrincipal());
		volver.setBounds(436, 111, 120, 36);
		contentPanel.add(volver);
		
		
		JButton accion = new JButton("");
		accion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				accion.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				accion.setBackground(tema.getPrincipal());
			}
		});
		accion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(taza.getText().equals("") || nombre.getText().equals("") || pais.getSelectedItem().toString().equals("")){
					Icon icono = new ImageIcon(RegistroMonedas.class.getResource("/img/error.png"));
					JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
				}else{
					boolean status = true;
					Moneda Omoneda = new Moneda(nombre.getText(), pais.getSelectedItem().toString(),Integer.parseInt(taza.getText()));
			    	Omoneda.setConn(conn);
					if (moneda == 0){
			    		try {
			    			status = Omoneda.Registrar();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			    	}else{
			    		try {
							Omoneda.Editar(moneda);
			    		} catch (SQLException e) {
							// TODO Auto-generated catch block
			    			e.printStackTrace();
			    		}
				   	}
					if (status == true){
						taza.setText("");
	   					nombre.setText("");
	   					pais.setSelectedItem("");
						dispose();
					} 	   
				}
			}
		});
		accion.setForeground(Color.WHITE);
		accion.setBorderPainted(false);
		accion.setBackground(tema.getPrincipal());
		accion.setBounds(190, 111, 120, 36);
		contentPanel.add(accion);
		
		if (moneda == 0){
			accion.setText("Registrar");
			accion.setIcon(new ImageIcon(RegistroMonedas.class.getResource("/img/add.png")));
		}else{
			accion.setText("Guardar");
			accion.setIcon(new ImageIcon(RegistroMonedas.class.getResource("/img/save.png")));
		}
		
		JLabel label = new JLabel("(*)");
		label.setForeground(Color.RED);
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(577, 24, 12, 21);
		contentPanel.add(label);
		
		JLabel label_7 = new JLabel("(*)");
		label_7.setForeground(Color.RED);
		label_7.setFont(new Font("Arial", Font.BOLD, 11));
		label_7.setBounds(52, 24, 12, 21);
		contentPanel.add(label_7);
		
		JLabel label_11 = new JLabel("(*)");
		label_11.setForeground(Color.RED);
		label_11.setFont(new Font("Arial", Font.BOLD, 11));
		label_11.setBounds(330, 24, 12, 21);
		contentPanel.add(label_11);
		
		JLabel lblLosCamposMarcados = new JLabel("LOS CAMPOS MARCADOS CON       SON OBLIGATORIOS");
		lblLosCamposMarcados.setForeground(Color.GRAY);
		lblLosCamposMarcados.setFont(new Font("Arial", Font.ITALIC, 11));
		lblLosCamposMarcados.setBounds(392, 79, 298, 21);
		contentPanel.add(lblLosCamposMarcados);
		
		JLabel label_18 = new JLabel("(*)");
		label_18.setForeground(Color.RED);
		label_18.setFont(new Font("Arial", Font.BOLD, 11));
		label_18.setBounds(563, 79, 12, 21);
		contentPanel.add(label_18);
		
		if (moneda == 0){
			titulo = "REGISTRO DE MONEDAS";
		}else{
			titulo = "MODIFICAR MONEDAS";
		}
	}
}
