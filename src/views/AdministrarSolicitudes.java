package views;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.sql.SQLException;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import models.Solicitud;
import utility.Conexion;
import utility.Estilo;
import utility.Tables;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.DebugGraphics;
import javax.swing.Icon;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class AdministrarSolicitudes extends JInternalFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textField;
	public Tables listasolicitudes;
	private JButton actualizar;
	private JButton btnCambiarEstado;

	public AdministrarSolicitudes(Conexion conn) throws SQLException, PropertyVetoException {
		setFrameIcon(new ImageIcon(AdministrarSolicitudes.class.getResource("/img/comprobante.png")));
		setIconifiable(true);
		setClosable(true);
		setTitle("Solicitacoes de Envio");
		setBounds(100, 100, 920, 502);
		getContentPane().setLayout(null);
		Solicitud Osolicitud = new Solicitud();
		Osolicitud.setConn(conn);
		
		JLabel label = new JLabel("BUSCAR:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(31, 25, 56, 21);
		getContentPane().add(label);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				listasolicitudes.setClearGrid();
				try {
					listasolicitudes.Refresh(Osolicitud.FiltraSolicitudes(textField.getText()));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
				if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE ) {
			         evt.consume();
			    }		
			}
		});
		textField.setToolTipText("Digite o C�digo da Transa��o!");
		textField.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textField.setColumns(10);
		textField.setBounds(89, 25, 346, 21);
		getContentPane().add(textField);
		
		String campos[] = {"ID","CLIENTE","BENEFICIARIO","MOEDA","R$ REAL", "TAXA","TOTAL","ESTADO"};
		int ancho[] = {1,50,50,50,50,50,50,50};
		int editable[] = null;
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(31, 72, 840, 328);
		listasolicitudes = new Tables(campos, editable, ancho, null);
		listasolicitudes.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(!listasolicitudes.getValueAt(listasolicitudes.getSelectedRow(), 7).toString().equals("PROCESSANDO")){
					btnCambiarEstado.setEnabled(false);
				}else{
					btnCambiarEstado.setEnabled(true);
				}
			}
		});
		listasolicitudes.Listar(Osolicitud.ListarSolicitudes());
		
		getContentPane().add(scrollPane);
		scrollPane.setViewportView(listasolicitudes);
		
		Estilo tema = new Estilo();
		
		actualizar = new JButton("Detalhes");
		actualizar.setIcon(new ImageIcon(AdministrarSolicitudes.class.getResource("/img/file.png")));
		actualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Solicitud Osolicitud = new Solicitud();
				Osolicitud.setConn(conn);
				if(listasolicitudes.getSelectedRow() != -1){
					Icon icono = new ImageIcon(Solicitud.class.getResource("/img/informacion.png"));
					if(listasolicitudes.getValueAt(listasolicitudes.getSelectedRow(), 7).toString().equals("PROCESSANDO")){
						JOptionPane.showMessageDialog(null, "Para ver el datetalle la Solicitud debe entrar en Revision", "Informacion", JOptionPane.INFORMATION_MESSAGE, icono);
						int id;
						try {
							id = Osolicitud.RetornaID(listasolicitudes.getValueAt(listasolicitudes.getSelectedRow(), 0).toString());
							listasolicitudes.CambioEstadoSolicitud(id, listasolicitudes.getSelectedRow(), 7);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}		
					}else{
						try {
							int id = Osolicitud.RetornaID(listasolicitudes.getValueAt(listasolicitudes.getSelectedRow(), 0).toString());
							DetalleSolicitud modal = new DetalleSolicitud(id, conn);
							AdministrarSolicitudes ventana = new AdministrarSolicitudes(conn);
							modal.setLocationRelativeTo(ventana);
							modal.setVisible(true);
							listasolicitudes.setClearGrid();
							listasolicitudes.Refresh(Osolicitud.ListarSolicitudes());
						} catch (SQLException e1) {
							e1.printStackTrace();
						} catch (PropertyVetoException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}			
				}else{
					Icon icono = new ImageIcon(AdministrarSolicitudes.class.getResource("/img/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar una Solicitud para ver su detalle","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}	
			}
		});
		actualizar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				actualizar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				actualizar.setBackground(tema.getPrincipal());
			}
		});
		actualizar.setRolloverEnabled(false);
		actualizar.setRequestFocusEnabled(false);
		actualizar.setForeground(Color.WHITE);
		actualizar.setDoubleBuffered(true);
		actualizar.setDefaultCapable(false);
		actualizar.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		actualizar.setBorder(null);
		actualizar.setBackground(tema.getPrincipal());
		actualizar.setAutoscrolls(true);
		actualizar.setBounds(363, 415, 164, 44);
		getContentPane().add(actualizar);
		
		JButton volver = new JButton("Sair");
		volver.setIcon(new ImageIcon(AdministrarSolicitudes.class.getResource("/img/back.png")));
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				volver.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				volver.setBackground(tema.getPrincipal());
			}
		});
		volver.setForeground(Color.WHITE);
		volver.setBorderPainted(false);
		volver.setBackground(tema.getPrincipal());
		volver.setBounds(633, 415, 164, 44);
		getContentPane().add(volver);
		
		btnCambiarEstado = new JButton("Altera Status");
		btnCambiarEstado.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnCambiarEstado.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnCambiarEstado.setBackground(tema.getPrincipal());
			}
		});
		btnCambiarEstado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Solicitud Osolicitud = new Solicitud();
				Osolicitud.setConn(conn);
				if(listasolicitudes.getSelectedRow() != -1){
					try {
						int id = Osolicitud.RetornaID(listasolicitudes.getValueAt(listasolicitudes.getSelectedRow(), 0).toString());
						listasolicitudes.CambioEstadoSolicitud(id, listasolicitudes.getSelectedRow(), 7);
					} catch (SQLException e) {
						e.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(AdministrarSolicitudes.class.getResource("/img/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar una Solicitud para cambiar su estado","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}	
			}
		});
		btnCambiarEstado.setIcon(new ImageIcon(AdministrarSolicitudes.class.getResource("/img/refresh.png")));
		btnCambiarEstado.setRolloverEnabled(false);
		btnCambiarEstado.setRequestFocusEnabled(false);
		btnCambiarEstado.setForeground(Color.WHITE);
		btnCambiarEstado.setDoubleBuffered(true);
		btnCambiarEstado.setDefaultCapable(false);
		btnCambiarEstado.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		btnCambiarEstado.setBorder(null);
		btnCambiarEstado.setBackground(new Color(52, 165, 197));
		btnCambiarEstado.setAutoscrolls(true);
		btnCambiarEstado.setBounds(93, 415, 164, 44);
		getContentPane().add(btnCambiarEstado);

	}
}
