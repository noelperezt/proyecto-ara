package views;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import models.Usuario;
import utility.Conexion;
import utility.Estilo;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.sql.SQLException;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EditarUsuarios extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField nombre;
	private JTextField usuario;
	
	public EditarUsuarios(int id_usuario, Conexion conn) throws SQLException {
		setModal(true);
		
		setTitle("Modificar Usuario");
		setResizable(false);
		setBounds(100, 100, 406, 181);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
	
		Estilo tema = new Estilo();
		Usuario Ouser = new Usuario();
		Ouser.setConn(conn);
		Ouser.TraerRegistro(id_usuario);
		
		JButton volver = new JButton("Volver");
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				volver.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				volver.setBackground(tema.getPrincipal());
			}
		});
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		volver.setIcon(new ImageIcon(EditarUsuarios.class.getResource("/img/back.png")));
		volver.setForeground(Color.WHITE);
		volver.setBorderPainted(false);
		volver.setBackground(tema.getPrincipal());
		volver.setBounds(232, 100, 120, 36);
		contentPanel.add(volver);
		
		
		JButton accion = new JButton("");
		accion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				accion.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				accion.setBackground(tema.getPrincipal());
			}
		});
		accion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(nombre.getText().equals("") || usuario.getText().equals("") ){
					Icon icono = new ImageIcon(EditarUsuarios.class.getResource("/img/error.png"));
					JOptionPane.showMessageDialog(null, "Hay campos vacios","Error" , JOptionPane.ERROR_MESSAGE,icono);
				}else{
					Usuario Ouser = new Usuario(nombre.getText(),usuario.getText(),"");
					Ouser.setConn(conn);
					try {
						Ouser.Editar(id_usuario);
						nombre.setText("");
						usuario.setText("");	
						dispose();	
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}	
			}
		});
		
		accion.setText("Guardar");
		accion.setIcon(new ImageIcon(EditarUsuarios.class.getResource("/img/save.png")));
		accion.setForeground(Color.WHITE);
		accion.setBorderPainted(false);
		accion.setBackground(tema.getPrincipal());
		accion.setBounds(61, 100, 120, 36);
		contentPanel.add(accion);
		
		JLabel label = new JLabel("NOMBRE:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(38, 24, 55, 22);
		contentPanel.add(label);
		
		nombre = new JTextField();
		nombre.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				nombre.nextFocus();
			}
		});
		nombre.setColumns(10);
		nombre.setBounds(99, 24, 268, 22);
		nombre.setText(Ouser.getNombre());
		contentPanel.add(nombre);
		
		JLabel label_1 = new JLabel("USUARIO:");
		label_1.setFont(new Font("Arial", Font.BOLD, 11));
		label_1.setBounds(38, 57, 55, 22);
		contentPanel.add(label_1);
		
		usuario = new JTextField();
		usuario.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				usuario.nextFocus();
			}
		});
		usuario.setColumns(10);
		usuario.setBounds(99, 57, 268, 22);
		usuario.setText(Ouser.getUsuario());
		contentPanel.add(usuario);
	}
}
