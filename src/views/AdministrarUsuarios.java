package views;

import javax.swing.JInternalFrame;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import models.Usuario;
import utility.Conexion;
import utility.Estilo;
import utility.Tables;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class AdministrarUsuarios extends JInternalFrame {
	private JTextField textField;
	private Tables listausuarios;
	
	public AdministrarUsuarios(Conexion conn) throws SQLException {
		setClosable(true);
		setIconifiable(true);
		setTitle("Administrar Usuarios");
		setFrameIcon(new ImageIcon(AdministrarUsuarios.class.getResource("/img/user.png")));
		setBounds(100, 100, 696, 331);
		getContentPane().setLayout(null);
		
		Estilo tema = new Estilo();
		String campos[] = {"ID","USUARIO","NOMBRE","ESTADO"};
		int ancho[] = {1,1,170,1};
		int editable[] = null;
		Usuario Ousuario = new Usuario();
		Ousuario.setConn(conn);
		
		JLabel label = new JLabel("BUSCAR:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(25, 27, 56, 19);
		getContentPane().add(label);
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(25, 66, 497, 210);
		listausuarios = new Tables(campos, editable, ancho, null);
		listausuarios.Listar(Ousuario.ListarUsuarios());
		getContentPane().add(scrollPane);
		scrollPane.setViewportView(listausuarios);
		
		JButton btnNovo = new JButton("Novo");
		btnNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					boolean accion;
					if(Ousuario.getConexion().getRowsCount(Ousuario.ListarUsuarios()) > 0){
						accion = true;
					}else{
						accion = false;
					}
					RegistroUsuarios modal = new RegistroUsuarios(conn);
					AdministrarUsuarios ventana = new AdministrarUsuarios(conn);
					modal.setLocationRelativeTo(ventana);
					modal.setVisible(true);
					listausuarios.setClearGrid();
					if(accion){
						listausuarios.Refresh(Ousuario.ListarUsuarios());
					}else{
						listausuarios.Listar(Ousuario.ListarUsuarios());
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		});
		btnNovo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnNovo.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				btnNovo.setBackground(tema.getPrincipal());
			}
		});
		btnNovo.setIcon(new ImageIcon(AdministrarUsuarios.class.getResource("/img/add.png")));
		btnNovo.setForeground(Color.WHITE);
		btnNovo.setBorderPainted(false);
		btnNovo.setBackground(tema.getPrincipal());
		btnNovo.setBounds(532, 66, 125, 33);
		getContentPane().add(btnNovo);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				try {
					listausuarios.setClearGrid();
					listausuarios.Refresh(Ousuario.FiltraUsuario(textField.getText()));
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		textField.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textField.setToolTipText("Busqueda por Usuario");
		textField.setColumns(10);
		textField.setBounds(81, 27, 346, 21);
		getContentPane().add(textField);
		
		JButton button_1 = new JButton("Editar");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listausuarios.getSelectedRow() != -1){
					int id = Integer.parseInt(listausuarios.getValueAt(listausuarios.getSelectedRow(), 0).toString());
					try {
						EditarUsuarios modal = new EditarUsuarios(id,conn);
						AdministrarUsuarios ventana = new AdministrarUsuarios(conn);
						modal.setLocationRelativeTo(ventana);
						modal.setVisible(true);
						listausuarios.setClearGrid();
						listausuarios.Refresh(Ousuario.ListarUsuarios());
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(AdministrarUsuarios.class.getResource("/img/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un usuario para editar","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button_1.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				button_1.setBackground(tema.getPrincipal());
			}
		});
		button_1.setIcon(new ImageIcon(AdministrarUsuarios.class.getResource("/img/edit.png")));
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(tema.getPrincipal());
		button_1.setBounds(532, 110, 125, 33);
		getContentPane().add(button_1);
		
		JButton button_2 = new JButton("Estado");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listausuarios.getSelectedRow() != -1){
					int id = Integer.parseInt(listausuarios.getValueAt(listausuarios.getSelectedRow(), 0).toString());
					try {
						listausuarios.CambioEstadoUsuario(id, listausuarios.getSelectedRow(), 3);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(AdministrarUsuarios.class.getResource("/img/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un usuario para cambiar su estado","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		button_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button_2.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				button_2.setBackground(tema.getPrincipal());
			}
		});
		button_2.setIcon(new ImageIcon(AdministrarUsuarios.class.getResource("/img/del.png")));
		button_2.setToolTipText("Cambiar Estado");
		button_2.setForeground(Color.WHITE);
		button_2.setBorderPainted(false);
		button_2.setBackground(tema.getPrincipal());
		button_2.setBounds(532, 154, 125, 33);
		getContentPane().add(button_2);
		
		JButton button_3 = new JButton("Reiniciar");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listausuarios.getSelectedRow() != -1){
					int id = Integer.parseInt(listausuarios.getValueAt(listausuarios.getSelectedRow(), 0).toString());
					Usuario Ouser = new Usuario();
					Ouser.setConn(conn);
					try {
						Ouser.ReiniciarClave(id);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(AdministrarUsuarios.class.getResource("/img/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un usuario para reiniciar su clave de acceso","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		button_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button_3.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				button_3.setBackground(tema.getPrincipal());
			}
		});
		button_3.setIcon(new ImageIcon(AdministrarUsuarios.class.getResource("/img/reset.png")));
		button_3.setToolTipText("Reiniciar Clave");
		button_3.setForeground(Color.WHITE);
		button_3.setBorderPainted(false);
		button_3.setBackground(tema.getPrincipal());
		button_3.setBounds(532, 198, 125, 33);
		getContentPane().add(button_3);
		
		JButton btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSair.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnSair.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				btnSair.setBackground(tema.getPrincipal());
			}
		});
		btnSair.setIcon(new ImageIcon(AdministrarUsuarios.class.getResource("/img/back.png")));
		btnSair.setForeground(Color.WHITE);
		btnSair.setBorderPainted(false);
		btnSair.setBackground(tema.getPrincipal());
		btnSair.setBounds(532, 242, 125, 33);
		getContentPane().add(btnSair);

	}
}
