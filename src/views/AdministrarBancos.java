package views;


import javax.swing.JInternalFrame;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import models.Banco;
import utility.Conexion;
import utility.Estilo;
import utility.Tables;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@SuppressWarnings("serial")
public class AdministrarBancos extends JInternalFrame {
	private JTextField textField;
	private Tables listabancos;

	public AdministrarBancos(Conexion conn) throws SQLException, PropertyVetoException {
		setIcon(true);
		setTitle("Administraci\u00F3n de Bancos");
		setFrameIcon(new ImageIcon(AdministrarBancos.class.getResource("/img/builder.png")));
		setClosable(true);
		setIconifiable(true);
		setBounds(100, 100, 731, 293);
		getContentPane().setLayout(null);
		
		String campos[] = {"ID","C�DIGO", "BANCO", "PAIS", "ESTADO"};
		int ancho[] = {1,10,200,40,10};
		int editable[] = null;
		Banco Obanco = new Banco();
		Obanco.setConn(conn);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(31, 62, 519, 177);
		listabancos = new Tables(campos, editable, ancho, null);
		listabancos.Listar(Obanco.ListarBancos());
		
		getContentPane().add(scrollPane);
		scrollPane.setViewportView(listabancos);
		
		JLabel label = new JLabel("BUSCAR:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(31, 22, 56, 21);
		getContentPane().add(label);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				try {
					listabancos.setClearGrid();
					listabancos.Refresh(Obanco.FiltraBancos(textField.getText()));
				} catch (SQLException e) {
					e.printStackTrace();
				}	
			}
		});
		textField.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textField.setColumns(10);
		textField.setBounds(84, 22, 346, 21);
		getContentPane().add(textField);
		
		Estilo tema = new Estilo();
		
		JButton btnAdicionar = new JButton("Adicionar");
		btnAdicionar.setBorderPainted(false);
		btnAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					boolean accion;
					if(Obanco.getConn().getRowsCount(Obanco.ListarBancos()) > 0){
						accion = true;
					}else{
						accion = false;
					}
					RegistroBancos modal = new RegistroBancos(0,conn);
					AdministrarBancos ventana = new AdministrarBancos(conn);
					modal.setLocationRelativeTo(ventana);
					modal.setVisible(true);
					listabancos.setClearGrid();
					if (accion){
						listabancos.Refresh(Obanco.ListarBancos());
					}else{
						listabancos.Listar(Obanco.ListarBancos());
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (PropertyVetoException e) {
					e.printStackTrace();
				}
			}
		});
		btnAdicionar.setIcon(new ImageIcon(AdministrarBancos.class.getResource("/img/add.png")));
		btnAdicionar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnAdicionar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				btnAdicionar.setBackground(tema.getPrincipal());
			}
		});
		btnAdicionar.setForeground(Color.WHITE);
		btnAdicionar.setBackground(tema.getPrincipal());
		btnAdicionar.setBounds(570, 62, 123, 35);
		getContentPane().add(btnAdicionar);
		
		JButton button_1 = new JButton("Editar");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listabancos.getSelectedRow() != -1){
					int id = Integer.parseInt(listabancos.getValueAt(listabancos.getSelectedRow(), 0).toString());
					try {
						RegistroBancos modal = new RegistroBancos(id,conn);
						AdministrarBancos ventana = new AdministrarBancos(conn);
						modal.setLocationRelativeTo(ventana);
						modal.setVisible(true);
						listabancos.setClearGrid();
						listabancos.Refresh(Obanco.ListarBancos());
					} catch (SQLException | PropertyVetoException e1) {
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(AdministrarBancos.class.getResource("/img/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un Banco para editar","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		button_1.setIcon(new ImageIcon(AdministrarBancos.class.getResource("/img/edit.png")));
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button_1.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				button_1.setBackground(tema.getPrincipal());
			}
		});
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(tema.getPrincipal());
		button_1.setBounds(570, 108, 123, 35);
		getContentPane().add(button_1);
		
		JButton btnAtivardesativar = new JButton("Status");
		btnAtivardesativar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listabancos.getSelectedRow() != -1){
					int id = Integer.parseInt(listabancos.getValueAt(listabancos.getSelectedRow(), 0).toString());
					try {
						listabancos.CambioEstadoBanco(id, listabancos.getSelectedRow(), 4);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(AdministrarBancos.class.getResource("/img/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un Banco para cambiar su estado","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnAtivardesativar.setIcon(new ImageIcon(AdministrarBancos.class.getResource("/img/del.png")));
		btnAtivardesativar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnAtivardesativar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				btnAtivardesativar.setBackground(tema.getPrincipal());
			}
		});
		btnAtivardesativar.setForeground(Color.WHITE);
		btnAtivardesativar.setBorderPainted(false);
		btnAtivardesativar.setBackground(tema.getPrincipal());
		btnAtivardesativar.setBounds(570, 154, 123, 35);
		getContentPane().add(btnAtivardesativar);
		
		JButton btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSair.setIcon(new ImageIcon(AdministrarBancos.class.getResource("/img/back.png")));
		btnSair.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnSair.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				btnSair.setBackground(tema.getPrincipal());
			}
		});
		btnSair.setForeground(Color.WHITE);
		btnSair.setBorderPainted(false);
		btnSair.setBackground(tema.getPrincipal());
		btnSair.setBounds(570, 202, 123, 35);
		getContentPane().add(btnSair);

	}

}
