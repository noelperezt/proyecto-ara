package views;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import models.Beneficiario;
import utility.Conexion;
import utility.Estilo;
import utility.Magician;
import utility.Tables;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JScrollPane;

public class RegistroBeneficiarios extends JDialog {


	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField codigo;
	private JTextField doc;
	private JTextField cpf;
	private JTextField nombre;
	private JTextField apellido;
	private JTextField profesion;
	private JTextField celular;
	private JTextField residencia;
	private JTextField email;
	@SuppressWarnings("rawtypes")
	private JComboBox tipo;
	private JDateChooser nac;
	private Tables listacuentas;
	@SuppressWarnings("rawtypes")
	private JComboBox pais;

	public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
		String cadena= (jTextfieldS.getText()).toUpperCase();
		jTextfieldS.setText(cadena);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public RegistroBeneficiarios(String beneficiario, Conexion conn) throws SQLException {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				/*if(listacuentas.getRowCount() == 0 ){
					Icon icono = new ImageIcon(RegistroBeneficiarios.class.getResource("/img/error.png"));
					JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
				}else{
					dispose();
				}*/
			}
		});
		setModal(true);
		String titulo;
		if(beneficiario.equals("0")){
			titulo = "Registro de Beneficiarios";
		}else{
			titulo = "Modificar Beneficiario";
		}
		
		Beneficiario Obeneficiario = new Beneficiario();
		Obeneficiario.setConn(conn);
		Obeneficiario.TraerRegistro(beneficiario);
		Estilo tema = new Estilo();
		
		setTitle(titulo);
		setResizable(false);
		setBounds(100, 100, 726, 491);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblRazobSocial = new JLabel("CODIGO:");
		lblRazobSocial.setFont(new Font("Arial", Font.BOLD, 11));
		lblRazobSocial.setBounds(22, 24, 52, 21);
		contentPanel.add(lblRazobSocial);
		
		codigo = new JTextField();
		codigo.setEditable(false);
		codigo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nombre.requestFocus();
			}
		});
		codigo.setText((String) null);
		codigo.setColumns(10);
		codigo.setBounds(22, 48, 154, 21);
		if(beneficiario.equals("0")){
			//codigo.setText(mago.RetornaCodigo(Obeneficiario.RetornaSerie()));
		}else{
			codigo.setText(Obeneficiario.getCodigo());
		}	
		contentPanel.add(codigo);
	
		
		JLabel lblTipoDeCliente = new JLabel("TIPO DE CLIENTE:");
		lblTipoDeCliente.setFont(new Font("Arial", Font.BOLD, 11));
		lblTipoDeCliente.setBounds(194, 24, 154, 21);
		contentPanel.add(lblTipoDeCliente);
		
		tipo = new JComboBox();
		tipo.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				tipo.nextFocus();
			}
		});
		tipo.setFont(new Font("Arial", Font.PLAIN, 11));
		tipo.setModel(new DefaultComboBoxModel(new String[] {"V-VENEZOLANO", "E-ESTRANGEIRO", "J-PESSOAJURIDICA", "P-PESSOA FISICA", "G-GOVERNO"}));
		tipo.setBounds(196, 48, 154, 20);
		tipo.setSelectedItem(Obeneficiario.getTipo());
		contentPanel.add(tipo);
		
		JLabel lblDocumentoDeIdentidad = new JLabel("DOC.DE IDENTIDAD:");
		lblDocumentoDeIdentidad.setFont(new Font("Arial", Font.BOLD, 11));
		lblDocumentoDeIdentidad.setBounds(22, 80, 154, 21);
		contentPanel.add(lblDocumentoDeIdentidad);
		
		doc = new JTextField();
		doc.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		doc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cpf.requestFocus();
			}
		});
		doc.setText((String) null);
		doc.setColumns(10);
		doc.setText(Obeneficiario.getDocumento());
		doc.setBounds(22, 104, 154, 21);
		contentPanel.add(doc);
		
		JLabel lblCpfcnpj = new JLabel("CPF/CNPJ (Passaporte):");
		lblCpfcnpj.setFont(new Font("Arial", Font.BOLD, 11));
		lblCpfcnpj.setBounds(194, 80, 154, 21);
		contentPanel.add(lblCpfcnpj);
		
		cpf = new JTextField();
		cpf.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		cpf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nombre.requestFocus();
			}
		});
		cpf.setText((String) null);
		cpf.setColumns(10);
		cpf.setText(Obeneficiario.getCpf());
		cpf.setBounds(194, 104, 154, 21);
		contentPanel.add(cpf);
		
		JLabel lblNome = new JLabel("NOME:");
		lblNome.setFont(new Font("Arial", Font.BOLD, 11));
		lblNome.setBounds(367, 80, 52, 21);
		contentPanel.add(lblNome);
		
		nombre = new JTextField();
		nombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				convertiraMayusculasEnJtextfield(nombre);
			}
		});
		nombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				apellido.requestFocus();
			}
		});
		nombre.setText((String) null);
		nombre.setColumns(10);
		nombre.setText(Obeneficiario.getNombre());
		nombre.setBounds(367, 104, 154, 21);
		contentPanel.add(nombre);
		
		JLabel lblNomeDoMeio = new JLabel("NOME DO MEIO:");
		lblNomeDoMeio.setFont(new Font("Arial", Font.BOLD, 11));
		lblNomeDoMeio.setBounds(536, 80, 154, 21);
		contentPanel.add(lblNomeDoMeio);
		
		apellido = new JTextField();
		apellido.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				convertiraMayusculasEnJtextfield(apellido);
			}
		});
		apellido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nac.requestFocus();
			}
		});
		apellido.setText((String) null);
		apellido.setColumns(10);
		apellido.setText(Obeneficiario.getApellido());
		apellido.setBounds(536, 104, 154, 21);
		contentPanel.add(apellido);
		
		JLabel lblFechaNascimento = new JLabel("FECHA NASCIMENTO:");
		lblFechaNascimento.setFont(new Font("Arial", Font.BOLD, 11));
		lblFechaNascimento.setBounds(22, 136, 154, 21);
		contentPanel.add(lblFechaNascimento);
		
		nac = new JDateChooser();
		nac.setDate(Obeneficiario.getFecha_nac());
		nac.getCalendarButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				profesion.requestFocus();
			}
		});
		nac.setBounds(22, 160, 154, 20);
		contentPanel.add(nac);
		
		JLabel lblProff = new JLabel("PROFISSAO:");
		lblProff.setFont(new Font("Arial", Font.BOLD, 11));
		lblProff.setBounds(194, 136, 154, 21);
		contentPanel.add(lblProff);
		
		profesion = new JTextField();
		profesion.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				convertiraMayusculasEnJtextfield(profesion);
			}
		});
		profesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pais.requestFocus();
			}
		});
		profesion.setText((String) null);
		profesion.setColumns(10);
		profesion.setText(Obeneficiario.getProfesion());
		profesion.setBounds(194, 160, 154, 21);
		contentPanel.add(profesion);
		
		JLabel lblPaisDeNacimiento = new JLabel("PAIS DE NACIMIENTO:");
		lblPaisDeNacimiento.setFont(new Font("Arial", Font.BOLD, 11));
		lblPaisDeNacimiento.setBounds(367, 136, 154, 21);
		contentPanel.add(lblPaisDeNacimiento);
		
		
		pais = new JComboBox();
		pais.setFont(new Font("Arial", Font.PLAIN, 11));
		pais.setEditable(true);
		pais.setBounds(367, 160, 154, 20);
		pais.setModel(new DefaultComboBoxModel(new String[] {"AFGANIST\u00C1N ", "AKROTIRI ", "ALBANIA ", "ALEMANIA ", "ANDORRA ", "ANGOLA ", "ANGUILA ", "ANT\u00C1RTIDA ", "ANTIGUA Y BARBUDA ", "ANTILLAS NEERLANDESAS ", "ARABIA SAUD\u00CD ", "ARCTIC OCEAN ", "ARGELIA ", "ARGENTINA ", "ARMENIA ", "ARUBA ", "ASHMORE ANDCARTIER ISLANDS ", "ATLANTIC OCEAN ", "AUSTRALIA ", "AUSTRIA ", "AZERBAIY\u00C1N ", "BAHAMAS ", "BAHR\u00C1IN ", "BANGLADESH ", "BARBADOS ", "B\u00C9LGICA ", "BELICE ", "BEN\u00CDN ", "BERMUDAS ", "BIELORRUSIA ", "BIRMANIA MYANMAR ", "BOLIVIA ", "BOSNIA Y HERCEGOVINA ", "BOTSUANA ", "BRASIL ", "BRUN\u00C9I ", "BULGARIA ", "BURKINA FASO ", "BURUNDI ", "BUT\u00C1N ", "CABO VERDE ", "CAMBOYA ", "CAMER\u00DAN ", "CANAD\u00C1 ", "CHAD ", "CHILE ", "CHINA ", "CHIPRE ", "CLIPPERTON ISLAND ", "COLOMBIA ", "COMORAS ", "CONGO ", "CORAL SEA ISLANDS ", "COREA DEL NORTE ", "COREA DEL SUR ", "COSTA DE MARFIL ", "COSTA RICA ", "CROACIA ", "CUBA ", "DHEKELIA ", "DINAMARCA ", "DOMINICA ", "ECUADOR ", "EGIPTO ", "EL SALVADOR ", "EL VATICANO ", "EMIRATOS \u00C1RABES UNIDOS ", "ERITREA ", "ESLOVAQUIA ", "ESLOVENIA ", "ESPA\u00D1A ", "ESTADOS UNIDOS ", "ESTONIA ", "ETIOP\u00CDA ", "FILIPINAS ", "FINLANDIA ", "FIYI ", "FRANCIA ", "GAB\u00D3N ", "GAMBIA ", "GAZA STRIP ", "GEORGIA ", "GHANA ", "GIBRALTAR ", "GRANADA ", "GRECIA ", "GROENLANDIA ", "GUAM ", "GUATEMALA ", "GUERNSEY ", "GUINEA ", "GUINEA ECUATORIAL ", "GUINEA-BISSAU ", "GUYANA ", "HAIT\u00CD ", "HONDURAS ", "HONG KONG ", "HUNGR\u00CDA ", "INDIA ", "INDIAN OCEAN ", "INDONESIA ", "IR\u00C1N ", "IRAQ ", "IRLANDA ", "ISLA BOUVET ", "ISLA CHRISTMAS ", "ISLA NORFOLK ", "ISLANDIA ", "ISLAS CAIM\u00C1N ", "ISLAS COCOS ", "ISLAS COOK ", "ISLAS FEROE ", "ISLAS GEORGIA DEL SUR Y SANDWICH DEL SUR ", "ISLAS HEARD Y MCDONALD ", "ISLAS MALVINAS ", "ISLAS MARIANAS DEL NORTE ", "ISLASMARSHALL ", "ISLAS PITCAIRN ", "ISLAS SALOM\u00D3N ", "ISLAS TURCAS Y CAICOS ", "ISLAS V\u00CDRGENES AMERICANAS ", "ISLAS V\u00CDRGENES BRIT\u00C1NICAS ", "ISRAEL ", "ITALIA ", "JAMAICA ", "JAN MAYEN ", "JAP\u00D3N ", "JERSEY ", "JORDANIA ", "KAZAJIST\u00C1N ", "KENIA ", "KIRGUIZIST\u00C1N ", "KIRIBATI ", "KUWAIT ", "LAOS ", "LESOTO ", "LETONIA ", "L\u00CDBANO ", "LIBERIA ", "LIBIA ", "LIECHTENSTEIN ", "LITUANIA ", "LUXEMBURGO ", "MACAO ", "MACEDONIA ", "MADAGASCAR ", "MALASIA ", "MALAUI ", "MALDIVAS ", "MAL\u00CD ", "MALTA ", "MAN, ISLE OF ", "MARRUECOS ", "MAURICIO ", "MAURITANIA ", "MAYOTTE ", "M\u00C9XICO ", "MICRONESIA ", "MOLDAVIA ", "M\u00D3NACO ", "MONGOLIA ", "MONTSERRAT ", "MOZAMBIQUE ", "NAMIBIA ", "NAURU ", "NAVASSA ISLAND ", "NEPAL ", "NICARAGUA ", "N\u00CDGER ", "NIGERIA ", "NIUE ", "NORUEGA ", "NUEVA CALEDONIA ", "NUEVA ZELANDA ", "OM\u00C1N ", "PACIFIC OCEAN ", "PA\u00CDSES BAJOS ", "PAKIST\u00C1N ", "PALAOS ", "PANAM\u00C1 ", "PAP\u00DAA-NUEVA GUINEA ", "PARACEL ISLANDS ", "PARAGUAY ", "PER\u00DA ", "POLINESIA FRANCESA ", "POLONIA ", "PORTUGAL ", "PUERTO RICO ", "QATAR ", "REINO UNIDO ", "REP\u00DABLICA CENTROAFRICANA ", "REP\u00DABLICA CHECA ", "REP\u00DABLICA DEMOCR\u00C1TICA DEL CONGO ", "REP\u00DABLICA DOMINICANA ", "RUANDA ", "RUMANIA ", "RUSIA ", "S\u00C1HARA OCCIDENTAL ", "SAMOA ", "SAMOA AMERICANA ", "SAN CRIST\u00D3BAL Y NIEVES ", "SAN MARINO ", "SAN PEDRO Y MIQUEL\u00D3N ", "SAN VICENTE Y LAS GRANADINAS ", "SANTA HELENA ", "SANTA LUC\u00CDA ", "SANTO TOM\u00C9 Y PR\u00CDNCIPE ", "SENEGAL ", "SEYCHELLES ", "SIERRA LEONA ", "SINGAPUR ", "SIRIA ", "SOMALIA ", "SOUTHERN OCEAN ", "SPRATLY ISLANDS ", "SRI LANKA ", "SUAZILANDIA ", "SUD\u00C1FRICA ", "SUD\u00C1N ", "SUECIA ", "SUIZA ", "SURINAM ", "SVALBARD Y JAN MAYEN ", "TAILANDIA ", "TAIW\u00C1N ", "TANZANIA ", "TAYIKIST\u00C1N ", "TERRITORIOBRIT\u00C1NICODEL OC\u00C9ANO INDICO ", "TERRITORIOS AUSTRALES FRANCESES ", "TIMOR ORIENTAL ", "TOGO ", "TOKELAU ", "TONGA ", "TRINIDAD Y TOBAGO ", "T\u00DANEZ ", "TURKMENIST\u00C1N ", "TURQU\u00CDA ", "TUVALU ", "UCRANIA ", "UGANDA ", "UNI\u00D3N EUROPEA ", "URUGUAY ", "UZBEKIST\u00C1N ", "VANUATU ", "VENEZUELA ", "VIETNAM ", "WAKE ISLAND ", "WALLIS Y FUTUNA ", "WEST BANK ", "WORLD ", "YEMEN ", "YIBUTI ", "ZAMBIA ", "ZIMBABUE"}));
		AutoCompleteDecorator.decorate(pais);
		contentPanel.add(pais);
		if(!beneficiario.equals("0")){
			if(Obeneficiario.getPais().equals("")){
				pais.setSelectedIndex(-1);
			}else{
				pais.setSelectedItem(Obeneficiario.getPais());
			}
		}else{
			pais.setSelectedIndex(-1);
		}
			
		
		JLabel lblNCelular = new JLabel("N. CELULAR:");
		lblNCelular.setFont(new Font("Arial", Font.BOLD, 11));
		lblNCelular.setBounds(536, 136, 154, 21);
		contentPanel.add(lblNCelular);
		
		celular = new JTextField();
		celular.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || celular.getText().length() == 20 ){
		            evt.consume();
		        }
			}
		});
		celular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				residencia.requestFocus();
			}
		});
		celular.setText((String) null);
		celular.setColumns(10);
		celular.setText(Obeneficiario.getCelular());
		celular.setBounds(536, 160, 154, 21);
		contentPanel.add(celular);
		
		JLabel lblNTelefonoResidencial = new JLabel("N. TELEFONO RES:");
		lblNTelefonoResidencial.setFont(new Font("Arial", Font.BOLD, 11));
		lblNTelefonoResidencial.setBounds(22, 192, 99, 21);
		contentPanel.add(lblNTelefonoResidencial);
		
		residencia = new JTextField();
		residencia.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || residencia.getText().length() == 20 ){
		            evt.consume();
		        }
			}
		});
		residencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				email.requestFocus();
			}
		});
		residencia.setText((String) null);
		residencia.setColumns(10);
		residencia.setText(Obeneficiario.getTelefono());
		residencia.setBounds(22, 216, 154, 21);
		contentPanel.add(residencia);
		
		JLabel lblCorreoElectronico = new JLabel("CORREO ELECTRONICO:");
		lblCorreoElectronico.setFont(new Font("Arial", Font.BOLD, 11));
		lblCorreoElectronico.setBounds(196, 192, 173, 21);
		contentPanel.add(lblCorreoElectronico);
		
		email = new JTextField();
		email.setText((String) null);
		email.setColumns(10);
		email.setText(Obeneficiario.getCorreo());
		email.setBounds(194, 216, 496, 21);
		contentPanel.add(email);
		
		JLabel label = new JLabel("(*)");
		label.setForeground(Color.RED);
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(288, 24, 12, 21);
		contentPanel.add(label);
		
		JLabel label_1 = new JLabel("(*)");
		label_1.setForeground(Color.RED);
		label_1.setFont(new Font("Arial", Font.BOLD, 11));
		label_1.setBounds(124, 80, 12, 21);
		contentPanel.add(label_1);
		
		JLabel label_2 = new JLabel("(*)");
		label_2.setForeground(Color.RED);
		label_2.setFont(new Font("Arial", Font.BOLD, 11));
		label_2.setBounds(325, 79, 12, 21);
		contentPanel.add(label_2);
		
		JLabel label_3 = new JLabel("(*)");
		label_3.setForeground(Color.RED);
		label_3.setFont(new Font("Arial", Font.BOLD, 11));
		label_3.setBounds(407, 80, 12, 21);
		contentPanel.add(label_3);
		
		JLabel label_4 = new JLabel("(*)");
		label_4.setForeground(Color.RED);
		label_4.setFont(new Font("Arial", Font.BOLD, 11));
		label_4.setBounds(621, 79, 12, 21);
		contentPanel.add(label_4);
		
		JLabel label_8 = new JLabel("(*)");
		label_8.setForeground(Color.RED);
		label_8.setFont(new Font("Arial", Font.BOLD, 11));
		label_8.setBounds(607, 136, 12, 21);
		contentPanel.add(label_8);
		
		JLabel lblLosCamposMarcados = new JLabel("OS CAMPOS MARCADOS COM       SAO OBRIGATORIOS");
		lblLosCamposMarcados.setForeground(Color.GRAY);
		lblLosCamposMarcados.setFont(new Font("Arial", Font.ITALIC, 11));
		lblLosCamposMarcados.setBounds(367, 48, 305, 21);
		contentPanel.add(lblLosCamposMarcados);
		
		JLabel label_18 = new JLabel("(*)");
		label_18.setForeground(Color.RED);
		label_18.setFont(new Font("Arial", Font.BOLD, 11));
		label_18.setBounds(538, 48, 12, 21);
		contentPanel.add(label_18);
		JButton volver = new JButton("Sair");
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				volver.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				volver.setBackground(tema.getPrincipal());
			}
		});
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				/*if(listacuentas.getRowCount() == 0 ){
					Icon icono = new ImageIcon(RegistroBeneficiarios.class.getResource("/img/error.png"));
					JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
				}else{
					dispose();
				}*/
				dispose();
			}
		});
		volver.setIcon(new ImageIcon(RegistroBeneficiarios.class.getResource("/img/back.png")));
		volver.setForeground(Color.WHITE);
		volver.setBorderPainted(false);
		volver.setBackground(tema.getPrincipal());
		volver.setBounds(413, 412, 120, 36);
		contentPanel.add(volver);
		
		
		JButton accion = new JButton("");
		accion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				accion.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				accion.setBackground(tema.getPrincipal());
			}
		});
		accion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Magician mago = new Magician();
				if( tipo.getSelectedItem().toString().equals("") || nombre.getText().equals("") || apellido.getText().equals("") || doc.getText().equals("") || cpf.getText().equals("") || tipo.getSelectedItem().toString().equals("") || celular.getText().equals("") || listacuentas.getRowCount() == 0 ){
					Icon icono = new ImageIcon(RegistroBeneficiarios.class.getResource("/img/error.png"));
					JOptionPane.showMessageDialog(null, "A campos em branco","Error" , JOptionPane.ERROR_MESSAGE, icono);
				}else{
					if(mago.isEmail(email)){
						boolean status = true;
						String pais_nac = "";
						if(pais.getSelectedIndex() != -1){
							pais_nac = pais.getSelectedItem().toString();
						}
						String cod = null;
						try {
							cod = mago.RetornaCodigo(Obeneficiario.RetornaSerie());
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						Beneficiario Obeneficiario = new Beneficiario(cod, tipo.getSelectedItem().toString(), doc.getText(), cpf.getText(), nombre.getText(), apellido.getText(), nac.getDate(), profesion.getText(), pais_nac, celular.getText(), residencia.getText(), email.getText());
				    	Obeneficiario.setConn(conn);
						if (beneficiario.equals("0")){
				    		try {
				    			Obeneficiario.getConn().setBeginTrans(false);
				    			status = Obeneficiario.Registrar();
				    			Obeneficiario.RegistrarCuentasBancarias(listacuentas, Obeneficiario.RetornaSerie());
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				    	}else{
				    		try {
								Obeneficiario.Editar(beneficiario);
				    		} catch (SQLException e) {
								Obeneficiario.getConn().setRollbackTrans();
				    			e.printStackTrace();
				    		}
					   	}
						if (status == true){
							codigo.setText("");
							tipo.setSelectedItem("");
		   					nombre.setText("");
		   					apellido.setText("");
		   					doc.setText("");
		   					cpf.setText("");
		   					tipo.setSelectedItem("");
		   					nac.setDate(null);
		   					profesion.setText("");
		   					pais.setSelectedItem("");
		   					celular.setText("");
		   					residencia.setText("");
		   					email.setText("");
							dispose();
						} 	   
					}else{
						Icon icono = new ImageIcon(RegistroBeneficiarios.class.getResource("/img/error.png"));
				    	JOptionPane.showMessageDialog(null, "O formato do email esta incorreto, Exemplo: ola@servidor.com.br","Aviso!" , JOptionPane.ERROR_MESSAGE,icono); 
					}
				}
			}
		});
		accion.setForeground(Color.WHITE);
		accion.setBorderPainted(false);
		accion.setBackground(tema.getPrincipal());
		accion.setBounds(180, 412, 120, 36);
		contentPanel.add(accion);
		
		
		
		if(beneficiario.equals("0")){
			String campos[] = {"ID BANCO","PAIS","BANCO","N� DE CUENTA","TIPO"};
			int ancho[] = {30,30,50,190,40};
			int editable[] = null;
			Class[] tipos = new Class[] {java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class,java.lang.String.class};
			listacuentas = new Tables(campos, editable, ancho,tipos);
		}else{
			String campos[] = {"ID","PAIS","BANCO","N� DE CUENTA","TIPO"};
			int ancho[] = {30,30,50,190,40};
			int editable[] = null;
			listacuentas = new Tables(campos, editable, ancho, null);
			listacuentas.Listar(Obeneficiario.ListarCuentas(Obeneficiario.RetornaID(beneficiario)+""));
		}
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 261, 538, 130);
		scrollPane.setViewportView(listacuentas);
		contentPanel.add(scrollPane);
		
		
		JButton btnAgregar = new JButton("Adicionar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegistroBeneficiarios modal = null;
				AgregarCuenta cuentas = null;
				try {
					cuentas = new AgregarCuenta(beneficiario,"0", conn);
					cuentas.setLocationRelativeTo(modal);
					cuentas.setVisible(true);
					if(beneficiario.equals("0")){
						if(cuentas.getFila() != null){
							Object[] vector = new Object[5];
							vector[0] = cuentas.getCodigo();
							vector[1] = cuentas.getFila()[0];
							vector[2] = cuentas.getFila()[1];
							vector[3] = cuentas.getFila()[2];
							vector[4] = cuentas.getFila()[3];
							listacuentas.addRow(vector);
						}
					}else{
						listacuentas.setClearGrid();
						ResultSet rs = Obeneficiario.ListarCuentas(Obeneficiario.RetornaID(beneficiario)+"");
						listacuentas.Refresh(rs);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnAgregar.setIcon(new ImageIcon(RegistroBeneficiarios.class.getResource("/img/add.png")));
		btnAgregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnAgregar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnAgregar.setBackground(tema.getPrincipal());
			}
		});
		btnAgregar.setForeground(Color.WHITE);
		btnAgregar.setBorderPainted(false);
		btnAgregar.setBackground(tema.getPrincipal());
		btnAgregar.setBounds(570, 261, 120, 36);
		contentPanel.add(btnAgregar);
		
		JButton btnBorrar = new JButton("");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(beneficiario.equals("0")){
					if(listacuentas.getSelectedRow() != -1){
						listacuentas.deleteRow(listacuentas.getSelectedRow());
					}else{
						Icon icono = new ImageIcon(RegistroBeneficiarios.class.getResource("/img/precaucion.png"));
						JOptionPane.showMessageDialog(null, "Selecione uma conta para Excluir","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
					}	
				}else{
					if(listacuentas.getSelectedRow() != -1){
						RegistroBeneficiarios modal = null;
						AgregarCuenta cuentas = null;
						String idcuenta = listacuentas.getValueAt(listacuentas.getSelectedRow(), 0)+ "";
							try {
								cuentas = new AgregarCuenta(beneficiario,idcuenta,conn);
								cuentas.setLocationRelativeTo(modal);
								cuentas.setVisible(true);
								listacuentas.setClearGrid();
								ResultSet rs = Obeneficiario.ListarCuentas(Obeneficiario.RetornaID(beneficiario)+"");
								listacuentas.Refresh(rs);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}						
					}else{
						Icon icono = new ImageIcon(RegistroBeneficiarios.class.getResource("/img/precaucion.png"));
						JOptionPane.showMessageDialog(null, "Selecione uma conta para Editar","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
					}	
				}		
			}
		});
		btnBorrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnBorrar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnBorrar.setBackground(tema.getPrincipal());
			}
		});
		if (beneficiario.equals("0")){
			btnBorrar.setIcon(new ImageIcon(RegistroBeneficiarios.class.getResource("/img/del.png")));
			btnBorrar.setText("Borrar");
		}else{
			btnBorrar.setIcon(new ImageIcon(RegistroBeneficiarios.class.getResource("/img/edit.png")));
			btnBorrar.setText("Editar");
		}
		btnBorrar.setForeground(Color.WHITE);
		btnBorrar.setBorderPainted(false);
		btnBorrar.setBackground(new Color(52, 165, 197));
		btnBorrar.setBounds(570, 308, 120, 36);
		contentPanel.add(btnBorrar);
		
		JButton btnLimpiar = new JButton("");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(beneficiario.equals("0")){
					listacuentas.setClearGrid();
				}else{
					if(listacuentas.getSelectedRow() != -1){
						try {
							String idcuenta = listacuentas.getValueAt(listacuentas.getSelectedRow(), 0)+ "";
							Obeneficiario.CambiarEstadoCuenta(idcuenta);
							listacuentas.setClearGrid();
							ResultSet rs = Obeneficiario.ListarCuentas(Obeneficiario.RetornaID(beneficiario)+"");
							listacuentas.Refresh(rs);
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}					
					}else{
						Icon icono = new ImageIcon(RegistroBeneficiarios.class.getResource("/img/precaucion.png"));
						JOptionPane.showMessageDialog(null, "Selecione uma conta para Excluir","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
					}	
				}
			}
		});
		btnLimpiar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnLimpiar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnLimpiar.setBackground(tema.getPrincipal());
			}
		});
		if (beneficiario.equals("0")){
			btnLimpiar.setIcon(new ImageIcon(RegistroBeneficiarios.class.getResource("/img/trash.png")));
			btnLimpiar.setText("Limpiar");
		}else{
			btnLimpiar.setIcon(new ImageIcon(RegistroBeneficiarios.class.getResource("/img/del.png")));
			btnLimpiar.setText("Borrar");
		}
		
		btnLimpiar.setForeground(Color.WHITE);
		btnLimpiar.setBorderPainted(false);
		btnLimpiar.setBackground(new Color(52, 165, 197));
		btnLimpiar.setBounds(570, 355, 120, 36);
		contentPanel.add(btnLimpiar);
		
		if (beneficiario.equals("0")){
			accion.setText("Registrar");
			accion.setIcon(new ImageIcon(RegistroBeneficiarios.class.getResource("/img/add.png")));
		}else{
			accion.setText("Guardar");
			accion.setIcon(new ImageIcon(RegistroBeneficiarios.class.getResource("/img/save.png")));
		}
		
		if (beneficiario.equals("0")){
			titulo = "REGISTRO DE BENEFICIARIOS";
		}else{
			titulo = "MODIFICAR BENEFICIARIO";
		}
	}
}
