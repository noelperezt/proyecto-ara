package views;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import models.Usuario;
import utility.Conexion;
import utility.Estilo;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.sql.SQLException;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPasswordField;

public class RegistroUsuarios extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField nombre;
	private JTextField usuario;
	private JPasswordField clave;
	private JPasswordField confirmacion;
	
	public RegistroUsuarios(Conexion conn) throws SQLException {
		setModal(true);
		
		setTitle("Registro de Usuarios");
		setResizable(false);
		setBounds(100, 100, 416, 249);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
	
		Estilo tema = new Estilo();
		
		JButton volver = new JButton("Sair");
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				volver.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				volver.setBackground(tema.getPrincipal());
			}
		});
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		volver.setIcon(new ImageIcon(RegistroUsuarios.class.getResource("/img/back.png")));
		volver.setForeground(Color.WHITE);
		volver.setBorderPainted(false);
		volver.setBackground(tema.getPrincipal());
		volver.setBounds(230, 161, 120, 36);
		contentPanel.add(volver);
		
		
		JButton accion = new JButton("");
		accion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				accion.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				accion.setBackground(tema.getPrincipal());
			}
		});
		accion.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				if(nombre.getText().equals("") || usuario.getText().equals("") || clave.getText().equals("") || confirmacion.getText().equals("")){
					Icon icono = new ImageIcon(RegistroUsuarios.class.getResource("/img/error.png"));
					JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE,icono);
				}else{
					Usuario Ouser = new Usuario(nombre.getText(), usuario.getText(), clave.getText());
					Ouser.setConn(conn);
					if(Ouser.ClavesIguales(clave.getText(), confirmacion.getText())){
						try {
							if(Ouser.Registrar()){
								nombre.setText("");
								usuario.setText("");
								clave.setText("");
								confirmacion.setText("");
								dispose();
							}
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}else{
						Icon icono = new ImageIcon(RegistroUsuarios.class.getResource("/img/error.png"));
						JOptionPane.showMessageDialog(null, "Las Contraseņas no Coinciden","Error" , JOptionPane.ERROR_MESSAGE, icono);
					}
				}
			}
		});
		
		accion.setText("Novo");
		accion.setIcon(new ImageIcon(RegistroUsuarios.class.getResource("/img/add.png")));
		accion.setForeground(Color.WHITE);
		accion.setBorderPainted(false);
		accion.setBackground(tema.getPrincipal());
		accion.setBounds(59, 161, 120, 36);
		contentPanel.add(accion);
		
		JLabel lblNome = new JLabel("NOME:");
		lblNome.setFont(new Font("Arial", Font.BOLD, 11));
		lblNome.setBounds(46, 26, 55, 18);
		contentPanel.add(lblNome);
		
		nombre = new JTextField();
		nombre.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				nombre.nextFocus();
			}
		});
		nombre.setColumns(10);
		nombre.setBounds(115, 26, 268, 21);
		contentPanel.add(nombre);
		
		JLabel label_1 = new JLabel("USUARIO:");
		label_1.setFont(new Font("Arial", Font.BOLD, 11));
		label_1.setBounds(46, 58, 55, 18);
		contentPanel.add(label_1);
		
		usuario = new JTextField();
		usuario.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				usuario.nextFocus();
			}
		});
		usuario.setColumns(10);
		usuario.setBounds(115, 58, 268, 21);
		contentPanel.add(usuario);
		
		JLabel lblSenha = new JLabel("SENHA:");
		lblSenha.setFont(new Font("Arial", Font.BOLD, 11));
		lblSenha.setBounds(46, 87, 49, 21);
		contentPanel.add(lblSenha);
		
		clave = new JPasswordField();
		clave.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				clave.nextFocus();
			}
		});
		clave.setBounds(115, 90, 268, 21);
		contentPanel.add(clave);
		
		JLabel lblConfirmar = new JLabel("CONFIRMAR:");
		lblConfirmar.setFont(new Font("Arial", Font.BOLD, 11));
		lblConfirmar.setBounds(46, 119, 67, 21);
		contentPanel.add(lblConfirmar);
		
		confirmacion = new JPasswordField();
		confirmacion.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				confirmacion.nextFocus();
			}
		});
		confirmacion.setBounds(115, 122, 268, 21);
		contentPanel.add(confirmacion);
	}
}
