package views;

import java.awt.BorderLayout;
import java.sql.SQLException;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import utility.Estilo;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

@SuppressWarnings("serial")
public class CambiarEstado extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private String estado;
	private String observacion;
	@SuppressWarnings("rawtypes")
	private JComboBox tipo;
	private JTextArea obs; 
	/**
	 * Create the dialog.
	 * @throws SQLException 
	 */
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public CambiarEstado() throws SQLException {
		setModal(true);
		setTitle("Cambiar Estado");
		setResizable(false);
		setBounds(100, 100, 405, 292);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				setEstado("REVISION");
				setObservacion("");
				setVisible(false);
			}
		});
		
		
		Estilo tema = new Estilo();
		
		JLabel lblEstado = new JLabel("ESTADO:");
		lblEstado.setFont(new Font("Arial", Font.BOLD, 11));
		lblEstado.setBounds(35, 22, 54, 21);
		contentPanel.add(lblEstado);
		
		tipo = new JComboBox();
		tipo.setModel(new DefaultComboBoxModel(new String[] {"EFETUADA", "REJEITADA", "REEMBOLSO"}));
		tipo.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				tipo.nextFocus();
			}
		});
		tipo.setFont(new Font("Arial", Font.PLAIN, 11));
		tipo.setBounds(35, 46, 322, 20);
		contentPanel.add(tipo);
		
		JButton btnAgregar = new JButton("Guardar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setEstado(tipo.getSelectedItem().toString());
				setObservacion(obs.getText());
				setVisible(false);
			}
		});
		btnAgregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnAgregar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnAgregar.setBackground(tema.getPrincipal());
			}
		});
		btnAgregar.setIcon(new ImageIcon(CambiarEstado.class.getResource("/img/save.png")));
		btnAgregar.setForeground(Color.WHITE);
		btnAgregar.setBorderPainted(false);
		btnAgregar.setBackground(tema.getPrincipal());
		btnAgregar.setBounds(51, 211, 120, 36);
		contentPanel.add(btnAgregar);
		
		JButton button_1 = new JButton("Volver");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setEstado("REVISION");
				setObservacion("");
				setVisible(false);
			}
		});
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getPrincipal());
			}
		});
		button_1.setIcon(new ImageIcon(CambiarEstado.class.getResource("/img/back.png")));
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(tema.getPrincipal());
		button_1.setBounds(227, 211, 120, 36);
		contentPanel.add(button_1);
		
		JLabel label_4 = new JLabel("LOS CAMPOS MARCADOS CON       SON OBLIGATORIOS");
		label_4.setForeground(Color.GRAY);
		label_4.setFont(new Font("Arial", Font.ITALIC, 11));
		label_4.setBounds(55, 176, 302, 21);
		contentPanel.add(label_4);
		
		JLabel label_8 = new JLabel("(*)");
		label_8.setForeground(Color.RED);
		label_8.setFont(new Font("Arial", Font.BOLD, 11));
		label_8.setBounds(87, 22, 12, 21);
		contentPanel.add(label_8);
		
		JLabel label_9 = new JLabel("(*)");
		label_9.setForeground(Color.RED);
		label_9.setFont(new Font("Arial", Font.BOLD, 11));
		label_9.setBounds(227, 179, 12, 21);
		contentPanel.add(label_9);
		
		JLabel lblObservacion = new JLabel("OBSERVACION:");
		lblObservacion.setFont(new Font("Arial", Font.BOLD, 11));
		lblObservacion.setBounds(35, 77, 89, 21);
		contentPanel.add(lblObservacion);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(35, 100, 322, 65);
		contentPanel.add(scrollPane);
		
		obs  = new JTextArea();
		obs.setRows(3);
		obs.setColumns(22);
		scrollPane.setViewportView(obs);
		
	}
}
