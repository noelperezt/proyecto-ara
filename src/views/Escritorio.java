package views;

import java.awt.Toolkit;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import utility.Calculadora;
import utility.Conexion;
import utility.DesktopBackground;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import models.Usuario;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;


public class Escritorio {

	public JFrame escritorio;
	private DesktopBackground desk;
	private Usuario logueado;
	private Conexion conn;
	
	/* Verifica si es instancia de algun componente que ya este en el escritorio 
	 * @param inter JInternalFrame que se pretende ejecutar
	 */
	public void controlaInstancia(JInternalFrame window){
		boolean mostrar = true;
		String nombre = window.getTitle();
		for (int i = 0; i < desk.getComponentCount(); i++){    
		    if(window.getClass().isInstance( desk.getComponent(i))){
		    	Icon icono = new ImageIcon(Escritorio.class.getResource("/img/informacion.png"));
				JOptionPane.showMessageDialog(null,"La ventana "+ nombre +" que intenta abrir ya est� abierta, cierre la ventana actual e intente nuevamente","Informaci�n",JOptionPane.INFORMATION_MESSAGE,icono);
		        System.out.println("esta instancia, no se debe mostrar");
		        window.toFront();
		        desk.moveToFront(window);
		        mostrar = false;
		    }else{
		        System.out.println("no lo es, puede mostrarse");
		    }
		}
		
		//***** Centra  el JInternalFrame en el JDesktopPane ******
		if(mostrar){
			/*java.awt.Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
			java.awt.Dimension ventana = window.getSize();
			
			int x = (int) ((pantalla.getWidth() - ventana.getWidth())/2);
			int y = (int) ((pantalla.getHeight() - ventana.getHeight())/2);*/
			
			int x = (desk.getWidth()/2) - window.getWidth()/2;
			int y = (desk.getHeight()/2) - window.getHeight()/2;
			
			/*if(x < 0){
				x = x * -1;
			}
				
			if(y < 0){
				y = y * -1;
			}*/
			
			
			if (window.isShowing()){
				window.setLocation(x, y);
			}else{
				desk.add(window);
				window.setLocation(x, y);
				window.show();
			}
		}
	}
	
	public void controlaInstancia(JInternalFrame window, DesktopBackground desk){
		boolean mostrar = true;
		String nombre = window.getTitle();
		for (int i = 0; i < desk.getComponentCount(); i++){    
		    if(window.getClass().isInstance( desk.getComponent(i))){
		    	Icon icono = new ImageIcon(Escritorio.class.getResource("/img/informacion.png"));
				JOptionPane.showMessageDialog(null,"La ventana "+ nombre +" que interta abrir ya est� abierta, cierre la ventana actual e intente nuevamente","Informaci�n",JOptionPane.INFORMATION_MESSAGE,icono);
		        System.out.println("esta instancia, no se debe mostrar");
		        window.toFront();
		        desk.moveToFront(window);
		        mostrar = false;
		    }else{
		        System.out.println("no lo es, puede mostrarse");
		    }
		}
		
		//***** Centra  el JInternalFrame en el JDesktopPane ******
		if(mostrar){
			int x = (desk.getWidth()/2) - window.getWidth()/2;
			int y = (desk.getHeight()/2) - window.getHeight()/2;
			
			/*java.awt.Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
			java.awt.Dimension ventana = window.getSize();
			
			int x = (int) ((pantalla.getWidth()/2) - ventana.getWidth()/2);
			int y = (int) ((pantalla.getHeight()/2) - ventana.getHeight()/2);*/
			
			
			if(x < 0){
				x = x * -1;
			}
				
			if(y < 0){
				y = y * -1;
			}
			
			if (window.isShowing()){
				window.setLocation(x, y);
			}else{
				desk.add(window);
				window.setLocation(x, y);
				window.show();
			}
		}
	}
	
	/* Carga la imagen de fondo del escritorio
	 * @param Jdesk JDesktopPane en el cual se va a insertar la imagen
	 * @param image imagen de fondo
	 */

	
	public void CargaInicio() throws SQLException{
		
	}
	
	//public Escritorio(){};
	
	public Escritorio(Usuario user, Conexion conn) throws SQLException {
		this.logueado = user;
		this.conn = conn;
		initialize();
	}
	
	
	/*public Escritorio() {
		// TODO Auto-generated constructor stub
	}*/

	private void initialize() throws SQLException {
		escritorio = new JFrame();
		escritorio.setResizable(true);
		escritorio.setTitle("ARA CAMBIO & TURISMO   -    SISTEMA DE CAMBIO 2018");
		escritorio.setIconImage(Toolkit.getDefaultToolkit().getImage(Escritorio.class.getResource("/img/favicon.png")));
		escritorio.setBounds(100, 100, 799, 545);
		escritorio.setExtendedState(JFrame.MAXIMIZED_BOTH);
		escritorio.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		escritorio.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				Icon icono = new ImageIcon(Escritorio.class.getResource("/img/interrogacion.png"));
				if(JOptionPane.showConfirmDialog(null, "Deseja realmenet sair do sistema?","Aviso!",1,0,icono)==0){
		            System.exit(0);
		        }
			}
		});
		
		desk = new DesktopBackground();
		desk.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				try {
					CargaInicio();
					desk.setFocusable(false);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		desk.setFocusable(true);
		escritorio.getContentPane().add(desk, BorderLayout.CENTER);
		
		
		
		JMenuBar menu = new JMenuBar();
		escritorio.getContentPane().add(menu, BorderLayout.NORTH);
		
		JMenu mnProveedores = new JMenu("CLIENTE");
		mnProveedores.setIcon(new ImageIcon(Escritorio.class.getResource("/img/ico_grupo.png")));
		mnProveedores.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				AdministrarClientes ventana;
				try {
					ventana = new AdministrarClientes(conn);
					controlaInstancia(ventana);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (PropertyVetoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		JMenu mnSolicitud = new JMenu("ENVIO DE MOEDA");
		mnSolicitud.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				RegistroSolicitud modal;
				try {
					modal = new RegistroSolicitud("0", logueado.RetornaIdUsuario(logueado.getUsuario()), conn);
					modal.setLocationRelativeTo(desk);
					modal.setVisible(true);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		mnSolicitud.setIcon(new ImageIcon(Escritorio.class.getResource("/img/comprobante.png")));
		menu.add(mnSolicitud);
		menu.add(mnProveedores);
		
		JMenu mnTrabajadores = new JMenu("BENEFICIARIO");
		mnTrabajadores.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				AdministrarBeneficiarios ventana;
				try {
					ventana = new AdministrarBeneficiarios(conn);
					controlaInstancia(ventana);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (PropertyVetoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		mnTrabajadores.setIcon(new ImageIcon(Escritorio.class.getResource("/img/man.png")));
		menu.add(mnTrabajadores);
		
		JMenu mnReportes = new JMenu("RELAT\u00D3RIOS");
		mnReportes.setIcon(new ImageIcon(Escritorio.class.getResource("/img/report.png")));
		menu.add(mnReportes);
		
		JMenu mnNewMenu_1 = new JMenu("FERRAMENTAS");
		mnNewMenu_1.setIcon(new ImageIcon(Escritorio.class.getResource("/img/tool.png")));
		menu.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Calculadora");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Calculadora calculator = new Calculadora();
				calculator.Ejecutar();
			}
		});
		mntmNewMenuItem_3.setIcon(new ImageIcon(Escritorio.class.getResource("/img/calculator.png")));
		mnNewMenu_1.add(mntmNewMenuItem_3);
		
		JMenu mnAdministracin = new JMenu("ADMINISTRA\u00C7\u00C3O");
		mnAdministracin.setIcon(new ImageIcon(Escritorio.class.getResource("/img/ico_config.png")));
		menu.add(mnAdministracin);
		
		JMenuItem mntmUsuarios = new JMenuItem("Cadastro de Usuarios");
		mntmUsuarios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdministrarUsuarios ventana;
				try {
					ventana = new AdministrarUsuarios(conn);
					controlaInstancia(ventana);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		mntmUsuarios.setIcon(new ImageIcon(Escritorio.class.getResource("/img/user.png")));
		mnAdministracin.add(mntmUsuarios);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Alterar Senha");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CambioClave ventana;
				ventana = new CambioClave(logueado, conn);
				controlaInstancia(ventana);
			}
		});
		
		JMenuItem mntmBancos = new JMenuItem("Cadastro de Bancos");
		mntmBancos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdministrarBancos ventana;
				try {
					ventana = new AdministrarBancos(conn);
					controlaInstancia(ventana);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (PropertyVetoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		mntmBancos.setIcon(new ImageIcon(Escritorio.class.getResource("/img/builder.png")));
		mnAdministracin.add(mntmBancos);
		
		JMenuItem mntmMonedas = new JMenuItem("Cadastrdo de Moedas");
		mntmMonedas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdministrarMonedas ventana;
				try {
					ventana = new AdministrarMonedas(conn);
					controlaInstancia(ventana);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (PropertyVetoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		mntmMonedas.setIcon(new ImageIcon(Escritorio.class.getResource("/img/money.png")));
		mnAdministracin.add(mntmMonedas);
		
		JMenuItem mntmSolicitudes = new JMenuItem("Solicitacoes");
		mntmSolicitudes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdministrarSolicitudes ventana;
				try {
					ventana = new AdministrarSolicitudes(conn);
					controlaInstancia(ventana);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (PropertyVetoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		mntmSolicitudes.setIcon(new ImageIcon(Escritorio.class.getResource("/img/comprobante.png")));
		mnAdministracin.add(mntmSolicitudes);
		mntmNewMenuItem_1.setIcon(new ImageIcon(Escritorio.class.getResource("/img/pass.png")));
		mnAdministracin.add(mntmNewMenuItem_1);
		
		JMenu mnNewMenu_2 = new JMenu("AJUDA");
		mnNewMenu_2.setIcon(new ImageIcon(Escritorio.class.getResource("/img/help.png")));
		menu.add(mnNewMenu_2);
		
		JMenu mnNewMenu = new JMenu("SAIR");
		mnNewMenu.setIcon(new ImageIcon(Escritorio.class.getResource("/img/ico_puerta.png")));
		menu.add(mnNewMenu);
		
		JMenuItem mntmCerrarSesin = new JMenuItem("Bloquear");
		mntmCerrarSesin.setIcon(new ImageIcon(Escritorio.class.getResource("/img/ico_bloqueado.png")));
		mntmCerrarSesin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Icon icono = new ImageIcon(Escritorio.class.getResource("/img/interrogacion.png"));
				if(JOptionPane.showConfirmDialog(null, "�Est� seguro de cerrar la sesi�n actual?","Cerrar sesi�n",1,0,icono)==0){
			        Login ini = new Login();
			        ini.frmIniciarSesin.setVisible(true);
			        escritorio.dispose();
			    }
			}
		});
		mnNewMenu.add(mntmCerrarSesin);
		
		JMenuItem mntmSalir = new JMenuItem("Sair");
		mntmSalir.setIcon(new ImageIcon(Escritorio.class.getResource("/img/ico_mal.png")));
		mntmSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Icon icono = new ImageIcon(Escritorio.class.getResource("/img/interrogacion.png"));
				if(JOptionPane.showConfirmDialog(null, "�Est� seguro de cerrar el Sistema?","Cerrar Sistema",1,0,icono)==0){
		            System.exit(0);
		        }
			}
		});
		mnNewMenu.add(mntmSalir);
		
		
		JPanel panel = new JPanel();
		escritorio.getContentPane().add(panel, BorderLayout.SOUTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{46, 0, 0, 0, 0, 0, 0, 0, 0, 111, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 584, 0};
		gbl_panel.rowHeights = new int[]{14, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblNewLabel = new JLabel(logueado.getNombre());
		lblNewLabel.setToolTipText("Sesi�n iniciada por "+logueado.getUsuario());
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setIcon(new ImageIcon(Escritorio.class.getResource("/img/user.png")));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
		
		
	}
}
