package views;

import java.awt.BorderLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import models.Banco;
import models.Beneficiario;
import utility.Conexion;
import utility.Estilo;
import utility.Objeto;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@SuppressWarnings("serial")
public class AgregarCuenta extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private Integer codigo;
	private JTextField cuenta;
	@SuppressWarnings("rawtypes")
	private JComboBox pais;
	@SuppressWarnings("rawtypes")
	private JComboBox banco;
	@SuppressWarnings("rawtypes")
	private JComboBox tipo;
	private Object[] fila;
	
	/**
	 * Create the dialog.
	 * @throws SQLException 
	 */
	
	public Integer getCodigo(){
		return codigo;
	}
	
	public void setCodigo(Integer codigo){
		this.codigo = codigo;
	}
	
	public Object[] getFila(){
		return fila;
	}
	
	public void setFila(Object[] fila){
		this.fila = fila;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public AgregarCuenta(String beneficiario, String id_cuenta, Conexion conn) throws SQLException {
		setModal(true);
		String titulo;
		
		if(id_cuenta.equals("0")){
			titulo = "Agregar Cuenta";
		}else{
			titulo = "Editar Cuenta";
		}
		setTitle(titulo);
		setResizable(false);
		setBounds(100, 100, 717, 194);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				setCodigo(0);
				setVisible(false);
			}
		});
		
		
		Estilo tema = new Estilo();
		Beneficiario Obeneficiario = new Beneficiario();
		Obeneficiario.setConn(conn);
		
		String[] vector = Obeneficiario.TraerRegistroCuenta(id_cuenta);
		
		JLabel label = new JLabel("PAIS:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(22, 24, 114, 21);
		contentPanel.add(label);
		
		pais = new JComboBox();
		pais.setEditable(true);
		pais.setModel(new DefaultComboBoxModel(new String[] {"BOLIVIA", "BRASIL", "CHILE", "COLOMBIA", "VENEZUELA"}));
		pais.setSelectedItem(vector[1]);
		pais.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				Banco Obanco = new Banco();
				Obanco.setConn(conn);
				ResultSet buscar;
				try {
					buscar = Obanco.ListarBancosPais(pais.getSelectedItem().toString());
					banco.removeAllItems();
					DefaultComboBoxModel value = new DefaultComboBoxModel();
					banco.setModel(value);
					while (buscar.next()){
						value.addElement(new Objeto(buscar.getInt("id"), buscar.getString("nombre").toUpperCase()));
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}		
			}
		});
		pais.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				pais.nextFocus();
			}
		});
		pais.setFont(new Font("Arial", Font.PLAIN, 11));
		pais.setBounds(22, 48, 120, 20);
		AutoCompleteDecorator.decorate(pais);
		contentPanel.add(pais);
		
		JLabel lblBanco = new JLabel("BANCO:");
		lblBanco.setFont(new Font("Arial", Font.BOLD, 11));
		lblBanco.setBounds(160, 24, 154, 21);
		contentPanel.add(lblBanco);
		
		banco = new JComboBox();
		banco.setEditable(true);
		Banco Obanco = new Banco();
		ResultSet buscar;
		try {
			buscar = Obanco.ListarBancosPais(vector[1]);
			banco.removeAllItems();
			DefaultComboBoxModel value = new DefaultComboBoxModel();
			banco.setModel(value);
			while (buscar.next()){
				value.addElement(new Objeto(buscar.getInt("id"), buscar.getString("nombre").toUpperCase()));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		banco.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				banco.nextFocus();
			}
		});
		banco.setSelectedItem(vector[2]);
		banco.setFont(new Font("Arial", Font.PLAIN, 11));
		banco.setBounds(160, 48, 156, 20);
		AutoCompleteDecorator.decorate(banco);
		contentPanel.add(banco);
		
		JLabel lblConta = new JLabel("CONTA:");
		lblConta.setFont(new Font("Arial", Font.BOLD, 11));
		lblConta.setBounds(326, 24, 234, 21);
		contentPanel.add(lblConta);
		
		cuenta = new JTextField();
		cuenta.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		cuenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tipo.requestFocus();
			}
		});
		cuenta.setText((String) null);
		cuenta.setText(vector[3]);
		cuenta.setColumns(10);
		cuenta.setBounds(326, 48, 234, 21);
		contentPanel.add(cuenta);
		
		JLabel lblTipoDeConta = new JLabel("TIPO DE CONTA:");
		lblTipoDeConta.setFont(new Font("Arial", Font.BOLD, 11));
		lblTipoDeConta.setBounds(570, 24, 114, 21);
		contentPanel.add(lblTipoDeConta);
		
		tipo = new JComboBox();
		tipo.setModel(new DefaultComboBoxModel(new String[] {"POUPANCA", "CORRENTE"}));
		tipo.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				tipo.nextFocus();
			}
		});
		tipo.setSelectedItem(vector[4]);
		tipo.setFont(new Font("Arial", Font.PLAIN, 11));
		tipo.setBounds(570, 48, 120, 20);
		contentPanel.add(tipo);
		
		JButton btnAgregar = new JButton("Salvar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!(pais.getSelectedItem().toString().equals("")|| banco.getSelectedItem().toString().equals("") || cuenta.getText().equals("") || tipo.getSelectedItem().toString().equals(""))){
					if(beneficiario.equals("0")){
						fila = new Object[4];
						fila[0] = pais.getSelectedItem().toString();
						fila[1] = banco.getSelectedItem().toString();
						fila[2] = cuenta.getText();
						fila[3] = tipo.getSelectedItem().toString();
						Objeto dp = (Objeto) banco.getSelectedItem();
						setCodigo(dp.getId());
						setVisible(false);
					}else{
						if(id_cuenta.equals("0")){
							try {
								Beneficiario Obeneficiario = new Beneficiario();
								Obeneficiario.setConn(conn);
								Objeto dp = (Objeto) banco.getSelectedItem();
								Obeneficiario.RegistrarCuentaBancaria(dp.getId(), tipo.getSelectedItem().toString(), cuenta.getText(), Obeneficiario.RetornaID(beneficiario)+"");
								setVisible(false);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}else{
							try {
								Beneficiario Obeneficiario = new Beneficiario();
								Obeneficiario.setConn(conn);
								Objeto dp = (Objeto) banco.getSelectedItem();
								Obeneficiario.EditarCuentaBancaria(id_cuenta,dp.getId(), tipo.getSelectedItem().toString(), cuenta.getText());
								setVisible(false);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}	
					}	
				}else{
					Icon icono = new ImageIcon(AgregarCuenta.class.getResource("/img/error.png"));
					JOptionPane.showMessageDialog(null, "Campos Vacios en el Formulario","Error" , JOptionPane.ERROR_MESSAGE, icono); 
				}
			}
		});
		btnAgregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnAgregar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnAgregar.setBackground(tema.getPrincipal());
			}
		});
		btnAgregar.setIcon(new ImageIcon(AgregarCuenta.class.getResource("/img/add.png")));
		btnAgregar.setForeground(Color.WHITE);
		btnAgregar.setBorderPainted(false);
		btnAgregar.setBackground(tema.getPrincipal());
		btnAgregar.setBounds(194, 112, 120, 36);
		contentPanel.add(btnAgregar);
		
		if (id_cuenta.equals("0")){
			btnAgregar.setText("Agregar");
			btnAgregar.setIcon(new ImageIcon(AgregarCuenta.class.getResource("/img/add.png")));
		}else{
			btnAgregar.setText("Guardar");
			btnAgregar.setIcon(new ImageIcon(AgregarCuenta.class.getResource("/img/save.png")));
		}
		
		JButton btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCodigo(0);
				setVisible(false);
			}
		});
		btnSair.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnSair.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnSair.setBackground(tema.getPrincipal());
			}
		});
		btnSair.setIcon(new ImageIcon(AgregarCuenta.class.getResource("/img/back.png")));
		btnSair.setForeground(Color.WHITE);
		btnSair.setBorderPainted(false);
		btnSair.setBackground(tema.getPrincipal());
		btnSair.setBounds(401, 112, 120, 36);
		contentPanel.add(btnSair);
		
		JLabel lblOsCamposMarcados = new JLabel("OS CAMPOS MARCADOS COM       SAO OBRIGATORIOS");
		lblOsCamposMarcados.setForeground(Color.GRAY);
		lblOsCamposMarcados.setFont(new Font("Arial", Font.ITALIC, 11));
		lblOsCamposMarcados.setBounds(388, 80, 302, 21);
		contentPanel.add(lblOsCamposMarcados);
		
		JLabel label_5 = new JLabel("(*)");
		label_5.setForeground(Color.RED);
		label_5.setFont(new Font("Arial", Font.BOLD, 11));
		label_5.setBounds(53, 22, 12, 21);
		contentPanel.add(label_5);
		
		JLabel label_6 = new JLabel("(*)");
		label_6.setForeground(Color.RED);
		label_6.setFont(new Font("Arial", Font.BOLD, 11));
		label_6.setBounds(276, 22, 12, 21);
		contentPanel.add(label_6);
		
		JLabel label_7 = new JLabel("(*)");
		label_7.setForeground(Color.RED);
		label_7.setFont(new Font("Arial", Font.BOLD, 11));
		label_7.setBounds(376, 22, 12, 21);
		contentPanel.add(label_7);
		
		JLabel label_8 = new JLabel("(*)");
		label_8.setForeground(Color.RED);
		label_8.setFont(new Font("Arial", Font.BOLD, 11));
		label_8.setBounds(662, 22, 12, 21);
		contentPanel.add(label_8);
		
		JLabel label_9 = new JLabel("(*)");
		label_9.setForeground(Color.RED);
		label_9.setFont(new Font("Arial", Font.BOLD, 11));
		label_9.setBounds(558, 80, 12, 21);
		contentPanel.add(label_9);
		
	}
}
