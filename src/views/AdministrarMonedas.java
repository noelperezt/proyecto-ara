package views;


import javax.swing.JInternalFrame;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import models.Moneda;
import utility.Conexion;
import utility.Estilo;
import utility.Tables;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@SuppressWarnings("serial")
public class AdministrarMonedas extends JInternalFrame {
	private JTextField textField;
	private Tables listamonedas;

	public AdministrarMonedas(Conexion conn) throws SQLException, PropertyVetoException {
		setIcon(true);
		setTitle("Cadastro de Moedas");
		setFrameIcon(new ImageIcon(AdministrarMonedas.class.getResource("/img/money.png")));
		setClosable(true);
		setIconifiable(true);
		setBounds(100, 100, 731, 293);
		getContentPane().setLayout(null);
		
		String campos[] = {"ID","MOEDA", "PAIS", "TAXA", "ESTADO"};
		int ancho[] = {1,200,40,10,10};
		int editable[] = null;
		Moneda Omoneda = new Moneda();
		Omoneda.setConn(conn);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(31, 62, 519, 177);
		listamonedas = new Tables(campos, editable, ancho, null);
		listamonedas.Listar(Omoneda.ListarMonedas());
		
		getContentPane().add(scrollPane);
		scrollPane.setViewportView(listamonedas);
		
		JLabel label = new JLabel("BUSCAR:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(31, 22, 56, 21);
		getContentPane().add(label);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				try {
					listamonedas.setClearGrid();
					listamonedas.Refresh(Omoneda.FiltraMonedas(textField.getText()));
				} catch (SQLException e) {
					e.printStackTrace();
				}	
			}
		});
		textField.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textField.setColumns(10);
		textField.setBounds(84, 22, 346, 21);
		getContentPane().add(textField);
		
		Estilo tema = new Estilo();
		
		JButton btnAdicionar = new JButton("Adicionar");
		btnAdicionar.setBorderPainted(false);
		btnAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					boolean accion;
					if(Omoneda.getConn().getRowsCount(Omoneda.ListarMonedas()) > 0){
						accion = true;
					}else{
						accion = false;
					}
					RegistroMonedas modal = new RegistroMonedas(0,conn);
					AdministrarMonedas ventana = new AdministrarMonedas(conn);
					modal.setLocationRelativeTo(ventana);
					modal.setVisible(true);
					listamonedas.setClearGrid();
					if (accion){
						listamonedas.Refresh(Omoneda.ListarMonedas());
					}else{
						listamonedas.Listar(Omoneda.ListarMonedas());
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (PropertyVetoException e) {
					e.printStackTrace();
				}
			}
		});
		btnAdicionar.setIcon(new ImageIcon(AdministrarMonedas.class.getResource("/img/add.png")));
		btnAdicionar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnAdicionar.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				btnAdicionar.setBackground(tema.getPrincipal());
			}
		});
		btnAdicionar.setForeground(Color.WHITE);
		btnAdicionar.setBackground(tema.getPrincipal());
		btnAdicionar.setBounds(570, 62, 123, 35);
		getContentPane().add(btnAdicionar);
		
		JButton button_1 = new JButton("Editar");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listamonedas.getSelectedRow() != -1){
					int id = Integer.parseInt(listamonedas.getValueAt(listamonedas.getSelectedRow(), 0).toString());
					try {
						RegistroMonedas modal = new RegistroMonedas(id,conn);
						AdministrarMonedas ventana = new AdministrarMonedas(conn);
						modal.setLocationRelativeTo(ventana);
						modal.setVisible(true);
						listamonedas.setClearGrid();
						listamonedas.Refresh(Omoneda.ListarMonedas());
					} catch (SQLException | PropertyVetoException e1) {
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(AdministrarMonedas.class.getResource("/img/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar una Moneda para editar","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		button_1.setIcon(new ImageIcon(AdministrarMonedas.class.getResource("/img/edit.png")));
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button_1.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				button_1.setBackground(tema.getPrincipal());
			}
		});
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(tema.getPrincipal());
		button_1.setBounds(570, 108, 123, 35);
		getContentPane().add(button_1);
		
		JButton button_2 = new JButton("Estado");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listamonedas.getSelectedRow() != -1){
					int id = Integer.parseInt(listamonedas.getValueAt(listamonedas.getSelectedRow(), 0).toString());
					try {
						listamonedas.CambioEstadoMoneda(id, listamonedas.getSelectedRow(), 4);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(AdministrarMonedas.class.getResource("/img/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar una Moneda para cambiar su estado","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		button_2.setIcon(new ImageIcon(AdministrarMonedas.class.getResource("/img/del.png")));
		button_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button_2.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				button_2.setBackground(tema.getPrincipal());
			}
		});
		button_2.setForeground(Color.WHITE);
		button_2.setBorderPainted(false);
		button_2.setBackground(tema.getPrincipal());
		button_2.setBounds(570, 154, 123, 35);
		getContentPane().add(button_2);
		
		JButton btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnSair.setIcon(new ImageIcon(AdministrarMonedas.class.getResource("/img/back.png")));
		btnSair.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnSair.setBackground(tema.getSecundario());
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				btnSair.setBackground(tema.getPrincipal());
			}
		});
		btnSair.setForeground(Color.WHITE);
		btnSair.setBorderPainted(false);
		btnSair.setBackground(tema.getPrincipal());
		btnSair.setBounds(570, 202, 123, 35);
		getContentPane().add(btnSair);

	}

}
